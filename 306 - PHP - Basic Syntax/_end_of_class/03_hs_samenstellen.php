<h1>Maak een samenstelling van 2 variabelen en 1 string</h1>
De variabele 1 bevat een voornaam<br>
De variabele 2 bevat een achternaam<br>
<br>
<h3>Resultaat:</h3>
Mijn naam is Harald Schilling<br>
<br>
<br>
<b>Extra:</b>
Maak 3 verschillende codes die in alle gevallen hetzelfde resultaat geven.

<?php
$debug = true; 
$nl = "\n";
$br = '<br>' . $nl;
$hr = '<hr>' . $nl;

error_reporting( E_ALL );
ini_set('display_errors', $debug);

$voorNaam   = 'Harald';
$achterNaam = 'Schilling';
echo $hr;

// Alternatief 1
echo 'Mijn naam is ';
echo $voorNaam;
echo ' ';
echo $achterNaam;
echo $br;
echo $hr;

// Alternatief 2
echo 'Mijn naam is ' . $voorNaam . ' ' . $achterNaam . $br;
echo $hr;

// Alternatief 3
echo "Mijn naam is {$voorNaam} {$achterNaam}" . $br;
echo $hr;

// Alternatief 4
?>
Mijn naam is <?=$voorNaam?> <?=$achterNaam?><br>
<hr>
