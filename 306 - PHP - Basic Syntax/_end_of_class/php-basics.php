<?php
echo "<h1>Het Werkt!</h1>";
?>
<h2>de Slager via Homestead</h2>
<a href="01_errors.php">oefening 01</a><br>
<a href="02_weergave.php">oefening 02</a><br>
<a href="03_samenstellen.php">oefening 03</a><br>


... in de wereld van html ...

<?php 
$naam = 'Harald';
$leeftijd = 23 + 3.5;
$betaald = true;
$naamKind = "Davey";		// camelCase
$naam_kind = "Robin";	// snake_case
$var = array('Aap', 'Noot' );

define('PASSWORD', 'Worst');

define('PI', 3.1415);

// define('PASSWORD', '123');

print $naam_kind;

echo "<pre>";
var_dump(PASSWORD);
echo "</pre>";

?>

<h1>Welkom op deze site <?= $naam ?>!</h1>
<a href="http://www.<?= $naam ?>.nl">Klik</a>
<!-- <?= "debug: " . print_r($var) ?>-->

<?php

// phpinfo();

/* 
dit is ook commentaar
maar dan extra
refgels
*/

# dit ook commentaar

$voornaam = 'Harald';
$achternaam = 'Schilling';

$vollenaam = $voornaam . ' ' . $achternaam;


echo '<div>$vollenaam</div>'; // literal string
echo "<div>$vollenaam</div>"; // substitution van variabelen
echo "<div>{$vollenaam}</div>"; // substitution van variabelen

$fruit = 'Apple';

echo "<div>I like {$fruit}s as breakfast!<\div>";

echo "<h3>Hallo {$naam}!, ik zie dat je {$leeftijd} jaar bent.</h3>";

?>
<h3>Hallo <?=$naam?>!, ik zie dat je <?= $leeftijd ?> jaar bent.</h3>
<?php

$leeg = null;

$lijst[5] = 'five'; // indexed Array
$lijst['number'] = 'five'; // associative Array


echo "<pre>";
echo gettype($vollenaam) . "\n"; 
echo gettype($leeftijd) . "\n"; 
// soort typeof, geeft string met datatype
var_dump( is_int($leeftijd) );

$firstname = '1111';
var_dump( isset($firstname) ); // bestaat een variabel!

var_dump( empty($firstname) ); // is een variabel gevuld!


echo "</pre>";
$br = "<br>\n";
// Blz 48
 
// echo 'my age is ' . 17 + 23;
// echo 20 + '17.5 jaar';
// $price = -1; // gedraagt zich als boolean bij bijv if (0 = false)
// if ($price){
// 	echo 'er is een prijs';
// }
$getal = '17.5 jaar';

echo intval($getal) . $br; // parseInt()

echo (int) $getal . $br;

echo doubleval($getal) . $br; // parseFloat()

echo (double) $getal . $br;

echo (string) $getal . $br; // .toString()

settype($getal, 'int'); // typecast naar integer
echo $getal . $br;

// var prijs = Math.ceil(123.456);
$prijs = ceil(123.456);
// floor round
echo round(123.456, 2) . $br;

echo chr(226) . $br;
echo ord('™') . $br;

$lijst = array('Aap','Noot','Mies');

echo implode(' : ',$lijst) . $br;

$tijd = explode(':', '03:45:12');

print_r($tijd);


// echo( $eenvar );
// echo $eenvar, $tweevar, $drievar;

echo 'Hallo meneer ', $vollenaam, $br; 

// print_r(); // toont de inhoud van een mixed variabel (kort)
// var_dump() // toont de inhoud van een mixed variabel (lang)

// printf(); // zoek eens op php.net
// sprintf()


/**
 * leeftijd van de klant
 * @var integer
 */
$leeftijd = 23;





