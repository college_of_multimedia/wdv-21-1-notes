<h1>Operators</h1>
Maak de volgende berekeningen compleet.<br>
Vervang de <b>a</b> voor een operator die de berekening oplost.<br>
<br>
14,5 a 8 = 116<br>
99 a 24 = 123<br>
3 a 1 = 2<br>
3 a 2 = 9<br>
9 a 2 = 4.5<br>
33 a 3 = 30<br>
5 a 2 = 1<br>

<hr>
<h2> oplossing: </h2>

<?php
echo "14.5 * 8 = 116 : ", 14.5 * 8,'<br>';
?>

99 + 24 = 123 : <?= 99 + 24 ?><br>

3 - 1 = 2 : <?= 3 - 1 ?><br>

3 ** 2 = 9 : <?= 3 ** 2 ?><br>

9 / 2 = 4.5 : <?= 9 / 2 ?><br>

33 - 3 = 30 : <?= 33 - 3 ?><br>

5 % 2 = 1 : <?= 5 % 2 ?><br>