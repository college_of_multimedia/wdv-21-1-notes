<h1>Statements.</h1>
Op basis van variabele $mijnVar moeten de volgende mogelijkheden zichtbaar zijn<br>
Per vraag moet er 1 statement gemaakt worden die alles kan controleren.<br>
<br>
<h2>Vraag 1, $mijnVar is een kleur:</h2>
1- wat is $mijnVar als de onderstaande vragen niet kloppen<br>
2- geef aan of de $mijnVar rood is<br>
3- is $mijnVar geel<br>
4- is $mijnVar groen<br>
<br>
<h2> oplossing 1: </h2>
<?php
$mijnVar = 'rood';
switch ( $mijnVar ) {
    case 'rood' :
        echo 'het is rood';
        break;
    case 'geel' :
        echo 'het is geel';
        break;
    case 'groen' :
        echo 'het is groen';
        break;
    default:
        echo 'iets anders';
}
?>

<hr>

<h2>Vraag 2, $mijnVar is mogelijk een getal:</h2>
1- is $mijnVar echt een getal<br>
2- als 1 waar is, is het dan kleiner dan 4<br>
3- als 1 waar is en 2 niet, is het dan wel groter dan 4<br>
4- kan $mijnVar dan 4 zijn?
<h2> oplossing 2: </h2>
<?php

$mijnVar = 12;

if ( is_numeric($mijnVar) ){
	echo 'Het is een getal', '<br>';
	if ($mijnVar < 4) {
	  echo 'Het is een getal < 4', '<br>';
	} elseif ($mijnVar > 5){
	  echo 'Het is een getal > 5', '<br>';
	} elseif($mijnVar == 4) {
		echo 'Het is een getal = 4', '<br>';
	}
}
?>
<hr>

<h2>Vraag 3, $mijnVar is onbekend:</h2>
1- is $mijnVar een string<br>
2- als 1 waar is, is het eerste karakter dan een getal?<br>
3- als 1 niet waar is, is het dan misschien een boolean?<br>
<h2> oplossing 3: </h2>

<?php
$mijnVar = "12";

if (is_string($mijnVar)){
	echo 'Het is een string', '<br>';
	if (is_numeric($mijnVar[0])){
		echo 'Het 1e karakter is een getal', '<br>';
	}
} elseif (is_bool($mijnVar)){
	echo 'Het is een Bool', '<br>';
}
?>
<hr>
