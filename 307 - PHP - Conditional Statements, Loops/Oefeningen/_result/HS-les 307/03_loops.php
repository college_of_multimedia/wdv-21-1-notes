<h1>Loops.</h1>
<h2>Opdracht 1</h2>
Geef met maximaal 3 regels php de getallen 1 tot en met 99 weer<br>
<h2> oplossing 1: </h2>
<?php
for ($i=1; $i <= 99 ; $i++) {
	echo $i, '<br>'; }
?>
<hr>
<h2>Opdracht 2</h2>
Geef $counter weer zolang $counter < 87<br>
Tel tijdens elke loop 2 bij $counter op<br>
Het eerste getal dat je weergeeft is 0<br>
Elke counter op een losse regel<br>
<h2> oplossing 2: </h2>
<?php
for ($i=0; $i <= 99 ; $i++) {
	if ($i >= 87) break;
	echo $i, '<br>';
	$i += 2; 
}
?>
<hr>
<h2>Opdracht 3</h2>
Geef twee getallen weer in dezelfde loop<br>
Getal A begint bij 0<br>
Getal B begint bij 33<br>
Laat dit zien zolang getal A kleiner is dan getal B<br>
<h2> oplossing 3: </h2>
<?php
for ($a=0,$b=33; $a <= 99 ; $a++, $b--) {
	if ($a < $b ){
		echo $a,' : ', $b,'<br>';
	} else {
		break;
	}
	
}
?>
<hr>
<h2>Opdracht 4</h2>
Geef de getallen 1 tot en met 99 weer<br>
Sla de volgende getallen over: 99, 2, 5<br>
Zodra het getal deelbaar is door 2 moet je de kleur aanpassen van de tekst<br>
Als het getal groter is dan 78 mag je het script afbreken.
<h2> oplossing 4: </h2>
<?php
// ##### optimale versie #####
for ($i=1; $i <= 99 ; $i++) {
	if ($i == 99 || $i == 2 || $i == 5) continue;
	
	$style = $i % 2 == 0 ? ' style="color:red;" ' : '';
	
	echo "<span$style>$i</span><br>";
	
	if ($i >= 87) break;//of die('bye');
}

// ##### Minder optimale versie #####
for ($i=1; $i <= 99 ; $i++) {
	if ($i == 99 || $i == 2 || $i == 5) continue;
	
	if ($i % 2 == 0) {
		echo '<span style="color:red;">',$i,'</span><br>';
	} else {
		echo "<span>$i</span><br>";
	}

	if ($i >= 87) break;// of ('Bye');
}
?>

<hr>



