<a href="01_operators.php">01_operators.php</a><br>
<a href="02_statements.php">02_statements.php</a><br>
<a href="03_loops.php">03_loops.php</a><br>

<?php  

$debug = true;

ini_set('error_reporting', E_ALL);
ini_set('display_errors', $debug);

$nl = "\n"; 
$br = '<br>' . $nl;
$hr = '<hr>' . $nl;
$num = 23;

echo 14 + 15, $br;
echo 34 - 15, $br;
echo -34, $br;
echo 14 * 15, $br;
echo 34 / 15, $br;
echo 34 % 15, $br;
echo 5 ** 2, $br;

echo $num++, $br; 
// blijft nog 23 in de uitvoer maar de variable is achteraf 24 geworden
echo $num, $br;
echo ++$num, $br;

echo $num--, $br;
echo $num, $br;
echo --$num, $br;

// *= /= %= += -=

echo 'Hallo ' . 'daar', $br; // String concate, plakken
$groet = 'Hallo ';

$groet .= 'Harald'; // string extend methode
echo $groet, $br;

$leeftijd = 23;

var_dump( $leeftijd == '23' ); // loose equal to
echo $br;
var_dump( $leeftijd === '23' ); // strict equal to (incl datatype)
echo $hr;

var_dump( $leeftijd >= '23' ); 
echo $br;
var_dump( $leeftijd <= '23' ); 
echo $hr;


var_dump( $leeftijd == '23' OR 15 == 12 ); // verbose stijl
echo $br;
var_dump( $leeftijd == '23' or 15 == 12 ); // verbose stijl
echo $br;
var_dump( $leeftijd == '23' || 15 == 12 ); 
echo $hr;

var_dump( $leeftijd == '23' AND 15 == 12 ); // verbose stijl
echo $br;
var_dump( $leeftijd == '23' and 15 == 12 ); // verbose stijl
echo $br;
var_dump( $leeftijd == '23' && 15 == 12 ); 
echo $hr;

var_dump( !$leeftijd == '23' ); 
var_dump( $leeftijd == '23' ); 
echo $hr;

// ##### Conditional statements #####

if ($leeftijd == 23) { // block if (then)
	echo 'in de if', $br;
}
echo 'na de if', $br; // niet meer deel van de "then"
echo $hr;

if ($leeftijd == 23) // single line if (then)
	echo 'in de if', $br;
echo 'na de if', $br; // niet meer deel van de "then"
echo $hr;

if ($leeftijd == 25): // block if (then) Colon syntax if
	echo 'in de if', $br;
	echo 'in de if', $br;
	echo 'in de if', $br;
	// jump in (to html)
?> 

<h2> dit is html ! </h2>	

<?php
	// jump out (of html)
endif;	
echo 'na de if', $br; // niet meer deel van de "then"
echo $hr;


if ($leeftijd == 25) { // block if (then)
	echo 'in de if, hij is 23', $br;
} else { // else (then)
	echo 'in de if, hij is niet 23', $br;
}
echo 'na de if', $br; // niet meer deel van de "then"
echo $hr;

if ($leeftijd == 25): // block if (then)
	echo 'in de if, hij is 23', $br;
else: // else (then)
	echo 'in de if, hij is niet 23', $br;
endif;

echo 'na de if', $br; // niet meer deel van de "then"
echo $hr;

if ($leeftijd == 25)  // single line if (then)
	echo 'in de if, hij is 23', $br;
else // single line else (then)
	echo 'in de if, hij is niet 23', $br;
echo 'na de if', $br; // niet meer deel van de "then"
echo $hr;

if ($leeftijd == 25) echo 'in de if, hij is 23', $br;
else echo 'in de if, hij is niet 23', $br;
echo 'na de if', $br; // niet meer deel van de "then"
echo $hr;

if ($leeftijd == 23) { // block if (then)
	echo 'in de if, hij is 23', $br;
} elseif($leeftijd == 25) { // else if block (then)
	echo 'in de if, hij is 25', $br;
} else {
	echo 'in de if, hij is niet 23 of 25', $br;
}
echo 'na de if', $br; // niet meer deel van de "then"
echo $hr;

$leeftijd = 35;

switch ($leeftijd) {
	case 26:
		echo 'in de switch, hij is 26', $br;
		break;
	case 25:
		echo 'in de switch, hij is 25', $br;
		break;
	case 24:
		echo 'in de switch, hij is 24', $br;
		break;
	case $leeftijd < 24:
		echo 'in de switch, hij is jonger dan 24', $br;
		break;
	
	default:
		echo 'in de switch, hij is geen van de cases', $br;
		break;
}
echo $hr;

$leeftijd = 18;

switch ($leeftijd) {
	case $leeftijd > 65:
		echo 'in de switch, hij is te oud', $br;
		$toegang = false;
		break;
	case $leeftijd < 23:
		echo 'in de switch, hij is te jong', $br;
		$toegang = false;
		break;
	
	default:
		echo 'in de switch, hij is juiste leeftijd', $br;
		$toegang = true;
		break;
}
echo $hr;
$leeftijd = 6;

switch ($leeftijd) {
	case $leeftijd > 65:
	case $leeftijd < 23:
		$toegang = false;
		break;
	
	default:
		$toegang = true;
		break;
}
var_dump($toegang);
echo $hr;

// ternary operator
$toegang = $leeftijd >= 23 ? 'Welkom op de site' : 'Je bent te jong';
var_dump($toegang);
echo $hr;

var_dump($toegang);
echo $hr;

$leeftijd = 6;

while ( $leeftijd >= 10) { // je mag het blok pas in als de conditie waar is!!
	echo $leeftijd, $br;
	$leeftijd -= 10;
}

echo $hr;

$leeftijd = 6;

do { // je mag WEL het blok in
	echo $leeftijd, $br;
	$leeftijd -= 10;
} while ( $leeftijd >= 10); // aan het einde controle of het nog een keer mag

echo $hr;

for ($i=0; $i < 100; $i++) { 
	if ($i % 4 == 0) {
		echo 'Mag niet!',$br;
		continue;
	} // NIET de heel deelbare getallen
	// als conditie waar is sla de rest van de huidige loop over, en ga naar de volgende waarde
	if ($i > 40) break;
	// als conditie waar is stop de loop direct!
	echo $i, $br;
}
echo $hr;

// foreach as
// indexed Array (positie 0,1,2,...)
$kids = ['Davey','Robin'];

foreach ($kids as $kid) {
	echo "$kid is een kind van my$br";
}
echo $hr;


// associative Array (keys!)
$kids = [
	'primary' => [
		'naam' => 'Davey',
		'leeftijd' => 28,
	],'secondary' => [
		'naam' => 'Robin',
		'leeftijd' => 23,
	],'tertiary' => [
		'naam' => 'Klaas',
		'leeftijd' => 18,
		'paspoort' => false,
	],
];
// , na de laatste value MAG in php
// maar mag NIET in JavaScript of JSON!

foreach ($kids as $key => $value) {
	echo "\$value is mijn $key kind$br";
}
echo $hr;

foreach ($kids as $key => $value) {
	echo "{$value['naam']} is mijn {$key} kind, en is {$value['leeftijd']} jaar$br";
}

/*
foreach ($producten as $productcode => $product) {
	$product['naam']
	$product['omschrijving']
	$product['prijs']
}
*/

	// exit('Oeps! ik heb geen zin meer want: '); // stopt DIRECT het huidige script!
	// die;  // stopt DIRECT het huidige script zonder echo
	die('pannekoek');  // stopt DIRECT het huidige script met echo
	// die ('</body></html>');

