<h1>Arrays.</h1>



<h2>Opdracht 0</h2>
Maak een assoc Array aan met de volgende elementen: <br>
- type<br>
- merk<br>
- kleur<br>
<br>
Geef de volgende zin weer op basis van de informatie uit de array<br>
<br>
De <b>Volkswagen</b> van het type <b>Polo</b> is <b>rood</b><br>
<?php
$auto = [];
$auto['type'] 	= 'A3';
$auto['merk'] 	= 'Audi';
$auto['kleur'] 	= 'paars';

echo 'De <b>' . $auto['merk'] . '</b> ' . $auto['type'] . ' heeft de kleur: ' . $auto['kleur'];
?>
<br>
<br>
<br>
<br>
<br>









<h2>Opdracht 1</h2>
Een indexed array aan met minimaal 5 auto merken<br>
Geef de complete inhoud van deze array weer<br>
<br>

<h2>Opdracht 2</h2>
Maak nu een assiocatieve array met 5 automerken<br>
Geef de complete inhoud van deze array weer<br>
<br>

<h2>Opdracht 3</h2>
Gebruik de bovenstaande array<br>
Voeg per merk 3 types toe<br>
Geef de complete inhoud van deze array weer<br>
<br>

<h2>Opdracht 4</h2>
Gebruik de bovenstaande array<br>
Geef per type aan wat de prijs is<br>
Geef de complete inhoud van deze array weer<br>
<br>

<h2>Opdracht 5</h2>
Pas de weergave van de laaste array zo aan dat het volgende zichtbaar is<br>
De <b>Volkswagen</b> van het type <b>Polo</b> kost <b>€ 13.333,00</b><br>
De <b>Volkswagen</b> van het type <b>Up</b> kost <b>€ 9.999,00</b><br>
De <b>Volkswagen</b> van het type <b>Golf</b> kost<b>€ 39.999,00</b><br>
De <b>BMW</b> van het type <b>3</b> kost <b>€ 13.333,00</b><br>
etc.