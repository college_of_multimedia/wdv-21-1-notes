<h1>Arrays - Statements.</h1>
<h2>Opdracht 1</h2>
Laat alle items uit de onderstaande array zien<br>

<h2>Opdracht 2</h2>
Laat alleen Karel zien<br>

<h2>Opdracht 3</h2>
Laat iedereen zien behalve Kees<br>

<h2>Opdracht 4</h2>
hoeveel regels zitten er in de array?<br>

<h2>Opdracht 5</h2>
Geef alle namen netjes onder elkaar weer.<br>
Als iemand geen tussenvoegsel heeft dan mag er maar 1 spatie zitten tussen de voor en achternaam<br>

<br>
<br>

<?php
// de array
$myArr = array();
$myArr[] = array( 'voornaam'=>'Kees', 	'tussen'=>'de',	'achternaam'=>'Vries');
$myArr[] = array( 'voornaam'=>'Jan', 	'tussen'=>'', 	'achternaam'=>'Bakker');
$myArr[] = array( 'voornaam'=>'Karel', 	'tussen'=>'', 	'achternaam'=>'Groenteman');
$myArr[] = array( 'voornaam'=>'Jan', 	'tussen'=>'de', 'achternaam'=>'Vries');
$myArr[] = array( 'voornaam'=>'Joop', 	'tussen'=>'de', 'achternaam'=>'Bakker');
$myArr[] = array( 'voornaam'=>'Piet', 	'tussen'=>'', 	'achternaam'=>'Bakker');

echo "<hr><h2>Oplossing 1</h2><hr>";
foreach ($myArr as $persoon) {
	echo "Voornaam: {$persoon['voornaam']} <br>";
	echo "Tussen: {$persoon['tussen']} <br>";
	echo "Achternaam: {$persoon['achternaam']} <br>";
	echo "<hr>";
}

echo "<hr><h2>Oplossing 2</h2><hr>";
foreach ($myArr as $persoon) {
	if ($persoon['voornaam'] !== 'Karel') continue;
	echo "Voornaam: {$persoon['voornaam']} <br>";
	echo "Tussen: {$persoon['tussen']} <br>";
	echo "Achternaam: {$persoon['achternaam']} <br>";
	echo "<hr>";
}

echo "<hr><h2>Oplossing 3</h2><hr>";
foreach ($myArr as $persoon) {
	if ($persoon['voornaam'] === 'Kees') continue;
	echo "Voornaam: {$persoon['voornaam']} <br>";
	echo "Tussen: {$persoon['tussen']} <br>";
	echo "Achternaam: {$persoon['achternaam']} <br>";
	echo "<hr>";
}
echo "<hr><h2>Oplossing 4</h2><hr>";
	echo 'er zijn ' . count($myArr) . ' personen<br>';
echo "<hr>";

echo "<hr><h2>Oplossing 5</h2><hr>\n";
foreach ($myArr as $persoon) {
	echo $persoon['voornaam'];
	if ( !empty($persoon['tussen']) ){
		echo ' ' . $persoon['tussen'];
	}
	echo ' ' . $persoon['achternaam'];
	echo "<br>\n";
}
echo "<hr>";

echo "<hr><h2>Oplossing 5b</h2><hr>\n";
foreach ($myArr as $persoon) {
	$vollenaam = trim( $persoon['voornaam'] . ' ' . $persoon['tussen'] ) . ' ' . $persoon['achternaam'];
	echo $vollenaam , "<br>\n";
}
echo "<hr>";

