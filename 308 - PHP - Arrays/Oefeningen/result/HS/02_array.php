<h1>Arrays.</h1>



<h2>Opdracht 0</h2>
Maak een assoc Array aan met de volgende elementen: <br>
- type<br>
- merk<br>
- kleur<br>
<br>
Geef de volgende zin weer op basis van de informatie uit de array<br>
<br>
De <b>Volkswagen</b> van het type <b>Polo</b> is <b>rood</b><br>
<?php
$auto = [];
$auto['type'] 	= 'A3';
$auto['merk'] 	= 'Audi';
$auto['kleur'] 	= 'paars';

echo 'De <b>' . $auto['merk'] . '</b> ' . $auto['type'] . ' heeft de kleur: ' . $auto['kleur'];
?>
<br>
<br>
<br>
<br>
<br>


<h2>Opdracht 1</h2>
Een indexed array aan met minimaal 5 auto merken<br>
Geef de complete inhoud van deze array weer<br>
<br>

<?php
$autos = [ 
	[	'A3', 
		'Audi', 
		'Paars'
	], ['Polo', 
		'Volkswagen', 
		'Rood'
	], ['B-Class', 
		'Mercedes', 
		'Zwart'
	], ['SLK', 
		'Mercedes', 
		'Paars'
	], ['D4', 
		'Citroen', 
		'Paars'
	]
];

// var_dump($autos);
// echo 'De <b>' . $autos[0][1] . '</b> ' . $autos[0][0] . ' heeft de kleur: ' . $autos[0][2] . '<br>';
// echo 'De <b>' . $autos[1][1] . '</b> ' . $autos[1][0] . ' heeft de kleur: ' . $autos[1][2] . '<br>';
// echo 'De <b>' . $autos[2][1] . '</b> ' . $autos[2][0] . ' heeft de kleur: ' . $autos[2][2] . '<br>';
// echo 'De <b>' . $autos[3][1] . '</b> ' . $autos[3][0] . ' heeft de kleur: ' . $autos[3][2] . '<br>';

foreach ( $autos as $auto ) {
	echo 'De <b>' . $auto[1] . '</b> ' . $auto[0] . ' heeft de kleur: ' . $auto[2] . '<br>';
}


?>
<h2>Opdracht 2</h2>
Maak nu een assiocatieve array met 5 automerken<br>
Geef de complete inhoud van deze array weer<br>
<br>

<?php
$autos = [  'A3' =>
	[	'type' => 'A3', 
		'merk' => 'Audi', 
		'kleur' => 'Paars'
	], 'Polo' => [
		'type' => 'Polo', 
		'merk' => 'Volkswagen', 
		'kleur' => 'Rood'
	], 'B-Class' => [	
		'type' => 'B-Class', 
		'merk' => 'Mercedes', 
		'kleur' => 'Zwart'
	], 'SLK' => [
		'type' => 'SLK', 
		'merk' => 'Mercedes', 
		'kleur' => 'Paars'
	],	'D4'=> [
		'type' => 'D4', 
		'merk' => 'Citroen', 
		'kleur' => 'Paars'
	]
];

foreach ( $autos as $auto ) {
	// echo 'De <b>' . $auto['merk'] . '</b> van het type ' . $auto['type'] . ' heeft de kleur: ' . $auto['kleur'] . '<br>';
	echo "De <b>{$auto['merk']}</b> van het type {$auto['type']} heeft de kleur {$auto['kleur']}<br>";
}


?>
<h2>Opdracht 3</h2>
Gebruik de bovenstaande array<br>
Voeg per merk 3 types toe<br>
Geef de complete inhoud van deze array weer<br>
<br>
<?php
$autos = [  'Golf' =>
	[	'type' => 'Golf', 
		'merk' => 'Volkswagen', 
		'kleur' => 'Paars'
	], 'Polo' => [
		'type' => 'Polo', 
		'merk' => 'Volkswagen', 
		'kleur' => 'Rood'
	], 'Up' => [
		'type' => 'Up', 
		'merk' => 'Volkswagen', 
		'kleur' => 'Zwart'
	], 'B-Class' => [	
		'type' => 'B-Class', 
		'merk' => 'Mercedes', 
		'kleur' => 'Zwart'
	], 'SLK' => [
		'type' => 'SLK', 
		'merk' => 'Mercedes', 
		'kleur' => 'Paars'
	],	'C-Class'=> [
		'type' => 'C-Class', 
		'merk' => 'Mercedes', 
		'kleur' => 'Paars'
	]
];

foreach ( $autos as $auto ) {
	echo "De <b>{$auto['merk']}</b> van het type {$auto['type']} heeft de kleur {$auto['kleur']}<br>";
}


?>
<h2>Opdracht 4</h2>
Gebruik de bovenstaande array<br>
Geef per type aan wat de prijs is<br>
Geef de complete inhoud van deze array weer<br>
<br>

<?php 
$autos['B-Class']['prijs'] = 33323.00;
$autos['C-Class']['prijs'] = 44323.45;
$autos['SLK']['prijs'] = 12345.67;
$autos['Golf']['prijs'] = 10000.00;
$autos['Polo']['prijs'] = 623.40;
$autos['Up']['prijs'] = 1212.129;

foreach ( $autos as $auto ) {
	echo "De <b>{$auto['merk']}</b> van het type {$auto['type']} heeft de kleur {$auto['kleur']}, en kost &euro;{$auto['prijs']}<br>";
}


?>


<h2>Opdracht 5</h2>
Pas de weergave van de laaste array zo aan dat het volgende zichtbaar is<br>
De <b>Volkswagen</b> van het type <b>Polo</b> kost <b>€ 13.333,00</b><br>
De <b>Volkswagen</b> van het type <b>Up</b> kost <b>€ 9.999,00</b><br>
De <b>Volkswagen</b> van het type <b>Golf</b> kost<b>€ 39.999,00</b><br>
De <b>BMW</b> van het type <b>3</b> kost <b>€ 13.333,00</b><br>
etc.<br><br>

<?php

foreach ( $autos as $auto ) {
	$mooie_prijs = number_format($auto['prijs'],2,',','.');
	echo "De <b>{$auto['merk']}</b> van het type {$auto['type']} heeft de kleur {$auto['kleur']}, en kost &euro;{$mooie_prijs}<br>";
}

