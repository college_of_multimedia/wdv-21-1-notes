<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Array</title>
</head>
<body>
	
<a href="01_array_statements.php">01_array_statements.php</a><br>
<a href="02_array.php">02_array.php</a><br>
<a href="03_array_extra.php">03_array_extra.php</a><br>
<pre>
<!--
<?php  

$debug = true;

ini_set('error_reporting', E_ALL);
ini_set('display_errors', $debug);

$nl = "\n"; 
$br = '<br>' . $nl;
$hr = '<hr>' . $nl;

$lijst = [3,4,5];
$lijst = array(3,4,5); // ----> [5]
// Javascript: var lijst = new Array(5); ----> [,,,,]
$lijst[10] = "Tien"; 
// bestaat niet maakt hij hem aan
// bestaat hij wel vervangt hij de waarde

$lijst[] = "Raar"; // voeg toe aan het einde!
// var_dump($lijst);

$lijst2[] = "nieuw";
$lijst2[] = "ook nieuw";
// var_dump($lijst2);

$lijst = array(
	'naam' 		=> 'Harald'	, 
	'leeftijd' 	=> 23		, 
	'betaald' 	=> true 	,
);

$lijst = [];
	$lijst['naam'] = 'Harald';
	$lijst['leeftijd'] = 23;
	$lijst['betaald'] = true;

$lijst = [
	'naam' 		=> 'Harald',
	'leeftijd' 	=> 23,
	'betaald' 	=> true,
];

// var_dump($lijst);

// test of een waarde (needle) bestaat in de lijst (haystack) met true voor stricte testing
// var_dump( in_array(12, $lijst, true) );

$fruit_basket = ['apple', 'orange', 'banana'];
list($redfruit, $orangefruit) = $fruit_basket;
// echo $redfruit, ' - ', $orangefruit, '<br>';

// dvd[7] plank[3] kast[5] kamer[2]

$videostore[2][5][3][7] = 'ABBA';
$videostore[1][3][2][2] = 'U2';
$videostore[1][3][6][1] = 'Level42';

// var_dump($videostore);

// see blz 55
// echo $my_array['flower']['yellow'] ; // --> sunfower
// echo $my_array['fruit']['yellow'] ; // --> banana

// $fruit_basket[1] = null;

unset( $fruit_basket[1] );

print_r($fruit_basket);
var_dump($fruit_basket);
echo ($fruit_basket);

?>

-->
</pre>
</body>
</html>
