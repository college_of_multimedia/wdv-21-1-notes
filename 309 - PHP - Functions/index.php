<?php
// ##### Lesson 309 PHP - Functions
$debug = true;
ini_set('display_errors', $debug);
error_reporting(E_ALL);
// error_reporting(E_ALL & ~E_NOTICE); // ALLES behalve de notices

$nl = "\n";
$br = '<br>' . $nl;
$hr = '<hr>' . $nl;

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Lesson 309 PHP - Functions</title>
</head>
<body>
<?php
 

/**
 * multiply vermenigvuldigd 2 getallen 
 * en stuurt een antwoord terug
 * @param  integer $a een getal en standaard 0
 * @param  integer $b een getal en standaard 0
 * @return integer
 */
 function multiply($a = 0, $b = 0){
 	return $a * $b;
 }

$uitkomst = multiply(5, 4,3);
echo "De uitkomst is: $uitkomst $br";

$uitkomst = multiply(5,5);
echo "De uitkomst is: $uitkomst $br";

/**
 * multiplyS vermenigvuldigd 2 getallen
 * en stuurt een nette string met het antwoord terug
 * @param  float $a 
 * @param  float $b 
 * @param  bool  $c show true or false the hr 
 * @return string
 */
function multiplyS($a, $b, $c = true){
	global $br, $hr, $uitkomst;
	// $br is dan wel global, maar een functie in php heeft een eigen scope!!!
	// wil je hem toch gebruiken?? dan moet je hem IN de functie declareren
	// via het global commando 
	$string = "$a x $b = ";
	$string .= $a * $b;
	$string .= $br; 
	if ($c) $string .= $hr; 
	return $string;
}

echo multiplyS(7,8);
echo multiplyS(123.45,456.78);
// var_dump($string); // bestaat alleen in de scope van de functie, niet daarboven/onder


/**
 * laatTafelZien roep 10 keer de functie multiplyS op
 * om een nette tafel te kunnen bouwen in een string
 * @param  integer $tafel welke tafel we willen zien
 * @return string
 */
function laatTafelZien( $tafel = 1 ){
	$html = '<hr>';
	for ( $i = 1 ; $i <= 10 ; $i++ ){
		$html .= multiplyS($i, $tafel, false);
	}
	return $html;
}

echo laatTafelZien(5);
echo laatTafelZien(8);
echo laatTafelZien(12);


/**
 * multiplyS2 vermenigvuldigd 2 getallen
 * en stuurt een nette string met het antwoord terug
 * @param  float $a 
 * @param  float $b 
 * @param  bool  $c show true or false the hr 
 * @return string
 */
function multiplyS2($a, $b, $c = true){
	// $br is dan wel global, maar een functie in php heeft een eigen scope!!!
	// wil je hem toch gebruiken?? dan moet je hem IN de functie declareren
	// via het global commando of gebruik de super global $GLOBAL!
	$string = "$a x $b = ";
	$string .= $a * $b;
	$string .= $GLOBALS['br']; 
	if ($c) $string .= $GLOBALS['hr']; 
	$GLOBALS['uitkomst'] = $string;
}

multiplyS2(5,5);
echo $hr,$uitkomst;

// var_dump($GLOBALS['GLOBALS']);

$myArray = ['aap', 'noot', 'mies'];
$myArray2 = ['jan', 'jaap', 'klaas'];

function addTeun_old( $localArray ){
	//$localArray is een NIEUWE locale variabel met de inhoud van de geleverde array
	$localArray[] = 'teun';
	// print_r($localArray); // Dit is de scope van de FUNCTIE
}

/**
 * addTeun voegt teun toe aan een "globale" array
 * @param array $local bevay verwijzing naar de globale array
 * @return void 
 */
function addTeun( &$local ){
	global $new; // let op dit maakt ook een nieuwe global aan als hij niet bestaat!!!!
	$new = 'hoi';
	//$local is een NIEUWE locale variabel, maar met de reference naar de geleverde array
	// DUS stiekem toch $myArray (ref = verwijzing = alias)
	$local[] = 'teun';
	// print_r($local); // Dit is de scope van de FUNCTIE
}

addTeun($myArray);
print_r($myArray); // Dit is de scope van de ROOT

print_r ($new);

$kort = "addTeun";

$kort($myArray2);
print_r($myArray2); 






?>

	
</body>
</html>