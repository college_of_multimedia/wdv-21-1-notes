<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=2.5, user-scalable=yes">
		<title><?= $page_title ?></title>
		<link href="<?= CSS_ROOT ?>/garden-leaf.css" rel="stylesheet">
	</head>
	<body>
		<div id="container">
		<?php if ($debug) echo '<!-- einde doctype include -->', $nl; ?>