<?php
include_once '../Library/settings.php';
$page_title = 'Garden Leaf - Home';

include_once INC . '/doctype.php';
include_once INC . '/navigatie.php';
include_once INC . '/header-home.php';
?>

<section id="content">
	<article>
		<img src="images/leaf1.jpg" alt="">
		<img src="images/dwell.jpg" alt="Dwell">
		<h2>Dwell</h2>
		<p>Italian adventures for a select group of world travelers seeking authentic experiences layered with tailored activities.</p>
	</article>
	<article> 
		<img src="images/leaf2.jpg" alt="">
		<img src="images/decorate.jpg" alt="Decorate">
		<h2>Decorate</h2>
		<p>Exclusive Italian-designed, handmade home and giftware selections for style-conscious consumers.</p>
	</article>
	<article> 
		<img src="images/leaf3.jpg" alt="">
		<img src="images/discover.jpg" alt="Discover">
		<h2>Discover</h2>
		<p>A luxury vacation home in the undiscovered region of Puglia becomes a reality with the support of our multi-lingual, local team.</p>
	</article>
	<blockquote class="cta">
		<p>“Greenleaf takes you there”</p>
	</blockquote>
</section>
<!-- einde specifieke content -->

<?php
include_once INC . '/footer.php';

// echo __FILE__, '<br>'; // /home/vagrant/code/deslager/public_html/index.php
// include '../Library/settings.php'; // geen URL, maar een PATH!
// include_once '../Library/settings.php'; // geen URL, maar een PATH!
// require '../Library/settings.php'; // geen URL, maar een PATH!
// require_once '../Library/settings.php'; // geen URL, maar een PATH!
// ook allemaal als functie te gebruiken include();
// echo $geheim, '<br>';
// $geheim = 'Top Secret';
// require_once('../Library/settings.php');
// echo $geheim, '<br>';
// echo "Hoi hier ben ik <br>";