<?php
include_once '../../Library/settings.php';
$page_title = 'Garden Leaf - About Us';

include_once INC . '/doctype.php';
include_once INC . '/navigatie.php';
include_once INC . '/header-standard.php';
?>

<section id="content">
	<h2><?= $page_title ?></h2>
</section>
<!-- einde specifieke content -->

<?php
include_once INC . '/footer.php';