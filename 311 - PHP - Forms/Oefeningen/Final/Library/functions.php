<?php

/**
 * Functie om strings van formulieren onschadelijk te maken
 * @param  string $data  de verdachte string
 * @return string        de santized string
 */
function sanit_input( $data = '' ){
	$data = trim( $data );
	$data = stripslashes( $data );
	$data = htmlspecialchars( $data );
	return $data;
}

/**
 * testValue controleert de waarde van een formulierveld (type text,passw etc, en textarea)
 * en haalt spaties voor en/of achter weg
 * @param  string $naamVeld 
 * @return string           de value of een lege string
 */
function testValue($naamVeld = '') {
	if ( isset($_POST[$naamVeld]) && !empty($_POST[$naamVeld]) ){
		$_POST[$naamVeld] = sanit_input( $_POST[$naamVeld] );
		return $_POST[$naamVeld]; // early return
	}
	return ''; // last return
}

/**
 * testOption controleert de waarde van een option veld
 * @param  string $naamVeld    
 * @param  string $valueOption
 * @return string           	selected of een lege string
 */
function testOption( $naamVeld = '', $valueOption = '' ) {
	if ( isset($_POST[$naamVeld]) && !empty($_POST[$naamVeld]) && $valueOption === $_POST[$naamVeld] ){
		return ' selected'; // early return
	}
	return ''; // last return
}

/**
 * testRadio controleert een radioButton
 * @param  string  $naamVeld   
 * @param  string  $valueRadio 
 * @param  boolean $default    	standaard false, optioneel op true
 * @return string           	checked of een lege string
 */
function testRadio( $naamVeld = '', $valueRadio = '', $default = false ) {
	if ( isset($_POST[$naamVeld]) && !empty($_POST[$naamVeld]) && $valueRadio === $_POST[$naamVeld] ){
		return ' checked'; // early return
	} elseif (!isset($_POST[$naamVeld]) && $default){
		return ' checked'; // early return
	}
	return ''; // last return
}

/**
 * [testCheck description]
 * @param  string  $naamVeld  
 * @param  string  $valueCheck
 * @param  boolean $default    	standaard false, optioneel op true
 * @return string           	checked of een lege string
 */
function testCheck( $naamVeld = '', $valueCheck = '', $default = false ) {
	if ( isset($_POST[$naamVeld]) && !empty($_POST[$naamVeld]) && in_array( $valueCheck, $_POST[$naamVeld] ) ){
		return ' checked'; // early return
	} elseif (!isset($_POST[$naamVeld]) && $default){
		return ' checked'; // early return
	}
	return ''; // last return
}


// ##### DEEL 2 de Validatie #####
/**
 * valideer een formulier
 * @return bool // array
 */
function valideerForm() {
	if ( isset( $_POST['submit'] ) ) {
		$valideer = true;
		// $error = [];
		
		$naamVeld = 'email';
		$_POST[$naamVeld] = filter_var($_POST[$naamVeld], FILTER_SANITIZE_EMAIL);
		// alleen code die in email MAG staat er nu nog in!
		if ( isset($_POST[$naamVeld]) && !empty($_POST[$naamVeld]) && filter_var($_POST[$naamVeld], FILTER_VALIDATE_EMAIL) ){
			// het is goed, dus evt positive feedback
		} else {
			// het is fout, dus evt negatieve feedback
			// $error[] = 'Verkeerde of geen email ingevuld';
			$valideer = false;
		}

		$naamVeld = 'voornaam';
		$_POST[$naamVeld] = filter_var($_POST[$naamVeld], FILTER_SANITIZE_STRING);
		// alleen code die in email MAG staat er nu nog in!
		if ( isset($_POST[$naamVeld]) && !empty($_POST[$naamVeld]) ){
			// het is goed, dus evt positive feedback
		} else {
			// het is fout, dus evt negatieve feedback
			// $error[] = 'Verkeerde of geen email ingevuld';
			$valideer = false;
		}
		
		$naamVeld = 'achternaam';
		$_POST[$naamVeld] = filter_var($_POST[$naamVeld], FILTER_SANITIZE_STRING);
		// alleen code die in email MAG staat er nu nog in!
		if ( isset($_POST[$naamVeld]) && !empty($_POST[$naamVeld]) ){
			// het is goed, dus evt positive feedback
		} else {
			// het is fout, dus evt negatieve feedback
			// $error[] = 'Verkeerde of geen email ingevuld';
			$valideer = false;
		}
		// return $error;
		return $valideer;
		// als het true is dan is het dus goed ingevuld
		// als het false is is het verstuurd, maar niet goed...
	}
	// $error['firstrun'] = true;
	// return $error;
	return false;
	// want het is NIET verstuurd;
}

$formCorrect = valideerForm();
