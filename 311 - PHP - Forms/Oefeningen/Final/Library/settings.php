<?php
$debug = true;
ini_set('display_errors', $debug);
error_reporting(E_ALL);

define('DOCUMENT_ROOT', realpath( $_SERVER['DOCUMENT_ROOT'] ) );
define('SITE_ROOT',     realpath( DOCUMENT_ROOT . '/..'     ) );
define('LIB_ROOT',      realpath( SITE_ROOT . '/Library'    ) );

define('WEB_URL', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST']);

require_once('functions.php');