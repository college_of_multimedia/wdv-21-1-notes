<?php require_once '../Library/settings.php'; ?>
<?php // dit is THE SPOT voor een redirect ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Oefening 01 - html forms</title>
	<style>
		label {
			display: inline-block;
			width: 100px;
		}
	</style>
</head>
<body>
<!-- <?php print_r( $_GET ) ?> -->
<!-- niet verbeterde versie: <?php print_r( $_POST ) ?> -->

<h1>HTML Formulier</h1>
<h2>Opdracht 1</h2>
<p>
	Maak een HTML formulier waarbij ik een voornaam, tussenvoegsel, achternaam, email en postcode kan invoeren<br>
	Ook wil ik een woonplaats kunnen selecteren <em>( 3 opties zijn genoeg )</em><br>
	Als ik op 'verstuur' klik dan moet ik op dezelfde pagina komen<br>
</p>
<form action="<?= htmlspecialchars( $_SERVER['PHP_SELF'] ) ?>" method="post" name="myForm" id="myForm">

	<label for="voornaam">Voornaam:</label>
	<input type="text" name="voornaam" id="voornaam" value="<?= testValue('voornaam') ?>"><br>

	<label for="tussen">Tussenvoegsel:</label>
	<input type="text" name="tussen" id="tussen" value="<?= testValue('tussen') ?>"><br>

	<label for="achternaam">Achternaam:</label>
	<input type="text" name="achternaam" id="achternaam" value="<?= testValue('achternaam') ?>"><br>

	<label for="email">Email:</label>
	<input type="text" name="email" id="email" value="<?= testValue('email') ?>"><br>

	<label for="postcode">Postcode:</label>
	<input type="text" name="postcode" id="postcode" value="<?= testValue('postcode') ?>"><br>

	<label for="woonplaats">Woonplaats:</label>
	<select name="woonplaats" id="woonplaats">
		<option value="0"> -- maak een keuze -- </option>
		<option value="Amsterdam"<?= testOption('woonplaats', 'Amsterdam') ?>>Amsterdam</option>
		<option value="Rotterdam"<?= testOption('woonplaats', 'Rotterdam') ?>>Rotterdam</option>
		<option value="Lelystad"<?= testOption('woonplaats', 'Lelystad') ?>>Lelystad</option>
	</select><br>

	<label for="gender">Gender:</label>
	<input type="radio" name="gender" id="gender_m" value="Man" <?= testRadio('gender', 'Man'); ?> >Man - 
	<input type="radio" name="gender" id="gender_v" value="Vrouw" <?= testRadio('gender', 'Vrouw'); ?> >Vrouw - 
	<input type="radio" name="gender" id="gender_x" value="Neutraal" <?= testRadio('gender', 'Neutraal', true); ?> >Neutraal<br> 

	<label for="computer[]">Computer:</label>
	<input type="checkbox" name="computer[]" id="computer_1" value="Mac" <?= testCheck('computer', 'Mac', true); ?>>Macintosh - 
	<input type="checkbox" name="computer[]" id="computer_2" value="PC" <?= testCheck('computer', 'PC'); ?>>Personal Computer Windows -  
	<input type="checkbox" name="computer[]" id="computer_3" value="Unix" <?= testCheck('computer', 'Unix'); ?>>Unix zelfbouw -
	(kies er minstens 1)<br> 


	<label for="verstuur"></label>
	<input type="submit" name="submit" id="submit" value="Versturen"><br>

</form>
<hr>

<h2>Opdracht 2</h2>
<p>
	Na het versturen moeten de ingevulde waardes weer zichtbaar zijn in het formulier.<br>
    Ook de juiste selectie optie moet geselecteerd worden
</p>
<hr>

<h2>Opdracht 3</h2>
<p>
	Laat het formulier nu naar een nieuwe pagina verwijzen<br>
    Geef op deze nieuwe pagina de ingevulde waardes weer.
</p>

<h2>Dit is de nieuwe pagina</h2>

<p>
<?php
if ( !empty($_POST) ){
	$vollenaam = trim( trim ($_POST['voornaam'] . ' ' . $_POST['tussen'] ) . ' ' . $_POST['achternaam']);
	if ( empty( $vollenaam ) ) {
		$vollenaam = 'Anoniem';
	}

	$aanhef = '';
	if ($_POST['gender'] == "Man"){
		$aanhef = 'meneer';
	} elseif($_POST['gender'] == "Vrouw"){
		$aanhef = 'mevrouw';
	}

 	// voorbeeld als ternary, let op de ()!!
	// $aanhef = $_POST['gender'] == "Man" ? 'meneer' : ($_POST['gender'] == "Vrouw" ? 'mevrouw' : '');
	
	$woonplaats = '';
	if ($_POST['woonplaats']){
		$woonplaats = ' uit ' . $_POST['woonplaats'];
	}

	$computers = implode( ', ', $_POST['computer'] ) ;
	
	$bericht = "Hallo $aanhef $vollenaam{$woonplaats}, je gebruikt $computers!";
	echo $bericht;

	/*
		$result = mail('harald@nietdoen.nl', 'Feedback van de site', $bericht);

		if ($result){
			// als het gelukt is dan....
			// redirect
			header('Location: ' . '/danku.php');
		}
	*/

} else {
	echo "Er is nog niets verstuurd!";
}
?>	
</p>


<!-- <?php print_r( $_GET ) ?> -->
<!-- verbeterde versie: <?php print_r( $_POST ) ?> -->

</body>
</html>