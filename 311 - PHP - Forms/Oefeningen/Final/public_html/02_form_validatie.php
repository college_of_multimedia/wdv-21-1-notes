<?php require_once '../Library/settings.php'; ?>
<?php // dit is THE SPOT voor een redirect ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Oefening 02 - form validatie</title>
	<style>
		label {
			display: inline-block;
			width: 100px;
		}
	</style>
</head>
<body>
<h1>HTML Formulier Validatie</h1>
<h2>Opdracht 1 ( <?php var_dump( $formCorrect); ?>)</h2>
<p>
	Op basis van opgave 01_html_formulier komen nu de volgende validaties:<br>
	- Is het email een geldig email adres?<br>
	- Als het niet een echt email adres is dan moet er een waarschuwing weergegeven worden bij het email veld.<br>
	- maar ... het veld moet wel ingevuld blijven met de waarde die ik verstuurd heb<br>
    filter_var( $email, FILTER_VALIDATE_EMAIL );
</p>

	<?php 
		// if ($error){
			 // echo '<p class="errorForm">';
			 // toon alle fouten
			 // echho "</p>";
		// }
	?>

<form action="<?= htmlspecialchars( $_SERVER['PHP_SELF'] ) ?>" method="post" name="myForm" id="myForm">

	<label for="voornaam">Voornaam:</label>
	<input type="text" name="voornaam" id="voornaam" value="<?= testValue('voornaam') ?>"><br>

	<label for="tussen">Tussenvoegsel:</label>
	<input type="text" name="tussen" id="tussen" value="<?= testValue('tussen') ?>"><br>

	<label for="achternaam">Achternaam:</label>
	<input type="text" name="achternaam" id="achternaam" value="<?= testValue('achternaam') ?>"><br>

	<label for="email">Email:</label>
	<input type="text" name="email" id="email" value="<?= testValue('email') ?>"><br>

	<label for="postcode">Postcode:</label>
	<input type="text" name="postcode" id="postcode" value="<?= testValue('postcode') ?>"><br>

	<label for="woonplaats">Woonplaats:</label>
	<select name="woonplaats" id="woonplaats">
		<option value="0"> -- maak een keuze -- </option>
		<option value="Amsterdam"<?= testOption('woonplaats', 'Amsterdam') ?>>Amsterdam</option>
		<option value="Rotterdam"<?= testOption('woonplaats', 'Rotterdam') ?>>Rotterdam</option>
		<option value="Lelystad"<?= testOption('woonplaats', 'Lelystad') ?>>Lelystad</option>
	</select><br>

	<label for="gender">Gender:</label>
	<input type="radio" name="gender" id="gender_m" value="Man" <?= testRadio('gender', 'Man'); ?> >Man - 
	<input type="radio" name="gender" id="gender_v" value="Vrouw" <?= testRadio('gender', 'Vrouw'); ?> >Vrouw - 
	<input type="radio" name="gender" id="gender_x" value="Neutraal" <?= testRadio('gender', 'Neutraal', true); ?> >Neutraal<br> 

	<label for="computer[]">Computer:</label>
	<input type="checkbox" name="computer[]" id="computer_1" value="Mac" <?= testCheck('computer', 'Mac', true); ?>>Macintosh - 
	<input type="checkbox" name="computer[]" id="computer_2" value="PC" <?= testCheck('computer', 'PC'); ?>>Personal Computer Windows -  
	<input type="checkbox" name="computer[]" id="computer_3" value="Unix" <?= testCheck('computer', 'Unix'); ?>>Unix zelfbouw -
	(kies er minstens 1)<br> 


	<label for="verstuur"></label>
	<input type="submit" name="submit" id="submit" value="Versturen"><br>

</form>
<hr>

<h2>Opdracht 2</h2>
<p>
	Als de ingevoerde data valid is, dus door de controles heen komt dan:<br>
	Moet je een bedankt pagina laten zien, anders zie je dus nog steeds het formulier.<br>
	Zorg dat deze pagina een losse php pagina is zodat die goed opgemaakt kan worden.<br>
</p>
</body>
</html>