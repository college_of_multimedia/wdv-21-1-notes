<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
	<style>
		label {
			display: inline-block;
			width: 100px;
		}
	</style>
</head>
<body>

<!-- <?php print_r( $_FILES ) ?> -->	

<h1>Bestanden versturen</h1>
<h2>Opdracht 1</h2>
<p>
    Maak een nieuwe php pagina met daarin een html formulier<br>
    In dit formulier moet het mogelijk zijn om een bestand te selecteren en te versturen<br>
</p>

<form action="<?= htmlspecialchars($_SERVER['PHP_SELF']) ?>" method="post" enctype="multipart/form-data">
	<label for="bestand">File:</label>
	<input type="file" name="bestand"><br>

	<label for="verstuur"></label>
	<input type="submit" name="verstuur" value="Verstuur"><br>
</form>


<h2>Opdracht 2</h2>
<p>
    Sla het verstuurde bestand op als het verstuurd is<br>
    v.b.: <br>
    move_uploaded_file( $_FILES['bestandje']['tmp_name'], $_FILES['bestandje']['name'] );
</p>
<?php 
	// $uploaddir = realpath( __DIR__ . '/upload/');
	// var_dump($uploaddir);
	// $filename = $_FILES['bestand']['name'];
	// $file = $_FILES['bestand'];
    // $filename = $file['name'];
    // move_uploaded_file( $file['tmp_name'], $uploaddir . '/' . $filename );
?>

<h2>Opdracht 3</h2>
<p>
    Zorg ervoor dat er alleen png en jpg bestanden verstuurd kunnen worden.<br>
    array('image/jpeg', 'image/gif', 'image/png');
</p>

<?php 
	$uploaddir = realpath( __DIR__ . '/upload/');
	$validMimeTypes = array('image/jpeg', 'image/gif', 'image/png');
	$maxFileSize = 100000; // in bytes!!!!
    /*
    $validMimeTypes = array(	
    	'image/jpeg'	=> '.jpg',
    	'image/gif' 	=> '.gif',
    	'image/png' 	=> '.png'
    );
	*/
	if (isset($_FILES['bestand'])) {
		$file = $_FILES['bestand'];
   		$filename = explode('.', $file['name']);
   		$name = $filename[0];
   		$extension = $filename[1];
   		$size = $file['size'];
   		$mime = $file['type'];
   		$newFile = $uploaddir . '/' . $name . '.' . $extension;

   		if (in_array($mime, $validMimeTypes)){
   			if ($size < $maxFileSize) {
   				if (!file_exists($newFile)){
					move_uploaded_file( $file['tmp_name'], $newFile);
    				echo "bestand is geupload";
    			} else {
    				echo 'bestand bestaat al, kies een andere naam';
    				 $newFile = $uploaddir . '/' . $name . '_1.' . $extension; // slecht voorbeeld hardcoded serialize truc
    				 move_uploaded_file( $file['tmp_name'], $newFile);
    			}
    		} else {
    			echo 'te groot bestand, maximaal 100kb'; 
   			}
    	} else {
    		echo 'ongeldige bestands type'; 
    	}
	}



?>



<h2>Opdracht 4</h2>
<p>
    Zorg ervoor dat de bestanden nooit overschreven worden.<br>
    maar zet er iets achter
</p>
</body>
</html>
