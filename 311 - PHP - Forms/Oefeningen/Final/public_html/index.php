<?php require_once '../Library/settings.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Lesson 311 - PHP - Forms</title>
</head>
<body>
	<a href="01_html_form.php">Oefening 1 HTML Forms</a><hr>
	<a href="02_form_validatie.php">Oefening 2 Form validatie</a><hr>
	<a href="02_form_validatie_2.php">Oefening 2 Form validatie skelet</a><hr>
	<a href="03_show_file.php">Oefening 3 File upload</a><hr>
</body>
</html>