<?php
$debug = true;
ini_set('display_errors', $debug);
error_reporting(E_ALL);

define('DOCUMENT_ROOT', realpath( $_SERVER['DOCUMENT_ROOT'] ) );
define('SITE_ROOT',     realpath( DOCUMENT_ROOT . '/..'     ) );
define('LIB_ROOT',      realpath( SITE_ROOT . '/Library'    ) );

define('WEB_URL', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST']);

if ( !session_id() ) {
	session_start();
}

// hiervoor zou de controle van username en password
// als correct vullen we de waardes hieronder
// $_SESSION['fullname'] = 'Harald Schilling';
// $_SESSION['username'] = 'harald@cmm.nl';
// $_SESSION['login'] = true;
// $_SESSION['role'] = 'guest';
// $_SESSION['role'] = 'admin';

// session_destroy(); // de hele sessie is leeg
// unset($_SESSION['login']); // een specifieke parameter van de sessie wordt gewist

require_once('functions.php');