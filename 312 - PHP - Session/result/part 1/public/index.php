<?php require_once '../Library/settings.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>PHP Sessions</title>
</head>
<body>
<!-- 
<?php
var_dump( $_SESSION );

var_dump( session_id() );

var_dump( time() ); // NOW! (in sec sinds 1-1-1970 00:00)

var_dump( time() + 42000 );

?>
-->	
<a href='login.php'>login page</a>

<h1>Sessies</h1>
<h2 style="background-color: <?= $bgcolor; ?>">Opdracht 1 van <?= $_SESSION['fullname'] ?></h2>
<p>
	Maak op een nieuwe php pagina een HTML formulier waarbij ik een naam en wachtwoord kan invoeren.<br>
	Als ik op 'verstuur' klik dan moet ik op dezelfde pagina komen.<br>

	
</p>

<h2>Opdracht 2</h2>
<p>
	Na het versturen moeten de ingevulde waardes weer zichtbaar zijn in het formulier.<br>
</p>

<h2>Opdracht 3</h2>
<p>
	Als ik opnieuw op deze pagina kom moeten de ingevulde gegevens nog steeds zichtbaar zijn.<br>
    Dus niet na een page refresh, maar als je opnieuw naar dezelfde url gaat zonder de browser te sluiten.
    (OF het domijn hebben verlaten!)
</p>

<?php if ( isset($_SESSION['login']) && $_SESSION['role'] == 'guest'): ?>

<h2>Opdracht 4 (alleen voor ingelogde gasten!)</h2>
<p>
	Vergelijk de ingevuld gegevens met een standaard waarde om zo te zien of de juiste gegevens ingevuld zijn<br>
    Als de naam of het wachtwoord niet overeen komen dan moet je hiervan een melding geven.
</p>
<?php endif; ?>
<footer> Copyright CMM <?= date( 'd-M-Y' ) ?>


	</footer>
</body>
</html>

