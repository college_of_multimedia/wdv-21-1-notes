<?php if ( empty($_POST) && !isset($_SESSION["username"]) ): ?>

	<div class="alert">
		U dient in te loggen!
	</div>

<?php elseif ($_POST && !$ingelogd): ?>

    <div class="error">
		Inlogpoging mislukt, combinatie username en wachtwoord onbekend!
	</div>

<?php elseif (isset($_SESSION["username"]) ): ?>

	<div class="success">
		U bent ingelogd<br>
	</div>

<?php endif; 