<?php 
if (!isset($_SESSION['username'])): 
	header('Location: ' . WEB_URL ); // redirect
	exit();
endif; 