<?php require_once '../Library/settings.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>PHP Sessions</title>
	<style>
		label {
			display: inline-block;
			width: 100px;
		}

		.error {background-color: red;}
		.alert {background-color: yellow;}
		.success {background-color: green;}

	</style>
</head>
<body>

	<form method="post" action="">
		<label for='username'>Username:</label>
		<input type="text" name="username" value="<?= testLoginValue('username') ?>"><br>
		<label for='password'>Password:</label>
		<input type="password" name="password" value="<?= testLoginValue('password') ?>"><br>
		<label for='submit'></label>
		<input type="submit" name="submit" value="login"><br>
	</form>

<?php if ( empty($_POST) && !isset($_SESSION["username"]) ): ?>
	<div class="alert">
		U dient in te loggen!
	</div>

<?php elseif ($_POST && !$ingelogd): ?>
    <div class="error">
		Inlogpoging mislukt, combinatie username en wachtwoord onbekend!
	</div>

<?php elseif (isset($_SESSION["username"]) ): ?>
	<div class="success">
		Inlogpoging geslaagd, welkom terug <?= $_SESSION["fullname"]; ?><br>
		<a href="?logout">log out</a> <!-- get variabel -->
	</div>
<?php endif ?>




<a href="index.php" style="background-color: <?= $bgcolor ?>">Back to home</a>
<?php // phpinfo() ?>

</body>
</html>