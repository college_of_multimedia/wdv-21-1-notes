<?php require_once '../Library/settings.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>PHP Sessions</title>
	<style>
		.error {background-color: red;}
		.alert {background-color: yellow;}
		.success {background-color: green;}
	</style>
</head>
<body>

	<?php include_once(INC . '/feedback-login.php'); ?>

	<?php include(INC . '/loginform.php'); ?>

	<a href="index.php" style="background-color: <?= $bgcolor ?>">Back to home</a>

</body>
</html>