/* Commentaar */
-- Commentaar
# Commentaar

SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| phpmyadmin         |
| scotchbox          |
| sys                |
| wwwNewsSiteTest    |
+--------------------+
7 rows in set (0.00 sec)


CREATE DATABASE `CMM_people`;
CREATE DATABASE IF NOT EXISTS `CMM_people`;
CREATE DATABASE IF NOT EXISTS CMM_people;
## BACKTICKS ` niet QUOUTE '

USE CMM_people;
USE `CMM_people`;

mysql> CREATE TABLE `People` (
    -> userID SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    -> name VARCHAR(50) NOT NULL,
    -> email VARCHAR(50),
    -> pass CHAR(32) NOT NULL,
    -> regDate DATETIME NOT NULL,
    -> regDateTM TIMESTAMP NOT NULL,
    -> PRIMARY KEY (userID)
    -> );

CREATE TABLE IF NOT EXISTS `People` ( userID SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT, name VARCHAR(50) NOT NULL, email VARCHAR(50), pass CHAR(32) NOT NULL, regDate DATETIME NOT NULL, regDateTM TIMESTAMP NOT NULL, PRIMARY KEY (userID) );

SHOW COLUMNS from People;
+-----------+----------------------+------+-----+-------------------+-----------------------------+
| Field     | Type                 | Null | Key | Default           | Extra                       |
+-----------+----------------------+------+-----+-------------------+-----------------------------+
| userID    | smallint(5) unsigned | NO   | PRI | NULL              | auto_increment              |
| name      | varchar(50)          | NO   |     | NULL              |                             |
| email     | varchar(50)          | YES  |     | NULL              |                             |
| pass      | char(32)             | NO   |     | NULL              |                             |
| regDate   | datetime             | NO   |     | NULL              |                             |
| regDateTM | timestamp            | NO   |     | CURRENT_TIMESTAMP | on update CURRENT_TIMESTAMP |
+-----------+----------------------+------+-----+-------------------+-----------------------------+
6 rows in set (0.00 sec)

INSERT INTO `People` 
VALUES (NULL, 'me' , 'me@me.me', md5('worst'), NOW(), NOW() );

SELECT * FROM `People`;
+--------+------+----------+----------------------------------+---------------------+---------------------+
| userID | name | email    | pass                             | regDate             | regDateTM           |
+--------+------+----------+----------------------------------+---------------------+---------------------+
|      1 | me   | me@me.me | f5f46a62bde5dd979c0652f06d716a9e | 2021-06-05 10:49:53 | 2021-06-05 10:49:53 |
+--------+------+----------+----------------------------------+---------------------+---------------------+
1 row in set (0.00 sec)

INSERT INTO `People` (`name`, `email`, `pass`, `regDate`) VALUES
 ('ik', 'ik@ik.ik', 'geheim', NOW() ),
 ('jij', 'jij@ik.ik', 'geheim', NOW() ),
 ('wij', 'wij@ik.ik', 'geheim', NOW() ),
 ('zij', 'zij@ik.ik', 'geheim', NOW() ),
 ('zij2', 'jij2@ik.ik', 'geheim', NOW() );

SELECT * FROM `People`;                                                                                              
+--------+--------+------------+----------------------------------+---------------------+---------------------+
| userID | name   | email      | pass                             | regDate             | regDateTM           |
+--------+--------+------------+----------------------------------+---------------------+---------------------+
|      1 | me     | me@me.me   | f5f46a62bde5dd979c0652f06d716a9e | 2021-06-05 10:49:53 | 2021-06-05 10:49:53 |
|      2 | ik     | ik@ik.ik   | geheim                           | 2021-06-05 10:59:47 | 2021-06-05 10:59:47 |
|      3 | jij    | jij@ik.ik  | geheim                           | 2021-06-05 10:59:47 | 2021-06-05 10:59:47 |
|      4 | wij    | wij@ik.ik  | geheim                           | 2021-06-05 10:59:47 | 2021-06-05 10:59:47 |
|      5 | zij    | zij@ik.ik  | geheim                           | 2021-06-05 10:59:47 | 2021-06-05 10:59:47 |
|      6 | zij2   | jij2@ik.ik | geheim                           | 2021-06-05 10:59:47 | 2021-06-05 10:59:47 |
|      7 | mealso | me@me.me   | f5f46a62bde5dd979c0652f06d716a9e | 2021-06-05 11:06:39 | 2021-06-05 11:06:39 |
+--------+--------+------------+----------------------------------+---------------------+---------------------+
7 rows in set (0.00 sec)

SELECT name FROM `People`;
+--------+
| name   |
+--------+
| me     |
| ik     |
| jij    |
| wij    |
| zij    |
| zij2   |
| mealso |
+--------+
7 rows in set (0.00 sec)

SELECT * FROM `People` WHERE name = 'ik';
+--------+------+----------+--------+---------------------+---------------------+
| userID | name | email    | pass   | regDate             | regDateTM           |
+--------+------+----------+--------+---------------------+---------------------+
|      2 | ik   | ik@ik.ik | geheim | 2021-06-05 10:59:47 | 2021-06-05 10:59:47 |
+--------+------+----------+--------+---------------------+---------------------+
1 row in set (0.00 sec)

SELECT * FROM `People` WHERE email LIKE '_ij@ik.ik';
+--------+------+-----------+--------+---------------------+---------------------+
| userID | name | email     | pass   | regDate             | regDateTM           |
+--------+------+-----------+--------+---------------------+---------------------+
|      3 | jij  | jij@ik.ik | geheim | 2021-06-05 10:59:47 | 2021-06-05 10:59:47 |
|      4 | wij  | wij@ik.ik | geheim | 2021-06-05 10:59:47 | 2021-06-05 10:59:47 |
|      5 | zij  | zij@ik.ik | geheim | 2021-06-05 10:59:47 | 2021-06-05 10:59:47 |
+--------+------+-----------+--------+---------------------+---------------------+
3 rows in set (0.00 sec)

SELECT * FROM `People` WHERE email LIKE '%ik.ik';
+--------+------+------------+--------+---------------------+---------------------+
| userID | name | email      | pass   | regDate             | regDateTM           |
+--------+------+------------+--------+---------------------+---------------------+
|      2 | ik   | ik@ik.ik   | geheim | 2021-06-05 10:59:47 | 2021-06-05 10:59:47 |
|      3 | jij  | jij@ik.ik  | geheim | 2021-06-05 10:59:47 | 2021-06-05 10:59:47 |
|      4 | wij  | wij@ik.ik  | geheim | 2021-06-05 10:59:47 | 2021-06-05 10:59:47 |
|      5 | zij  | zij@ik.ik  | geheim | 2021-06-05 10:59:47 | 2021-06-05 10:59:47 |
|      6 | zij2 | jij2@ik.ik | geheim | 2021-06-05 10:59:47 | 2021-06-05 10:59:47 |
+--------+------+------------+--------+---------------------+---------------------+
5 rows in set (0.00 sec)

SELECT userID,name,email FROM `People` LIMIT 3;
# Beperk de resulaten op max 3
+--------+------+-----------+
| userID | name | email     |
+--------+------+-----------+
|      1 | me   | me@me.me  |
|      2 | ik   | ik@ik.ik  |
|      3 | jij  | jij@ik.ik |
+--------+------+-----------+
3 rows in set (0.00 sec)

SELECT userID,name,email FROM `People` LIMIT 3,3;
# Beperk de resulaten op max 3 start na 3
+--------+------+------------+
| userID | name | email      |
+--------+------+------------+
|      4 | wij  | wij@ik.ik  |
|      5 | zij  | zij@ik.ik  |
|      6 | zij2 | jij2@ik.ik |
+--------+------+------------+
3 rows in set (0.00 sec)

SELECT userID,name,email FROM `People` LIMIT 6,3;
# Beperk de resulaten op max 3 start na 6
+--------+--------+----------+
| userID | name   | email    |
+--------+--------+----------+
|      7 | mealso | me@me.me |
+--------+--------+----------+
1 row in set (0.00 sec)

SELECT userID,name,email FROM `People` WHERE userID = 1 LIMIT 1;

SELECT userID,name,email FROM `People` ORDER BY name;
+--------+--------+------------+
| userID | name   | email      |
+--------+--------+------------+
|      2 | ik     | ik@ik.ik   |
|      3 | jij    | jij@ik.ik  |
|      1 | me     | me@me.me   |
|      7 | mealso | me@me.me   |
|      4 | wij    | wij@ik.ik  |
|      5 | zij    | zij@ik.ik  |
|      6 | zij2   | jij2@ik.ik |
+--------+--------+------------+
7 rows in set (0.00 sec)

mysql> SELECT userID,name,email FROM `People` ORDER BY name DESC;
-- Desc is Descending standaard is Ascending
+--------+--------+------------+
| userID | name   | email      |
+--------+--------+------------+
|      6 | zij2   | jij2@ik.ik |
|      5 | zij    | zij@ik.ik  |
|      4 | wij    | wij@ik.ik  |
|      7 | mealso | me@me.me   |
|      1 | me     | me@me.me   |
|      3 | jij    | jij@ik.ik  |
|      2 | ik     | ik@ik.ik   |
+--------+--------+------------+
7 rows in set (0.00 sec)


UPDATE People SET email='new@new.ik' WHERE name = 'me';
UPDATE `People` SET `email`='new@new.ik' WHERE `name` = 'me';

ALTER TABLE `People` CHANGE userID people_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `People` CHANGE email email VARCHAR(50) NOT NULL AFTER pass;
DELETE FROM People WHERE people_id = 3 LIMIT 1;

DROP TABLE People;
DROP DATABASE CMM_people;
SHOW DATABASES;

### PMA ###
CREATE TABLE `cmm_people`.`people` ( `id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT ,  `firstname` VARCHAR(50) NULL DEFAULT NULL ,  `surname` VARCHAR(50) NULL DEFAULT NULL ,  `username` VARCHAR(20) NULL DEFAULT NULL ,  `email` VARCHAR(50) NULL DEFAULT NULL ,  `pass` CHAR(32) NULL DEFAULT NULL ,    PRIMARY KEY  (`id`),    INDEX  `Email` (`email`),    UNIQUE  `Username` (`username`)) ENGINE = InnoDB;

ALTER TABLE `people` ADD `reg_date` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP  AFTER `pass`;
ALTER TABLE `people` ADD INDEX(`surname`);
