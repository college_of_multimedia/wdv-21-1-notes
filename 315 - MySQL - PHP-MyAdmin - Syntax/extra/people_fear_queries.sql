SELECT people.name, fears.fear 
FROM people, fears, person_fear 
WHERE people.personID = person_fear.personID 
	AND person_fear.fearID = fears.fearID 
ORDER BY people.name, fears.fear;
+------------------------+-------------+
| name                   | fear        |
+------------------------+-------------+
| Aloysius Snuffleupagus | Black Cats  |
| Aloysius Snuffleupagus | Friday 13th |
| Aloysius Snuffleupagus | Peanut      |
| ik                     | Black Cats  |
| Jane Jones             | Flying      |
| Jane Jones             | Friday 13th |
| Jane Jones             | Peanut      |
| John Johnson           | Black Cats  |
| John Johnson           | Flying      |
| John Johnson           | Friday 13th |
+------------------------+-------------+
10 rows in set (0.00 sec)

SELECT p.name AS Name, f.fear AS Fear 
FROM people AS p, fears AS f, person_fear AS pf
WHERE p.personID = pf.personID 
	AND pf.fearID = f.fearID 
ORDER BY p.name, f.fear;
+------------------------+-------------+
| name                   | fear        |
+------------------------+-------------+
| Aloysius Snuffleupagus | Black Cats  |
| Aloysius Snuffleupagus | Friday 13th |
| Aloysius Snuffleupagus | Peanut      |
| ik                     | Black Cats  |
| Jane Jones             | Flying      |
| Jane Jones             | Friday 13th |
| Jane Jones             | Peanut      |
| John Johnson           | Black Cats  |
| John Johnson           | Flying      |
| John Johnson           | Friday 13th |
+------------------------+-------------+
10 rows in set (0.00 sec)

SELECT p.name AS Name, COUNT(p.name) As Angsten 
FROM people AS p, fears AS f, person_fear AS pf
WHERE p.personID = pf.personID 
	AND pf.fearID = f.fearID 
GROUP BY p.name
ORDER BY p.name;
+------------------------+---------+
| Name                   | Angsten |
+------------------------+---------+
| Aloysius Snuffleupagus |       3 |
| ik                     |       1 |
| Jane Jones             |       3 |
| John Johnson           |       3 |
+------------------------+---------+
4 rows in set (0.00 sec)

SELECT f.fear AS Angst, COUNT(f.fear) As Aantal 
FROM fears AS f, person_fear AS pf
WHERE pf.fearID = f.fearID 
GROUP BY Angst
ORDER BY Aantal DESC, Angst ASC
LIMIT 5;
+-------------+--------+
| Angst       | Aantal |
+-------------+--------+
| Black Cats  |      3 |
| Friday 13th |      3 |
| Flying      |      2 |
| Peanut      |      2 |
+-------------+--------+
4 rows in set (0.00 sec)
