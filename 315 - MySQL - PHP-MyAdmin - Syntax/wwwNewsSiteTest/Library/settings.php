<?php
$debug = true;
ini_set('display_errors', $debug);
error_reporting(E_ALL);

$br = "<br>\n";

define('LIB_ROOT', 		realpath( __DIR__ ) 					);
// ### bijvoorbeeld is dit ~/Sites/deSlager/Library
define('SITE_ROOT',		realpath( LIB_ROOT . '/..' ) 			);
// ### dan is dit ~/Sites/deSlager

// ### we zoeken nu de rootfolder! (public, public_html, htdocs)
$rootfolder = explode('/', substr($_SERVER['SCRIPT_FILENAME'], strlen(SITE_ROOT)+1))[0];
// ### array('public','index.php') dan is dit dus public

define('DOCUMENT_ROOT',	realpath( SITE_ROOT . '/' . $rootfolder )	);
// ### dan is dit dus ~/Sites/deSlager/public
define('IMG_ROOT',		realpath( DOCUMENT_ROOT . '/images' ) 		);
define('INC', 			realpath( LIB_ROOT . '/Includes' ) 			);

// ### bijvoorbeeld is dit ~/Sites/deSlager/public/www2
$web_path = '';
if (DOCUMENT_ROOT !== $_SERVER['DOCUMENT_ROOT'])
	$web_path = substr(DOCUMENT_ROOT , strlen($_SERVER['DOCUMENT_ROOT']));
// ### dan is dit dus 'www2' en anders leeg

$scheme = (isset($_SERVER['REQUEST_SCHEME'])) ? $_SERVER['REQUEST_SCHEME'] . '://' : '//';
// ### bestaat REQUEST_SCHEME dan gebruikt hij het, en anders '//'

define('WEB_URL', 	$scheme . $_SERVER['HTTP_HOST'] . $web_path	);
// ### bijvoorbeeld 'http://www.deslager.nl/'
// ### bijvoorbeeld 'https://www.deslager.nl/www2'
// ### of '//www.deslager.nl/www2' etc

define('IMG_URL', 	WEB_URL . '/images'			);
define('CSS_URL', 	WEB_URL . '/css'			);
define('JS_URL', 	WEB_URL . '/js'				);
define('PAGE_URL', 	WEB_URL . '/pages'			);

// __DIR__ is het path van het huidige bestand!!
// __FILE__ is het path en naam van het huidige bestand!!
// $_SERVER['SCRIPT_FILENAME'] is het volledige path naar je pagina deSlager/public/index.php
// zie PATH_SEPARATOR voor / of \

require_once 'functions.php';