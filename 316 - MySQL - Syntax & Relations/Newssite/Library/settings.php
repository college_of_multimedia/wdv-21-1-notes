<?php
$debug = true;
ini_set('display_errors', $debug);
error_reporting(E_ALL);

// ### start with the server paths ###
define('LIB_ROOT',      realpath( __DIR__ 					) );
define('SITE_ROOT',     realpath( LIB_ROOT . '/..'     		) );
$rootfolder = explode('/', substr( $_SERVER['SCRIPT_FILENAME'], strlen(SITE_ROOT)+1))[0];
define('DOCUMENT_ROOT', realpath( SITE_ROOT . '/' . $rootfolder ) );
define('IMG_ROOT', 		realpath( DOCUMENT_ROOT . '/images' ) );
define('INC',     		realpath( LIB_ROOT . '/Includes'    ) );

// ### Now the Front-end paths ###
$web_path = '';
if ( DOCUMENT_ROOT !== $_SERVER['DOCUMENT_ROOT'] ){
	$web_path = substr( DOCUMENT_ROOT, strlen( $_SERVER['DOCUMENT_ROOT'] ));
}

// ### Verbeterde http/https detectie ###
if ( 	(!empty($_SERVER['REQUEST_SCHEME']) && $_SERVER['REQUEST_SCHEME'] == 'https') ||
		(!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ||
		(!empty($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443') ) {
	// Als ik op 1 van de 3 manieren kan detecteren dat https word gebruikt
	$scheme = 'https://';
} elseif (!empty($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '80' ) {
	// Als niet https, maar ik kan toch op 1 manier detecteren dat http word gebruikt
		$scheme = 'http://';
} else {
	// default scheme als ik niets kan vinden dan maar deze
	$scheme = '//'; 
}

define('WEB_URL', 	$scheme . $_SERVER['HTTP_HOST'] . $web_path);
define('IMG_URL', 	WEB_URL . '/images' );
define('CSS_URL', 	WEB_URL . '/css' 	);
define('JS_URL', 	WEB_URL . '/js' 	);
define('PAGES_URL', WEB_URL . '/pages' 	);

// DATABASE constanten
// Locatie bepaald de values dmv SWITCH!!

if ( !session_id() ) {
	session_start();
}

require_once('functions.php');