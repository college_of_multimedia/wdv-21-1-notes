<?php 

$debug = true;
ini_set('display_errors', (int)$debug);
error_reporting(E_ALL);

$br = "<br>\n";
$nl = "\n";

define ('DB_HOST', 'localhost'	);
define ('DB_USER', 'root'		);
define ('DB_PASS', 'root'		);
define ('DB_NAME', 'cmm_autos'	);

/**
 * Verbind met de database
 * @return \mysqli Object
 */
function connectToDB(){
	$_mysqli = new mysqli( DB_HOST, DB_USER, DB_PASS, DB_NAME );
	if ( $_mysqli->connect_errno ) {
		die ('Failed to connect to database ' . $_mysqli->connect_errno);
	}
	return $_mysqli; // als errno dus 0 is (false)
}

$mysqli = connectToDB();

// die ( var_dump($mysqli) );

###### 1- maak een MySQL verbinding
# https://www.php.net/manual/en/mysqli.construct.php
###### met controle
# https://www.php.net/manual/en/mysqli.connect-error.php
# https://www.php.net/manual/en/mysqli.connect-errno.php
######

// deze maak je maar een keer....

function getAuto($mysqli, $auto_id){
	if ( ! is_int($auto_id) || empty($auto_id) ){
		throw new Exception('Ongeldig Auto nummer');
	}

	###### 2- maak een MySQL query om de data op te halen
	# https://www.php.net/manual/en/mysqli.query.php
	######
	$query = 'SELECT * FROM autos WHERE id=?';

	###### 3- bereid een query voor om uit te voeren
	# https://www.php.net/manual/en/mysqli-stmt.prepare.php
	######

	if ( ! $stmt = $mysqli->prepare($query) ) {
		throw new Exception('Prepare Failed: ' $mysqli->errno);
	}

	###### 4- bind de parameters en geef aan welk type het item is
	# https://www.php.net/manual/en/mysqli-stmt.bind-param.php
	######

	if ( ! $stmt->bind_param('i', $auto_id) ) {
		throw new Exception('Bind Failed: ' $mysqli->errno);
	}

	###### 5- voer de query uit
	# https://www.php.net/manual/en/mysqli-stmt.execute.php
	######

	if ( ! $stmt->execute() ) {
		throw new Exception('Execute Failed: ' $mysqli->errno);
	}

	###### 6- haal de gegevens op
	# https://www.php.net/manual/en/mysqli-stmt.get-result.php
	######

	$result = $stmt->get_result();

	###### 7- controleer of je data hebt gekregen of lege info
	# https://www.php.net/manual/en/mysqli-result.num-rows.php
	######
	
	if ( empty($result->num_rows) ){
		// throw new Exception('Geen auto gevonden');
		return [];
	}

	###### 8- vertaal het resultaat naar een Associatieve Array
	# https://www.php.net/manual/en/mysqli-result.fetch-assoc.php
	######
	
	$auto = $result->fetch_assoc();
	return auto;
}

$auto = getAuto($mysqli, 1);

die (var_dump($auto) );

###### Belangrijke objecten
# https://www.php.net/manual/en/class.mysqli.php
# https://www.php.net/manual/en/class.mysqli-stmt.php
# https://www.php.net/manual/en/class.mysqli-result.php
######

###### Cleanup!
# https://www.php.net/manual/en/mysqli.close.php
# https://www.php.net/manual/en/mysqli-stmt.close.php
# https://www.php.net/manual/en/mysqli-result.free.php
###### 