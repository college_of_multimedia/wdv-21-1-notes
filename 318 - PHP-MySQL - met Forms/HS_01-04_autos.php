<h1>Gecombineerde tabellen weergeven</h1>
<p>
    Op basis van php_mysql_voorbeeld.php <br>
    wil ik uiteindelijk een combinatie zien van autos en merken<br>
</p>

<h2>Opdracht 1</h2>
<p>
	Importeer de database uit php_mysql_voorbeeld.php<br>
</p>

<h2>Opdracht 2</h2>
<p>
	Maak verbinding met deze database en laat de merken zien.<br>
</p>
<?php
$debug = true;
ini_set('display_errors', (int)$debug);
error_reporting(E_ALL);

$br = "<br>\n";
$nl = "\n";
$tb = "\t";

define ('DB_HOST', 'localhost'	);
define ('DB_USER', 'root'		);
define ('DB_PASS', 'root'		);
define ('DB_NAME', 'cmm_autos'	);

/**
 * Verbind met de database
 * @return \mysqli Object
 */
function connectToDB(){
	$_mysqli = new mysqli( DB_HOST, DB_USER, DB_PASS, DB_NAME );
	if ( $_mysqli->connect_errno ) {
		die ('Failed to connect to database: #' . $_mysqli->connect_errno);
	}
	return $_mysqli;
}

$mysqliObject = connectToDB(); // Only once!!

$query = 'SELECT * FROM `merken` ORDER BY `naam`';
echo 'Query: ', $query, $br;

$mysqliResult = $mysqliObject->query($query); // prepare, bind & excecute
echo 'Aantal items gevonden : ', $mysqliResult->num_rows, $br;
// die( var_dump ($mysqliResult->fetch_assoc() ) );


 echo '
<table border="1" cellspacing="0" cellpadding="2">
	<tr>
		<th>id</th>
		<th>merk</th>
		<th>land</th>
	</tr>	
 ', $nl;

while ( $row = $mysqliResult->fetch_assoc() ){
	echo '	<tr>',$nl;
	echo '		<td>' . $row['id'] . '</td>',$nl;
	echo '		<td>' . $row['naam'] . '</td>',$nl;
	echo '		<td>' . $row['land'] . '</td>',$nl;
	echo '	</tr>',$nl;
}
echo '</table>', $nl;

?>
<h2>Opdracht 3</h2>
<p>
	Laat nu alle auto's zien in een overzicht<br>
</p>
<?php
$query = 'SELECT * FROM `autos` ORDER BY `type`';
echo 'Query: ', $query, $br;

?>


<h2>Opdracht 4</h2>
<p>
	Maak een 3e tabel op de html pagina.<br>
    Hier moet een combinatie zichtbaar zijn van de auto's en merken, bv:<br>
<table border='1' cellspacing='0' cellpadding='2'>
	<tr>
		<th>titel</th>
		<th>merk</th>
		<th>land</th>
		<th>type</th>
		<th>kleur</th>
		<th>brandstof</th>
		<th>zitplaatsen</th>
		<th>prijs</th>
	</tr><tr>
		<td>Eerste versie</td>
		<td>Tesla</td>
		<td>Amerika</td>
		<td>Model 3</td>
		<td>zwart</td>
		<td>electra</td>
		<td>5</td>
		<td>590000</td>
	</tr><tr>
		<td>First Sight</td>
		<td>Spyker</td>
		<td>Nederland</td>
		<td>C8 Preliator</td>
		<td>blauw</td>
		<td>Hybrid</td>
		<td>2</td>
		<td>129000</td>
	</tr></table>
</p>

<?php
$query = 'SELECT a.titel, m.naam AS merk, m.land, a.type, a.kleur, a.brandstof, a.zitplaatsen, a.prijs
	FROM `autos` AS a
	LEFT JOIN `merken` AS m
	ON m.id = a.merk_id';	

echo 'Query: ', $query, $br;

$mysqliResult = $mysqliObject->query($query); 
// hergebruiken de variabel met een NIEUW resultaat

echo 'Aantal items = ', $mysqliResult->num_rows, $br;

echo '
<table border="1" cellspacing="0" cellpadding="2">
	<tr>
		<th>titel</th>
		<th>merk</th>
		<th>land</th>
		<th>type</th>
		<th>kleur</th>
		<th>brandstof</th>
		<th>zitplaatsen</th>
		<th>prijs</th>
	</tr>
';

while ( $auto = $mysqliResult->fetch_assoc() ){
	echo '	<tr>',$nl;
	echo '		<td>' . $auto['titel'] . '</td>',$nl;
	echo '		<td>' . $auto['merk'] . '</td>',$nl;
	echo '		<td>' . $auto['land'] . '</td>',$nl;
	echo '		<td>' . $auto['type'] . '</td>',$nl;
	echo '		<td>' . $auto['kleur'] . '</td>',$nl;
	echo '		<td>' . $auto['brandstof'] . '</td>',$nl;
	echo '		<td>' . $auto['zitplaatsen'] . '</td>',$nl;
	echo '		<td>' . $auto['prijs'] . '</td>',$nl;
	echo '	</tr>',$nl;
}


echo '</table>', $br;
?>
<h2>Opdracht 5</h2>
<p>
	Kun je ervoor zorgen dat de sortering van de tabel aangepast door de een get variabele mee te sturen<br>
    Je url wordt dan bijvoorbeeld: http://cmm-lessen.dev/01_autos.php?sort=titel<br>
    of http://cmm-lessen.dev/01_autos.php?sort=prijs
</p>

<h2>Opdracht 6</h2>
<p>
	Kun je ervoor zorgen dat als je op de titel van een kolom klikt je sorteert op die kolom?<br>
</p>


