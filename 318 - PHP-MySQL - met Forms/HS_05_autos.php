<h1>Gecombineerde tabellen weergeven</h1>
<p>
    Op basis van php_mysql_voorbeeld.php <br>
    wil ik uiteindelijk een combinatie zien van autos en merken<br>
</p>
<h2>Opdracht 5</h2>
<p>
	Kun je ervoor zorgen dat de sortering van de tabel aangepast door de een get variabele mee te sturen<br>
    Je url wordt dan bijvoorbeeld: http://cmm-lessen.dev/01_autos.php?sort=titel<br>
    of http://cmm-lessen.dev/01_autos.php?sort=prijs<br>
    <a href="?sort=titel">sort op titel</a><br>
    <a href="?sort=prijs">sort op prijs</a><br>
    <a href="?sort=land">sort op land</a><br>
    <a href="?sort=merk">sort op merk</a><br>
    <a href="?sort=zitplaatsen">sort op zitplaatsen</a><br>
</p>

<?php
$debug = true;
ini_set('display_errors', (int)$debug);
error_reporting(E_ALL);

$br = "<br>\n";
$nl = "\n";
$tb = "\t";

define ('DB_HOST', 'localhost'	);
define ('DB_USER', 'root'		);
define ('DB_PASS', 'root'		);
define ('DB_NAME', 'cmm_autos'	);

/**
 * Verbind met de database
 * @return \mysqli Object
 */
function connectToDB(){
	$_mysqli = new mysqli( DB_HOST, DB_USER, DB_PASS, DB_NAME );
	if ( $_mysqli->connect_errno ) {
		die ('Failed to connect to database: #' . $_mysqli->connect_errno);
	}
	return $_mysqli;
}

$mysqliObject = connectToDB(); // Only once!!

$order = 'titel';

if ( isset( $_GET['sort'] ) ){ 
    // check if its save!!!
	$order = $_GET['sort'];
}

$query = 'SELECT a.titel, m.naam AS merk, m.land, a.type, a.kleur, a.brandstof, a.zitplaatsen, a.prijs
	FROM `autos` AS a
	LEFT JOIN `merken` AS m
	ON m.id = a.merk_id
	ORDER BY ' . $order;	

echo 'Query: ', $query, $br;

$mysqliResult = $mysqliObject->query($query); 
// hergebruiken de variabel met een NIEUW resultaat

echo 'Aantal items = ', $mysqliResult->num_rows, $br;

echo '
<table border="1" cellspacing="0" cellpadding="2">
	<tr>
		<th>titel</th>
		<th>merk</th>
		<th>land</th>
		<th>type</th>
		<th>kleur</th>
		<th>brandstof</th>
		<th>zitplaatsen</th>
		<th>prijs</th>
	</tr>
';

while ( $auto = $mysqliResult->fetch_assoc() ){
	echo '	<tr>',$nl;
	echo '		<td>' . $auto['titel'] . '</td>',$nl;
	echo '		<td>' . $auto['merk'] . '</td>',$nl;
	echo '		<td>' . $auto['land'] . '</td>',$nl;
	echo '		<td>' . $auto['type'] . '</td>',$nl;
	echo '		<td>' . $auto['kleur'] . '</td>',$nl;
	echo '		<td>' . $auto['brandstof'] . '</td>',$nl;
	echo '		<td>' . $auto['zitplaatsen'] . '</td>',$nl;
	echo '		<td>' . $auto['prijs'] . '</td>',$nl;
	echo '	</tr>',$nl;
}


echo '</table>', $br;
?>

<h2>Opdracht 6</h2>
<p>
	Kun je ervoor zorgen dat als je op de titel van een kolom klikt je sorteert op die kolom?<br>
</p>


