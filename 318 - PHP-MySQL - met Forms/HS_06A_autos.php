<h1>Gecombineerde tabellen weergeven</h1>
<p>
    Op basis van php_mysql_voorbeeld.php <br>
    wil ik uiteindelijk een combinatie zien van autos en merken<br>
</p>
<h2>Opdracht 6</h2>
<p>
	Kun je ervoor zorgen dat als je op de titel van een kolom klikt je sorteert op die kolom?<br>
</p>

<?php
$debug = true;
ini_set('display_errors', (int)$debug);
error_reporting(E_ALL);

$br = "<br>\n";
$nl = "\n";
$tb = "\t";

define ('DB_HOST', 'localhost'	);
define ('DB_USER', 'root'		);
define ('DB_PASS', 'root'		);
define ('DB_NAME', 'cmm_autos'	);

/**
 * Verbind met de database
 * @return \mysqli Object
 */
function connectToDB(){
	$_mysqli = new mysqli( DB_HOST, DB_USER, DB_PASS, DB_NAME );
	if ( $_mysqli->connect_errno ) {
		die ('Failed to connect to database: #' . $_mysqli->connect_errno);
	}
	return $_mysqli;
}

$mysqliObject = connectToDB(); // Only once!!

$order = 'titel';

if ( isset( $_GET['sort'] ) ){ 
    // check if its save!!!
	$order = $_GET['sort'];
}

$query = 'SELECT a.titel, m.naam AS merk, m.land, a.type, a.kleur, a.brandstof, a.zitplaatsen, a.prijs
	FROM `autos` AS a
	LEFT JOIN `merken` AS m
	ON m.id = a.merk_id
	ORDER BY ' . $order;	

echo 'Query: ', $query, $br;

$mysqliResult = $mysqliObject->query($query); 
// hergebruiken de variabel met een NIEUW resultaat

echo 'Aantal items = ', $mysqliResult->num_rows, $br;

$auto = $mysqliResult->fetch_assoc(); // haal één auto op!

// stel $auto bestaat niet wat dan???
// laat een foutmelding zien
// bestaat hij wel dan gaan we verder...

echo '
<table border="1" cellspacing="0" cellpadding="2">
	<tr>
		<th><a href="?sort=titel"		>titel</a></th>
		<th><a href="?sort=merk"		>merk</a></th>
		<th><a href="?sort=land"		>land</a></th>
		<th><a href="?sort=type"		>type</a></th>
		<th><a href="?sort=kleur"		>kleur</a></th>
		<th><a href="?sort=brandstof"	>brandstof</a></th>
		<th><a href="?sort=zitplaatsen"	>zitplaatsen</a></th>
		<th><a href="?sort=prijs"		>prijs</a></th>
	</tr>
';

do {
	echo '	<tr>',$nl;
	echo '		<td>' . $auto['titel'] . '</td>',$nl;
	echo '		<td>' . $auto['merk'] . '</td>',$nl;
	echo '		<td>' . $auto['land'] . '</td>',$nl;
	echo '		<td>' . $auto['type'] . '</td>',$nl;
	echo '		<td>' . $auto['kleur'] . '</td>',$nl;
	echo '		<td>' . $auto['brandstof'] . '</td>',$nl;
	echo '		<td>' . $auto['zitplaatsen'] . '</td>',$nl;
	echo '		<td>' . $auto['prijs'] . '</td>',$nl;
	echo '	</tr>',$nl;
} while ( $auto = $mysqliResult->fetch_assoc() );


echo '</table>', $br;
?>
