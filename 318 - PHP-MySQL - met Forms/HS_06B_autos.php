<h1>Gecombineerde tabellen weergeven</h1>
<p>
    Op basis van php_mysql_voorbeeld.php <br>
    wil ik uiteindelijk een combinatie zien van autos en merken<br>
</p>
<h2>Opdracht 6</h2>
<p>
	Kun je ervoor zorgen dat als je op de titel van een kolom klikt je sorteert op die kolom?<br>
	Dit keer met gegeneerde table headers en data<br>
</p>

<?php
$debug = true;
ini_set('display_errors', (int)$debug);
error_reporting(E_ALL);

$br = "<br>\n";
$nl = "\n";
$tb = "\t";

define ('DB_HOST', 'localhost'	);
define ('DB_USER', 'root'		);
define ('DB_PASS', 'root'		);
define ('DB_NAME', 'cmm_autos'	);

/**
 * Verbind met de database
 * @return \mysqli Object
 */
function connectToDB(){
	$_mysqli = new mysqli( DB_HOST, DB_USER, DB_PASS, DB_NAME );
	if ( $_mysqli->connect_errno ) {
		die ('Failed to connect to database: #' . $_mysqli->connect_errno);
	}
	return $_mysqli;
}

$mysqliObject = connectToDB(); // Only once!!

$order = 'titel';

if ( isset( $_GET['sort'] ) ){ 
    // check if its save!!!
	$order = $_GET['sort'];
}

$query = 'SELECT a.id, a.titel, m.naam AS merk, m.land, a.type, a.kleur, a.brandstof, a.zitplaatsen, a.prijs
	FROM `autos` AS a
	LEFT JOIN `merken` AS m
	ON m.id = a.merk_id
	ORDER BY ' . $order;	

echo 'Query: ', $query, $br;

$mysqliResult = $mysqliObject->query($query); 
// hergebruiken de variabel met een NIEUW resultaat

echo 'Aantal items = ', $mysqliResult->num_rows, $br;

// stel num_rows bestaat niet wat dan???
// laat een foutmelding zien
// bestaat hij wel dan gaan we verder...

$auto = $mysqliResult->fetch_assoc(); // haal eerst één auto op! om de table headers te kunnen maken:
$tableHeader = '<table border="1" cellspacing="0" cellpadding="2">' . $nl;
$tableHeader .= '	<tr>' . $nl;
foreach ($auto as $key => $value){
	$tableHeader .= '		<th><a href="?sort=' . $key . '">' . $key . '</a></th>' . $nl;
}
$tableHeader .= '	</tr>' . $nl;
echo $tableHeader;

// nu van die eerste auto de "inhoud" laten zien, en dan herhalen tot de autos op zijn

do {
	echo '	<tr>', $nl;
	foreach ($auto as $key => $value){
		echo '		<td>' . $value . '</td>', $nl;
	}
	echo '	</tr>',$nl;
} while ( $auto = $mysqliResult->fetch_assoc() );

echo '</table>', $nl;
