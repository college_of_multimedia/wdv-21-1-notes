<h1>Gecombineerde tabellen weergeven</h1>
<p>
    Op basis van php_mysql_voorbeeld.php <br>
    wil ik uiteindelijk een combinatie zien van autos en merken<br>
</p>
<h2>Opdracht 6</h2>
<p>
	Kun je ervoor zorgen dat als je op de titel van een kolom klikt je sorteert op die kolom?<br>
	Dit keer met gegeneerde table headers en data en mogelijkheid tot ascending en descending<br>
</p>

<?php
$debug = true;
ini_set('display_errors', (int)$debug);
error_reporting(E_ALL);

$br = "<br>\n";
$nl = "\n";
$tb = "\t";

define ('DB_HOST', 'localhost'	);
define ('DB_USER', 'root'		);
define ('DB_PASS', 'root'		);
define ('DB_NAME', 'cmm_autos'	);

/**
 * Verbind met de database
 * @return \mysqli Object
 */
function connectToDB(){
	$_mysqli = new mysqli( DB_HOST, DB_USER, DB_PASS, DB_NAME );
	if ( $_mysqli->connect_errno ) {
		die ('Failed to connect to database: #' . $_mysqli->connect_errno);
	}
	return $_mysqli;
}

$mysqliObject = connectToDB(); // Only once!!

$order = 'titel';

$direction = 'ASC';

$zoek = '';

if ( isset( $_GET['sort'] ) ){ 
    // check if its save!!!
	$order = $_GET['sort'];
}

if ( isset( $_GET['zoek'] ) ){ 
    // check if its save!!!
	$zoek = $_GET['zoek'];
}

if ( isset( $_GET['direction'] ) ){ 
    // check if its save!!!
	$direction = strtoupper( $_GET['direction'] );
}

$zoekLike = '%' . $zoek . '%';

$query = 'SELECT a.titel, m.naam AS merk, m.land, a.type, a.kleur, a.brandstof, a.zitplaatsen, a.prijs
	FROM `autos` AS a
	LEFT JOIN `merken` AS m
	ON m.id = a.merk_id
	WHERE m.naam LIKE ? OR a.titel LIKE ? OR a.type LIKE ? 
	ORDER BY ' . $order . ' ' . $direction;	

// echo 'Query: ', $query, $br;

### Stap 3 prepare ###
if ( ! $stmt = $mysqliObject->prepare($query) ){
	throw new Exception('Prepare Failed: ' . $mysqliObject->errno);
}
### Stap 4 prepare ###
if ( ! $stmt->bind_param('sss', $zoekLike, $zoekLike, $zoekLike) ){
	throw new Exception('Bind Failed: ' . $mysqliObject->errno);
}
### Stap 5 Execute ###
if ( ! $stmt->execute() ){
	throw new Exception('Execute Failed: ' . $mysqliObject->errno);
}
### Stap 6 Haal de gegevens op ###
$mysqliResult = $stmt->get_result();

if ( ! $mysqliResult->num_rows ){
	die('Geen autos gevonden, zoek opnieuw');
}

echo 'Aantal items = ', $mysqliResult->num_rows, $br;

$direction = 'ASC' == $direction ? 'DESC' : 'ASC';

$auto = $mysqliResult->fetch_assoc(); // haal eerst één auto op! om de table headers te kunnen maken:
$tableHeader = '<table border="1" cellspacing="0" cellpadding="2">' . $nl;
$tableHeader .= '	<tr>' . $nl;
foreach ($auto as $key => $value){
	$tableHeader .= '		<th><a href="?sort=' . $key . '&direction=' . strtolower( $direction ) . '&zoek=' . $zoek . '">' . $key . '</a></th>' . $nl;
}
$tableHeader .= '	</tr>' . $nl;
echo $tableHeader;

// nu van die eerste auto de "inhoud" laten zien, en dan herhalen tot de autos op zijn

do {
	echo '	<tr>', $nl;
	foreach ($auto as $key => $value){
		echo '		<td>' . $value . '</td>', $nl;
	}
	echo '	</tr>',$nl;
} while ( $auto = $mysqliResult->fetch_assoc() );

echo '</table>', $nl;

echo 'show all records';
