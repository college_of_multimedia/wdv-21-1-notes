<?php
define ('PW_LENGTH', 32);

// die (function_exists("hash_pbkdf2")); // test of de server het snap ja/nee

/**
 * newPassword genereert nieuw salt en hashed het wachtwoord
 * en returned beide via een Array
 * @uses 	integer	PW_LENGTH 	bepaalt de lengte van de salt en hash
 * @param 	string	$pw_raw		String met Wachtwoord  
 * @return 	array         		['salt' => 'gegenereerde salt', 'hash' => 'hashed password']
 */
function newPassword( $pw_raw ){
	$pw_strongSalt = false;
	$pw_salt = bin2hex( openssl_random_pseudo_bytes ( PW_LENGTH / 2, $pw_strongSalt ) );
	$pw_hash = pbkdf2( 'sha256', $pw_raw, $pw_salt, PW_LENGTH / 2, PW_LENGTH / 2, false );
	if (!$pw_strongSalt) die('weak salt');
	return [	'salt' => $pw_salt,
				'hash' => $pw_hash ];
}


/**
 * currentPassword hashed het wachtwoord met bestaand salt
 * @uses 	integer	PW_LENGTH 	bepaalt de lengte van de salt en hash
 * @param 	string	$pw_raw		String met Wachtwoord 
 * @param 	string	$pw_salt	String met bestaand salt 
 * @return  string           	Hash van het wachtwoord
 */
function currentPassword($pw_raw, $pw_salt){
	$pw_hash = pbkdf2('sha256', $pw_raw, $pw_salt, PW_LENGTH / 2, PW_LENGTH / 2, false);
	return $pw_hash;
}


/**
 * pbkdf2 genereert een 'veilige' hash (indien mogelijk)
 * @param  string  $algorithm  keuze van algoritme bijv: md5 of sha1 of sha256
 * @param  string  $password   String met Wachtwoord
 * @param  string  $salt       String met bestaand salt
 * @param  integer $count      aantal iteraties voor de encryptie
 * @param  integer $key_length gewenste lengte van de hash
 * @param  boolean $raw_output true = binaire uitvoer, false = hex uitvoer (default)
 * @return string              Hash van het wachtwoord
 */
function pbkdf2($algorithm, $password, $salt, $count, $key_length, $raw_output = false) {
    $algorithm = strtolower($algorithm);
    if(!in_array($algorithm, hash_algos(), true))
        trigger_error('PBKDF2 ERROR: Invalid hash algorithm.', E_USER_ERROR);
    
    if($count <= 0 || $key_length <= 0)
        trigger_error('PBKDF2 ERROR: Invalid parameters.', E_USER_ERROR);

    if (function_exists("hash_pbkdf2")) {
        // The output length is in NIBBLES (4-bits) if $raw_output is false!
        if (!$raw_output) {
            $key_length = $key_length * 2;
        }
        return hash_pbkdf2($algorithm, $password, $salt, $count, $key_length, $raw_output);
    }

    // als de modernere hash_pbkdf2 niet bestaat gebruik dan hash_hmac als alternatief!

    $hash_length = strlen(hash($algorithm, "", true));
    $block_count = ceil($key_length / $hash_length);

    $output = "";
    for($i = 1; $i <= $block_count; $i++) {
        // $i encoded as 4 bytes, big endian.
        $last = $salt . pack("N", $i);
        // first iteration
        $last = $xorsum = hash_hmac($algorithm, $last, $password, true);
        // perform the other $count - 1 iterations
        for ($j = 1; $j < $count; $j++) {
            $xorsum ^= ($last = hash_hmac($algorithm, $last, $password, true));
        }
        $output .= $xorsum;
    }

    if($raw_output)
        return substr($output, 0, $key_length);
    else
        return bin2hex(substr($output, 0, $key_length));
}

### einde script ### 
// Code hieronder is debug, kill in live site

$pw_raw = 'worst';
$hash = newPassword($pw_raw);

echo '<pre>';

echo  "New Pasword:\t{$pw_raw}
New Salt:\t{$hash['salt']}
New Hash:\t{$hash['hash']}<hr>\n";
// in de database opslaan $hash['salt'] en $hash['hash']

// jasper zijn zout
$storedSalt = 'ee72022df6ea874fdf87163bf32955f3';
// jasper zijn Hash
$storedHash = '1d4a69892db6e1334514dfcb1c7997a1';
// als dus $storedHash === $calculatedHash dan is de inlog geldig
$calculatedHash = currentPassword($pw_raw, $storedSalt);


echo  "Old Pasword:\t{$pw_raw}
Stored Salt:\t{$storedSalt}
Stored Hash:\t{$storedHash}
Calc Hash:\t{$calculatedHash}<hr>\n";
