<?php

$debug = true;

ini_set('display_errors', $debug);
error_reporting(E_ALL);

if ( !session_id() ) {
    session_start();
}

// ### start met de server paden ###
define('LIB_ROOT',      realpath( __DIR__                   ) );
define('SITE_ROOT',     realpath( LIB_ROOT . '/..'          ) );
$rootfolder = explode('/', substr( $_SERVER['SCRIPT_FILENAME'], strlen(SITE_ROOT)+1))[0];
define('DOCUMENT_ROOT', realpath( SITE_ROOT . '/' . $rootfolder ) );
define('IMG_ROOT',      realpath( DOCUMENT_ROOT . '/images' ) );
define('INC',           realpath( LIB_ROOT . '/Includes'    ) );

// ### Dan de Front-end paden ###
$web_path = '';
if ( DOCUMENT_ROOT !== $_SERVER['DOCUMENT_ROOT'] ){
    $web_path = substr( DOCUMENT_ROOT, strlen( $_SERVER['DOCUMENT_ROOT'] ));
}

// ### Verbeterde http/https detectie ###
if (    (!empty($_SERVER['REQUEST_SCHEME']) && $_SERVER['REQUEST_SCHEME'] == 'https') ||
        (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ||
        (!empty($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443') ) {
    // Als ik op 1 van de 3 manieren kan detecteren dat https word gebruikt
    $scheme = 'https://';
} elseif (  (!empty($_SERVER['REQUEST_SCHEME']) && $_SERVER['REQUEST_SCHEME'] == 'http') ||
            (!empty($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '80' ) ) {
    // Als niet https, maar ik kan toch op 2 manieren detecteren dat http word gebruikt
        $scheme = 'http://';
} else {
    // default scheme als ik niets kan vinden dan maar deze
    $scheme = '//'; 
}

define('WEB_URL',   $scheme . $_SERVER['HTTP_HOST'] . $web_path);
define('IMG_URL',   WEB_URL . '/images' );
define('CSS_URL',   WEB_URL . '/css'    );
define('JS_URL',    WEB_URL . '/js'     );
define('PAGES_URL', WEB_URL . '/pages'  );

// DATABASE constanten
// #### Zoek alle overige defines op door de server te identificeren ####
// Locatie bepaald de values dmv SWITCH!!

switch ($_SERVER['SERVER_NAME']) {
    case 'cmm-students.localhost':      // #### Nakijk Server (teacher Homestead) ####
        define('DB_HOST'        , 'localhost'                       );
        define('DB_NAME'        , 'cmmdatabase'                     );
        define('DB_USER'        , 'homestead'                       );
        define('DB_PASS'        , 'secret'                          );
        break; 
            
    case 'student.newssite.test':           // #### test Server (student thuis) ####
        define('DB_HOST'        , 'localhost'                       );
        define('DB_NAME'        , 'cmm_news'                        );
        define('DB_USER'        , 'root'                            );
        define('DB_PASS'        , 'root'                            );
        break;

     case 'www.newssite.nl':                // #### Live Server (echte site!) ####
        define('DB_HOST'        , 'localhost'                       );
        define('DB_NAME'        , 'news_db'                         );
        define('DB_USER'        , 'gdeyyxunhyugwe'                  );
        define('DB_PASS'        , 'jd4r73r83'                       );
        break;

    default:                                // #### Test Server ( student @ cmm) ####
        define('DB_HOST'        , 'localhost'                       );
        define('DB_NAME'        , 'cmm_news_2021'                   );
        define('DB_USER'        , 'root'                            );
        define('DB_PASS'        , 'root'                            );
}

// laad de algemene scripts
require_once( LIB_ROOT . '/functions.php' );