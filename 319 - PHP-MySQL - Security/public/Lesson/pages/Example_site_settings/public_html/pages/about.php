<?php 
############################
### DE PAGINA VARIABELEN ###
$active = 'about';
$page_title = 'About us';
############################
require_once('../../Library/settings.php' ); 
// Altijd eerste require of include via relatief pad! --> 
require_once( INC . '/head.php' ); 
?>

<!--
<?php
$nl = "\n";

echo 'Dit document'. $nl;
echo '$_SERVER["DOCUMENT_ROOT"] : ' . $_SERVER["DOCUMENT_ROOT"] . $nl;
echo '$_SERVER["HTTP_HOST"]     : ' . $_SERVER["HTTP_HOST"] . $nl;
echo '__DIR__                   : ' . __DIR__ . $nl;
echo '__FILE__                  : ' . __FILE__ . $nl . $nl;

echo 'Roots : De site absoluut'. $nl;
echo 'LIB_ROOT                  : ' . LIB_ROOT . $nl;
echo 'SITE_ROOT                 : ' . SITE_ROOT . $nl;
echo 'DOCUMENT_ROOT             : ' . DOCUMENT_ROOT . $nl;
echo 'IMG_ROOT                  : ' . IMG_ROOT . $nl . $nl;

echo 'Urls : De site via web'. $nl;
echo '$web_path                 : ' . $web_path .$nl;
echo 'WEB_URL                   : ' . WEB_URL . $nl;
echo 'IMG_URL                   : ' . IMG_URL . $nl;
echo 'JS_URL                    : ' . JS_URL . $nl;
echo 'CSS_URL                   : ' . CSS_URL . $nl . $nl;

/* Set timezone to Dutch */
date_default_timezone_set('Europe/Amsterdam');
/* Set locale to Dutch */  
setlocale(LC_TIME, 'nl_NL.UTF-8', 'nl_NL', 'NL_nl', 'nl', 'nld_nld', 'nld', 'nld_NLD', 'nl_NL.ISO8859-1', 'Dutch_Netherlands', 'Dutch');
// Note: indien geinstaleerd op de server!!!
// sudo apt-get install language-pack-nl          

?>
-->
    
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Web Developer</a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li <?=activeMenu('home')?> ><a href="<?=WEB_URL?>/index.php">Home</a></li>
                    <li <?=activeMenu('about')?> ><a href="<?=WEB_URL?>/pages/about.php">About</a></li>
                    <li <?=activeMenu('contact')?> ><a href="#contact">Contact</a></li>
                 </ul>
                <form class="navbar-form navbar-right">
                    <div class="form-group">
                        <input type="text" placeholder="Email" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="password" placeholder="Password" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-success">Sign in</button>
                </form>
            </div>
            <!--/.navbar-collapse -->
        </div>
    </div>

    <div id="jumbotronAbout" class="jumbotron">
        <div class="container">
            <h1>About</h1>

            <p>Dit is de pagina over mij</p>

        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <h2>Bericht 1</h2>

                <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris
                    condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis
                    euismod. Donec sed odio dui. </p>

                <p><a class="btn btn-default" href="#">View details &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Bericht 2</h2>

                <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris
                    condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis
                    euismod. Donec sed odio dui. </p>

                <p><a class="btn btn-default" href="#">View details &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Bericht 3</h2>

                <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula
                    porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut
                    fermentum massa justo sit amet risus.</p>

                <p><a class="btn btn-default" href="#">View details &raquo;</a></p>
            </div>
        </div>

        <hr>
<?php

/* get current date */
$today = getdate();    
$now = time(); // $now == $today[0];

debuggen( $today,   __FILE__ , __LINE__ );
debuggen( $now );
        
echo strftime("%D", $now). $br ;

echo strftime("%c", $now). $br ;
          
echo strftime("%A %e %B %Y %R", $now). $br ;
// https://www.php.net/manual/en/function.strftime.php

?>
<?php require_once( INC . '/footer.php' );