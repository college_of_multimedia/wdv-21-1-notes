<?php
/*
 * Start de sessie omdat ik die verderop bij de controles nodig heb.
 */
session_start();

?><!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="UTF-8">
    <title>Captcha test</title>

    <style type="text/css">
        body {
            font  : 1.5em sans-serif;
            color : #000;
        }

        .captcha_holder {
            border     : none !important;
            box-shadow : solid 5px #000;
            background : #eee;
            padding    : 25px 50px;
            width      : 50%;
            margin     : 15px auto;
        }

        .alert {
            width       : 100%;
            font-weight : bold;
            text-align  : center;
            color       : #0f0;
        }

        .warning {
            color : #f00;
        }
    </style>
    <script language="javascript">
		function setFocus() {
			document.captcha_form.ch_number.focus()
		}
    </script>

<body onload="setFocus();">

<div class="captcha_holder">
    <?php
    /*
     * Check if the code is valid
     */
    if (isset($_POST['ch_number']) && ! empty($_POST['ch_number'])) {
        $key    = substr($_SESSION['key'], 0, 5);
        $number = trim($_POST['ch_number']);
        if ($number != $key) {
            echo '<div class="alert warning">Validation string not valid! Please try again!</div>';
        } else {
            echo '<div class="alert">Your string is valid!</div>';
        }
    }
    ?>
    <form name="captcha_form" method="post">
        Afbeelding, gegenereerd door php:<br>
        <img src="php_captcha.php" width="100px"><br>
        Vul de bovenstaande code in:<br>
        <input type="text" name="ch_number" id="ch_number">
        <button>Controleer de code</button>
    </form>
</div>

</body>
</html>