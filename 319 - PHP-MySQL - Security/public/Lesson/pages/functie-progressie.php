<?php


function showMyValue_0($value = '')
{
    return ( ! is_string($value)) ? 'Geen string' : (strlen($value) > 5) ? 'String groter dan 5 karakters' : 'Dit is mijn String: ' . $value;
}


function showMyValue_1($value = '')
{
    if ( ! is_string($value)) {
        return 'Geen string';
    } elseif (strlen($value) > 5) {
        return 'String groter dan 5 karakters';
    } else {
        return 'Dit is mijn String: ' . $value;
    }
}


function showMyValue_2($value = '')
{
    if ( ! is_string($value)) {
        return 'Geen string';
    }

    if (strlen($value) > 5) {
        return 'String groter dan 5 karakters';
    }

    return 'Dit is mijn String: ' . $value;
}


/**
 * Dit is een mooie functie die iets speciaals doet.
 *
 * @param string $value
 *
 * @return string
 */
function showMyValue_3($value = '')
{
    /*
     * Is het echt een string
     */
    if ( ! is_string($value)) {
        return 'Geen string';
    }

    /*
     * Is de string lang genoeg
     */
    if (strlen($value) > 5) {
        return 'String groter dan 5 karakters';
    }

    return 'Dit is mijn String: ' . $value;
}