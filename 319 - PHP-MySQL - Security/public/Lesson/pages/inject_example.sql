# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.17)
# Database: cmm_wd319_example
# Generation Time: 2017-12-04 14:45:25 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table contact_form
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contact_form`;

CREATE TABLE `contact_form` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `language` varchar(25) NOT NULL DEFAULT '',
  `instance` varchar(255) NOT NULL DEFAULT '',
  `mailTo` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `startText` text NOT NULL,
  `sendOkText` text NOT NULL,
  `sendErrorText` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `contact_form` WRITE;
/*!40000 ALTER TABLE `contact_form` DISABLE KEYS */;

INSERT INTO `contact_form` (`id`, `language`, `instance`, `mailTo`, `title`, `startText`, `sendOkText`, `sendErrorText`)
VALUES
	(1,'NL','contact','jasper@cmm.nl','Informatie aanvraag','Vul het onderstaande formulier in svp','Uw bericht is verzonden, u krijgt&nbsp;altijd antwoord van&nbsp;ons.','<p><em>Er is iets mis gegaan in de verzending.<br /><br />Probeer het nogmaals svp !<br /></em></p>'),
	(2,'NL','contact','info@dragonet.nl','Informatie aanvraag','Vul het onderstaande formulier in svp','Uw bericht is verzonden, u krijgt&nbsp;altijd antwoord van&nbsp;ons.','<p><em>Er is iets mis gegaan in de verzending.<br /><br />Probeer het nogmaals svp !<br /></em></p>');

/*!40000 ALTER TABLE `contact_form` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table contact_requests
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contact_requests`;

CREATE TABLE `contact_requests` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `instance` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table text_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `text_category`;

CREATE TABLE `text_category` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `language` varchar(5) NOT NULL DEFAULT '',
  `parent_id` int(25) NOT NULL DEFAULT '0',
  `instance` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `orderNum` int(5) NOT NULL DEFAULT '0',
  `publish` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `text_category` WRITE;
/*!40000 ALTER TABLE `text_category` DISABLE KEYS */;

INSERT INTO `text_category` (`id`, `language`, `parent_id`, `instance`, `name`, `title`, `orderNum`, `publish`)
VALUES
	(1,'NL',0,'home','home','Home ',1,'1'),
	(2,'NL',0,'about','about','over ons',2,'1'),
	(3,'NL',0,'products','products','Producten',3,'1'),
	(4,'NL',0,'price','price','Tarieven',4,'1'),
	(5,'NL',0,'downloads','downloads','Downloads',5,'1'),
	(6,'NL',0,'links','links','Links',6,'1'),
	(7,'NL',0,'contact','contact','Contact ',7,'1'),
	(10,'NL',1000,'disclaimer','disclaimer','Disclaimer ',1,'1');

/*!40000 ALTER TABLE `text_category` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_rights
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_rights`;

CREATE TABLE `user_rights` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `user_id` int(25) NOT NULL DEFAULT '0',
  `instance` varchar(255) NOT NULL DEFAULT '',
  `module` varchar(255) NOT NULL DEFAULT '',
  `category` varchar(255) NOT NULL DEFAULT '',
  `rights` int(25) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `user_rights` WRITE;
/*!40000 ALTER TABLE `user_rights` DISABLE KEYS */;

INSERT INTO `user_rights` (`id`, `user_id`, `instance`, `module`, `category`, `rights`)
VALUES
	(1,0,'','','',5);

/*!40000 ALTER TABLE `user_rights` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `instance` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `login` varchar(10) NOT NULL DEFAULT '',
  `pass` varchar(255) NOT NULL,
  `level` int(5) NOT NULL DEFAULT '10',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `instance`, `username`, `email`, `login`, `pass`, `level`)
VALUES
	(1,'cmm','jasper','jasper@jasper.nl','jasper','a08ab7bd790184c16f246a9be8db5172',63),
	(2,'cmm','bert','bert@bert.nl','bert','02b0732024cad6ad3dc2989bc82a1ef5',63);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
