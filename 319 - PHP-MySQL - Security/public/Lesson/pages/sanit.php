<?php

/**
 * De sanit_input functie maakt een waarde die je niet zelf aangemaakt hebt veiliger
 * Kan gebruikt worden voor één waarde of in een loop
 *
 * @param        $value
 * @param string $type
 *
 * @return bool|int|string
 */
function sanit_input($value, $type = 'string')
{
    /*
     * Is return een int
     */
    if ('int' === strtolower($type)) {
        return ( int )$value;
    }

    /*
     * Ik wil graag een string terug krijgen
     */
    if ('string' === strtolower($type)) {
        $value = trim($value);
		$value = htmlspecialchars($value);

        // remove the union, concat and information schema to prevent hacking
        $value = preg_replace(
            '/(union[\s\+]*?select)|(concat(_ws)?\s*?\()|(information_schema)| or /i',
            '',
            $value);

        return stripslashes($value);
    }

    return false;
}