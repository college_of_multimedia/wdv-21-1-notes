<?php
// de NewsController
require_once(DOCUMENT_ROOT . '/models/NewsModel.php');

class NewsController
{
	
	/**
	 * @var array
	 */
	protected $_newsArray = [];

	/**
	 * Voeg een nieuw bericht toe
	 * @param String $title   
	 * @param String $message 
	 */
	public function addNewsItem($title, $message){
		$newsItem = new NewsModel();
		$newsItem->setTitle($title);
		$newsItem->setMessage($message);
		$newsItem->setId( count($this->_newsArray));
		// in het echt roep een method van het model op om dit bericht te bewaren in de database
		$this->_newsArray[] = $newsItem;
	}

	/**
	 * Toon een bericht via de view van het enkele newsitem
	 * @param  Integer $id 
	 * @return newsItem Object    
	 */
	public function showNewsItem($id){
		$newsItem = $this->getNewsItem($id);
		require DOCUMENT_ROOT . '/views/News_item.php';
	}
	
	/**
	 * Toon een Array met berichten via de view van het overzicht newsitem
	 * @return Array [newsItem Objects]    
	 */
	public function showNewsItems(){
		$newsItems = $this->getNewsItems();
		require DOCUMENT_ROOT . '/views/News_overview.php';
	}

	/**
	 * Haal een bericht op en return deze
	 * @param  Integer $id 
	 * @return newsItem Object    
	 */
	public function getNewsItem($id){
		if (empty( $this->_newsArray[$id] ) ){
			return new NewsModel();
		}
		return $this->_newsArray[$id];
	}
	
	/**
	 * Haal alle berichten op en stop deze in een Array
	 * @return Array [newsItem Objects]    
	 */
	public function getNewsItems(){
		if (empty( $this->_newsArray ) ){
			throw new Exception('Geen berichten gevonden');
		}
		return $this->_newsArray;
	}
}





