<?php

require_once 'ProtectedUser.php'; ## Dependency!!

class ExtendedUser extends ProtectedUser { // Class extends dus de Base Class
	/**
	 * @var integer
	 */
	private $_premiumId;

	/**
	 * Constructor van de ExtendedUser overruled de constructor van de ProtectedUser
	 * @param string $username 
	 * @param integer $id       
	 */
	public function __construct($username, $id){
		$this->setData($username, $id);
	}

	public function setData($username, $id){
		$this->setUsername( $username );   ## dus property van de ProtectedUser
		$this->_premiumId = $id; 		## property van de ExtendedUser
	}

	public function getUsername(){
		return $this->_username . ' heeft als id: ' . $this->_premiumId;
	}

}
