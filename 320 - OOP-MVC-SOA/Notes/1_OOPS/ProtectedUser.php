<?php

class ProtectedUser {	## Base Class

	/**
	 * de gebruikersnaam
	 * @var string
	 */
	protected $_username;

	/**
	 * user constructor
	 * @param string $name 
	 */
	public function __construct( $name ){
		$this->setUsername( $name );
	}

	/**
	 * set the username
	 * @param string $name 
	 */
	public function setUsername( $name ) {
		## lengte, verboden karakters, naam bestaat al?
		$this->_username = $name;
	}
	
	/**
	 * get the username
	 * @return string
	 */
	public function getUsername(){
		return $this->_username;
	}
}