<?php
$debug = true;

ini_set('display_errors', (int)$debug);
error_reporting(E_ALL);

$nl = "\n";
$br = '<br>' . $nl;

echo '<h2>OOP\'s 2 </h2>';

class User {

	/**
	 * de gebruikersnaam
	 * @var string
	 */
	protected $_username;

	/**
	 * user constructor
	 * @param string $name 
	 */
	public function __construct( $name ){
		## deze functie wordt automatisch aangeroepen bij het instancieren van deze class
		$this->setUsername( $name );
	}

	/**
	 * set the username
	 * @param string $name 
	 */
	public function setUsername( $name ) {
		## lengte, verboden karakters, naam bestaat al?
		$this->_username = $name;
	}
	
	/**
	 * get the username
	 * @return string
	 */
	public function getUsername(){
		return $this->_username;
	}
}

$user1 = new User('Bert');
$user2 = new User('Truus');
// echo $user2->getUsername();

echo $user1->getUsername() . ' en ' . $user2->getUsername() . $br;

