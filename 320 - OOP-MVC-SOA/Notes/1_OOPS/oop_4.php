<?php
$debug = true;

ini_set('display_errors', (int)$debug);
error_reporting(E_ALL);

$nl = "\n";
$br = '<br>' . $nl;

echo '<h2>OOP\'s 2 </h2>';

require_once 'ProtectedUser.php';
require_once 'ExtendedUser.php';

$user1 = new ProtectedUser('Bert');
$user2 = new ExtendedUser('Truus', 31);
// echo $user2->getUsername();

echo $user1->getUsername() . ' en ' . $user2->getUsername() . $br;

$user1->setUsername('Bertus');
$user2->setUsername('Geertruida');

echo $user1->getUsername() . ' en ' . $user2->getUsername() . $br;

class BaseUser {}
class GuestUser extends BaseUser {}
class RegisteredUser extends BaseUser {}
class CustomerUser extends RegisteredUser {}
