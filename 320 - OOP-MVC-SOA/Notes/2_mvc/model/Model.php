<?php

include_once('model/Book.php');

class Model {

	public function getBook( $title ) {
		$allBooks = $this->getBookList();
		return $allBooks[ $title ];
	}

	public function getBookList() {
		// in het echt wordt hier de data opgehaald uit de database!
		// nu hardcoded 
		return [
			'Jungle Book'		=> new Book('Jungle Book','R. Kippling','A classic book.'),
			'Moonwalker'		=> new Book('Moonwalker','J. Moonwalker','Dance, dance.'),
			'PHP for Dummies'	=> new Book('PHP for Dummies','Some Smart Guy','Start here with PHP.'),
		];

	}

} 