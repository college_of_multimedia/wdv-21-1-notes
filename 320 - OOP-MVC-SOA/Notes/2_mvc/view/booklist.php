<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>BookList</title>
</head>
<body>
	<p>Hier de lijst van onze boeken</p>
	<table>
		<tr>
			<th>Titel</th>
			<th>Auteur</th>
		</tr>
<?php		
	foreach ($books as $title => $book) {
		echo '<tr><td><a href="?book='. $book->title . '">' . $book->title . '</a></td><td>' . $book->author . '</td></tr>';
	}

?>
	</table>
</body>
</html>