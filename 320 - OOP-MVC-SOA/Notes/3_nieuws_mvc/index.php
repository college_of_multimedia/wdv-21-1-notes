<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
<?php

define ('DOCUMENT_ROOT', __DIR__);

require_once 'controllers/NewsController.php';

$newsController = new NewsController();

$newsController->addNewsItem('Titel 1', 'Bericht nummer 1'); // 0
$newsController->addNewsItem('Titel 2', 'Bericht nummer 2'); // 1
$newsController->addNewsItem('Titel 4', 'Bericht nummer 4'); // 2
$newsController->addNewsItem('Titel 5', 'Bericht nummer 5'); // 2
$newsController->addNewsItem('Titel 6', 'Bericht nummer 6'); // 2

if (! isset($_GET['id']) ){
	$newsController->showNewsItems();
} else {
	$newsController->showNewsItem( $_GET['id'] );
}
?>
</body>
</html>
