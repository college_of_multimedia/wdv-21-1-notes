<?php

class Auto
{
    protected $_wielen = 4;
    protected $_kleur = 'rood';

    public function __construct($wielen = 2, $kleur = 'geel')
    {
        $this->setWielen( $wielen );
        $this->setKleur( $kleur );
    }


    public function setKleur($kleur)
    {
        // valideer!!
        $this->_kleur = $kleur;
    }
  
    public function setWielen($aantal)
    {
        // Valideer!!
        $this->_wielen = $aantal;
    }

    public function getKleur()
    {
        return $this->_kleur;
    }
    
    public function getWielen()
    {
        return $this->_wielen;
    }
  
}
