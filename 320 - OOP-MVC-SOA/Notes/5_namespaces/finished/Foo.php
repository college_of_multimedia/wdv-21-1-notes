<?php

namespace Acme\Tools;

use DateTime;
use Exception;

class Foo {
    public function doAwesomeThings()  {
        echo "Hallo Luisteraars! <br>";

        $dt = new DateTime();

        // DateTime bestaat niet in deze Namespace, dus moet je use DateTime gebruiken 
        print_r($dt);
    }
}
