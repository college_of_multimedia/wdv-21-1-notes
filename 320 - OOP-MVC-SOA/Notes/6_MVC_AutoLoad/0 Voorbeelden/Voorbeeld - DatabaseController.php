<?php

/**
 * Voorbeeld 3, OOP met __construct()
 * Class DatabaseController
 */
class DatabaseController {
	public function __construct() {
		$this->connect_db();
	}

	private function connect_db() {
		$mysqli_object = new mysqli('localhost', 'homestead', 'secret', 'wd_cmmnews_2018');

		if ($mysqli_object->connect_errno) {
			die('Failed to connect to MySQL: (' . $mysqli_object->connect_errno . ') ' . $mysqli_object->connect_error);
		}
	}
}

$databaseObject = new DatabaseController();








/**
 * Voorbeeld 2, OOP
 * Class DatabaseController 
 
class DatabaseController {
    public function connect_db($) {
		$mysqli_object = new mysqli('localhost', 'homestead', 'secret', 'wd_cmmnews_2018');
		 
		if ($mysqli_object->connect_errno) {
			die('Failed to connect to MySQL: (' . $mysqli_object->connect_errno . ') ' . $mysqli_object->connect_error);
		}
	}
}
 
$databaseObject = new DatabaseController();
$databaseObject->connect_db();
*/ 


/**
 * Voorbeeld 1, functioneel:
 *

 function connect_db() {
	 $mysqli_object = new mysqli('localhost', 'homestead', 'secret', 'wd_cmmnews_2018');
	 
	 if ($mysqli_object->connect_errno) {
	 	die('Failed to connect to MySQL: (' . $mysqli_object->connect_errno . ') ' . $mysqli_object->connect_error);
	  }
	  return $mysqli_object;
 }

$db = connect_db();

*/






