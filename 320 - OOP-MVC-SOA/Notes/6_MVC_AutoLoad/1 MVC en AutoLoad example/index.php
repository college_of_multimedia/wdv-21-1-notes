<?php

/**
 * AutoLoad
 * Deze functie haalt onderdelen op via NameSpaceing
 */
spl_autoload_register (
	function ($class)
	{
		if (class_exists($class)) {
			return;
		}	
		$filepath = str_replace('\\', '/', $class). '.php';
		if (file_exists(__DIR__ . '/' . $filepath)) {
			require_once(__DIR__ . '/' . $filepath);
		}
	}
);



$model = new Model();
$controller = new Controller($model);
$view = new View($controller, $model);

echo $view->output();
