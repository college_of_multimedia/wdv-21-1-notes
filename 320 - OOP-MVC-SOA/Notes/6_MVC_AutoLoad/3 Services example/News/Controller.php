<?php

namespace News;

/**
 * Class newsController
 *
 * Mijn newsController die alle functionaliteit van de nieuwsberichten regelt
 * deze class geeft nieuwsbericht objecten terug
 * de weergave gebeurd view.php
 */
class Controller
{
     /**
     * @var \PDO
     */
    private $_pdo;

    // public function __construct(\PDO $pdo)
    public function __construct( string $pdo )
    {
        $this->_pdo = $pdo;
    }


    /**
     * Laat één berichten zien
     */
    public function showNewsItem($id)
    {
        $newsItem = new Item();
        $newsItem->setTitle('mijn titel');
        $newsItem->setMessage('mijn tekst');
        require_once(DOCUMENT_ROOT.'/News/Views/Item.php');

    }

    /**
     * Laat alle berichten zien
     */
    public function showNewsItems()
    {
        $newsItems = [];
        /*
         * Haal alle berichten op uit de database
         */
        // $newsDbItems = $this->_pdo->exec('SELECT * FROM nieuwsberichten');
        $newsDbItems = [
                        ['title'=>'Bla 1','message'=>'bla bla 1'],
                        ['title'=>'Bla 2','message'=>'bla bla 2'],
                        ['title'=>'Bla 4','message'=>'bla bla 4'],
                       ];
        
        /*
         * Vertaal nu elk bericht naar een model/item
         */
        foreach ($newsDbItems as $item) {
            $newsItem = new Item();
            $newsItem->setTitle($item['title']);
            $newsItem->setMessage($item['message']);
            // stop het newsItem in een Array 
            $newsItems[] = $newsItem;
        }

        require_once(DOCUMENT_ROOT.'/News/Views/Overview.php');
    }
}

