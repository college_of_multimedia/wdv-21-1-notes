<?php
// Dit stelt voor de "Client" zijn front-end, zou dus ook index.php kunnen heten

// Method: POST, PUT, DELETE, GET etc
// Data: array("param" => "value") ==> index.php?param=value met GET

function callAPI($method, $url, $data = false) {

    $ch = curl_init();
     
    switch ($method) {
        case "POST":
            curl_setopt($ch, CURLOPT_POST, 1);
            if ($data)
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            break;
        case "PUT":
            curl_setopt($ch, CURLOPT_PUT, 1);
            break;
        case "DELETE":
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            break;
        default:
            // Fallback naar GET
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
    }
    
    // Optional Authentication:
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, "username:password");

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
    $result = curl_exec($ch);
    
    // var_dump(curl_getinfo($ch));

    curl_close($ch);
    
    return $result;
}

echo "<h3> Call the API</h3>";
echo '<div>testURL = http://192.168.33.39/api/handle_rest.php</div>';
echo '<div>testAPI = /News/123</div>';
echo '<div>URL = http://192.168.33.39/api/handle_rest.php/News/123</div>';

$url = 'http://192.168.33.39/api/handle_rest.php/News/123';
// TestServer op ScotchBox heeft geen echte DNS dus test met het IP adres
// in live site kan het wel met de DNS naam

echo '<hr>';
echo callAPI('GET', $url, array('naam' => 'Harald', 'Leeftijd' => 23 ) );
echo '<hr>';
echo callAPI('POST', $url, array('naam' => 'Harald', 'Leeftijd' => 23 ) );
echo '<hr>';
echo callAPI('DELETE', $url );
echo '<hr>';
echo callAPI('PUT', $url );
echo '<hr>';
