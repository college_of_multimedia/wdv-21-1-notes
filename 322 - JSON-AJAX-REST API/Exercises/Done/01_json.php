<h1>Maak een JSON output</h1>
Op basis van een simpel news object maak je een JSON feed.
<h2>Opgave 1</h2>
<ul>
    <li>Maak een php pagina: news.php.</li>
    <li>Deze is bijvoorbeeld bereikbaar via: http://localhost/news.php</li>
</ul>

<h2>Opgave 2</h2>
<ul>
    <li>Maak een news php object met de volgende onderdelen:</li>
    <li>Titel</li>
    <li>Content</li>
    <li>Datum</li>
    <li>Wel / Niet actief</li>
    <li>Dit is een Model class</li>
</ul>
<h2>Opgave 3</h2>
<ul>
    <li>Vul het object met zelf verzonnen data om te testen.</li>
</ul>
<h2>Opgave 4</h2>
<ul>
    <li>Stuur het object in JSON formaat naar de browser, gebruik voor de formatering deze php functie: <pre>json_encode()</pre></li>
</ul>

<?php

class News {
    private $_item;
 
    public function __construct($titel, $content, $datum, $actief){
        $this->setItem($titel, $content, $datum, $actief);
    }

    public function getItem(){
        return $this->_item;
    }

    public function setItem($titel, $content, $datum, $actief){
        $this->_item = [
           'titel'     => $titel,
           'content'   => $content, 
           'datum'     => $datum,
           'actief'    => $actief,
        ]; 
    }
}


$news1 = new News('Berichtje', 'Wat inhoud', '2021-07-10 15:32', true);
$news2 = new News('Berichtje2', 'Wat inhoud2', '2021-07-10 15:32', true);
$moreNews[] = $news1->getItem();
$moreNews[] = $news2->getItem();


echo ( json_encode( $moreNews ) );