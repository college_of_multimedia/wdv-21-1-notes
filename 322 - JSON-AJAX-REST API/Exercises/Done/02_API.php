<?php
/*
<h1>Maak een API om nieuwsberichten op te halen</h1>
Het moet mogelijk zijn om alle berichten op te vragen of 1 bericht op basis van het ID van het bericht
<h2>Opgave 1</h2>
<ul>
    <li>Maak php pagina: news.php.</li>
    <li>Maak een array met minimaal 3 nieuwsberichten.</li>
    <li>Zorg dat elk bericht een key heeft, dit gebruiken wij als id</li>
</ul>

<h2>Opgave 2</h2>
<ul>
    <li>Als er geen parameter verzonden is wil ik alle berichten weergeven</li>
    <li>Geef de berichten weer volgens de JSON structuur uit opgave 1</li>
    <li>Voorbeeld call: http://localhost/news.php</li>
</ul>
<h2>Opgave 3</h2>
<ul>
    <li>Als de parameter id is meegegeven dan wil ik alleen het bericht zien met deze key</li>
    <li>Mocht een bericht niet bestaan dan moet er een foutmelding verschijnen</li>
    <li>Voorbeeld call: http://localhost/news.php?id=2</li>
</ul>
*/

// return the response as JSON
header("Content-Type:application/json");

$news_list = [ 1 => [
                'id' => '1',
                'titel' => 'De eerste',
                'content' => 'Bla en bla',
             ], 3 => [
                'id' => '3',
               'titel' => 'De tweede',
               'content' => 'bla bla en nog meer bla',
             ], 4 => [
               'id' => '4',
               'titel' => 'De Derde',
               'content' => 'enzo voorts...',
             ]
];

if ( isset( $_GET['id'] ) ) {
    // toon bericht met id
    $bericht = ['error' => 'Niets gevonden!'];
    $key = $_GET['id'];
    
    if ( array_key_exists($key, $news_list) ){
        $bericht = $news_list[ $key ];
    }
    
    echo json_encode($bericht);
} else {
    // toon alles
    echo json_encode($news_list);
}

exit();