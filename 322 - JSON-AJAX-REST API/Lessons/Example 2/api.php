<?php
/*
 * Voorbeeld API met twee verschillende endpoints
 *
 * Voor de complete lijst:
 * Use: action=get_app_list
 *
 * Of als je maar 1 app wilt ophalen:
 * action=get_app&id=1
 */

/**
 * Haal een app op op basis van een ID
 *
 * @param $id
 *
 * @return array
 */
function get_app_by_id($id)
{
    $app_info = [];

    /*
     * Normaal gesproken komt dit uit een database
     */
    switch ($id) {
        case 1:
            $app_info = ['app_name' => 'Web Demo', 'app_price' => 'Free', 'app_version' => '2.0'];
            break;
        case 2:
            $app_info = ['app_name' => 'Audio Countdown', 'app_price' => 'Free', 'app_version' => '1.1'];
            break;
        case 3:
            $app_info = ['app_name' => 'The Tab Key', 'app_price' => 'Free', 'app_version' => '1.2'];
            break;
        case 4:
            $app_info = ['app_name' => 'Music Sleep Timer', 'app_price' => 'Free', 'app_version' => '1.9'];
            break;
    }

    return $app_info;
}

/**
 * @return array
 */
function get_app_list()
{
    /*
     * Normaal gesproken komt dit uit een database
     */
    $app_list = [
        ['id' => 1, 'name' => 'Web Demo'],
        ['id' => 2, 'name' => 'Audio Countdown'],
        ['id' => 3, 'name' => 'The Tab Key'],
        ['id' => 4, 'name' => 'Music Sleep Timer'],
    ];

    return $app_list;
}

/*
 * Mogelijke opties, hier kun je straks op controleren
 */
$possible_url = ['get_app_list', 'get_app'];
$value        = 'An error has occurred.';

/*
 * Is de call correct uitgevoerd?
 */
if ( ! isset($_GET['action']) || ! in_array($_GET['action'], $possible_url)) {
    echo 'An error has occurred. No action is set.<br>';
    echo 'Try: <a href="api.php?action=get_app_list">action=get_app_list</a><br>';
    echo 'Or: <a href="api.php?action=get_app&id=1">action=get_app&id=1</a><br>';
    die();
}

/*
 * Op basis van de get action wordt er informatie terug gegeven
 */
switch ($_GET['action']) {
    case 'get_app_list':
        $value = get_app_list();
        break;
    case 'get_app':
        if ( ! isset($_GET['id'])) {
            $value = 'Missing argument';
            break;
        }
        $value = get_app_by_id($_GET['id']);
        break;

}

// return the response as JSON
header("Content-Type:application/json");
/*
 * return JSON array
 */
echo json_encode($value);
