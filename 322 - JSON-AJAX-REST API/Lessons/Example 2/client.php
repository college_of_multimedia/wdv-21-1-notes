<?php
// roep op via http://www.deslager.test/Example%201/client.php

/**
 * Zorg dat alle Errors weergeven worden
 */
ini_set('error_reporting', 1);
error_reporting(E_ALL);

/*
 * Defineer de url naar deze documenten
 */
# Haal de host op
$hostIP = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_ADDR'];
$host = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'];
# Haal het complete path in de url op
$path = explode('.php', $_SERVER['REQUEST_URI']);
// Alles achter .php hebben we niet nodig
$path = array_shift($path);
# Verwijder de document namen
$path = str_replace(['client', 'api'], '', $path);

# onthou deze url en IP
define('ROOT_URL', $host . $path);
define('ROOT_IP', $hostIP . $path);
?>
<html>
<title>Dit is een voorbeeld client</title>
<body>
<!-- 
<?php 
    var_dump(ROOT_URL); // Dit werkt alleen op een echte DNS gedreven Server
    var_dump(ROOT_IP); // Dit werkt altijd! (ook op Scotch e.d.) ivm GEEN DNS maar hostfile!
?>
-->
<h1>Voorbeeld client</h1>
Dit kan een statische pagina zijn of een app op een telefoon.<br/>
<?php
/*
 * Ophalen van 1 app als er een actie is ingesteld
 */
if (isset($_GET['action']) && isset($_GET['id']) && $_GET['action'] == 'get_app') {
    // $var = file_get_contents(url); "download" de data uit de url en stop dit in $var
    $app_info = file_get_contents(ROOT_IP . 'api.php?action=get_app&id=' . $_GET['id']);
    // vertaal de JSON style data naar PHP Assoc Array
    $app_info = json_decode($app_info, true);
    ?>
    <table>
        <tr>
            <td>App Name:</td>
            <td> <?= $app_info['app_name'] ?></td>
        </tr>
        <tr>
            <td>Price:</td>
            <td> <?= $app_info['app_price'] ?></td>
        </tr>
        <tr>
            <td>Version:</td>
            <td> <?= $app_info['app_version'] ?></td>
        </tr>
    </table>
    <br/>
    <a href="<?= ROOT_URL ?>client.php?action=get_app_list" alt="app list">Return to the app list</a>

    <?php
} else {
    /*
     * Als er geen id doorgegeven is dan wil ik alles zien
     */
    $app_list = file_get_contents(ROOT_IP . 'api.php?action=get_app_list');
    $app_list = json_decode($app_list, true);
    ?>
    <ul>
        <?php foreach ($app_list as $app): ?>
            <li>
                <a href="<?= ROOT_URL ?>client.php?action=get_app&id=<?= $app['id'] ?>"><?= $app['name']; ?></a>
            </li>
        <?php endforeach; ?>
    </ul>
    <?php
} ?>
</body>
</html>