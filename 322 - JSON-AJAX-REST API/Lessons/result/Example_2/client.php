<?php
ini_set('error_reporting', 1);
error_reporting(E_ALL);

$debug_array = [];

$host = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'];

	$debug_array[] = $host;

$hostIP = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_ADDR'];

	$debug_array[] = $hostIP;

$path = explode('.php?', $_SERVER['REQUEST_URI'] );

	$debug_array[] = $_SERVER['REQUEST_URI'];
	$debug_array[] = $path;

$path = array_shift($path);
	$debug_array[] = $path;

$path = str_replace(['client', 'api', '.php'], '', $path);
	$debug_array[] = $path;

define ('ROOT_URL' , $host . $path );
	$debug_array[] = ROOT_URL;

define ('ROOT_IP' , $hostIP . $path );
	$debug_array[] = ROOT_IP;

// ROOT_URL WERKT ALLEEN MET EEN ECHTE DNS!!!!
// en dus niet met virtuele host
// Backup is dus de ROOT_IP!

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Dit is een voorbeeld client</title>
</head>
<body>
<!-- Debug <?php 
var_dump( $debug_array ) 
?>
-->
	<h1>Dit is een voorbeeld client</h1>
<?php 
if (isset($_GET['action']) && isset($_GET['id']) && $_GET['action'] == 'get_app' ){
	$app_info = file_get_contents( ROOT_IP . 'api.php?action=get_app&id=' . $_GET['id'] );
	$app_info = json_decode($app_info, true);
?>
	<table>
		<tr>
			<td>App Name:</td>
			<td><?= $app_info['app_name'] ?></td>
		</tr>
		<tr>
			<td>App Price:</td>
			<td><?= $app_info['app_price'] ?></td>
		</tr>
		<tr>
			<td>App Version:</td>
			<td><?= $app_info['app_version'] ?></td>
		</tr>
	</table>
	<br>
	<a href="<?= ROOT_URL ?>client.php?action=get_app_list">Toon volledige lijst van Apps</a>
<?php
} else {
	// de lijst versie OOK als er dus niets wordt opgegeven
	$app_list = file_get_contents( ROOT_IP . 'api.php?action=get_app_list');
	$app_list = json_decode($app_list, true);
?>
	<ul>
<?php foreach ($app_list as $app): ?>
		<li> 
			<a href="<?= ROOT_URL ?>client.php?action=get_app&id=<?= $app['id']; ?>"><?= $app['name']; ?></a>
		</li>
<?php endforeach; ?>
	</ul>
<?php
} // einde else (if) 
?>
</body>
</html>








