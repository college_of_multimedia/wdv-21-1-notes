<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>News Overview</title>
</head>
<body>

	<?php // dd($messages); ?>

	<div class="content">

		<h1>Overzicht van berichten</h1>

		<ul>
			@foreach ($messages as $message)
			<li>
				<h2>{{ $message->title }}</h2>
				{{ $message->publish_date }}<br>
				<a href="/news/{{ $message->id }}">Lees meer</a>
			</li>
			@endforeach

		</ul>

		<a href="/">Terug naar Home</a><br>

	</div>
</body>
</html>