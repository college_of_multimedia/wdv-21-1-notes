<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>{{ $message->title }}</title>
</head>
<body>
	<div class="content">

		<h1>{{ $message->title }}</h1>
		<span>{{ $message->publish_date }}</span>
		<p>{{ $message->body }}</p>
	
		<a href="/news">Terug naar Overzicht</a><br>
		<a href="/">Terug naar Home</a><br>

	</div>
</body>
</html>