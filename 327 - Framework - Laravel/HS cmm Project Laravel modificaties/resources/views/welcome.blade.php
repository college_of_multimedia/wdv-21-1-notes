<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Welkom</title>
    </head>
    <body>
        <div class="content">
            <h1>Welkom {{ $naam }} uit {{ $plaats }}</h1>
            <h2>Nieuws van deze maand:</h2>

            <ul>
                @foreach ( $messages as $message )
                    <li>{{ $message->title }}</li>
                @endforeach
            </ul>

        <a href="/news">Naar nieuws overzicht</a><br>

        </div>
    </body>
</html>