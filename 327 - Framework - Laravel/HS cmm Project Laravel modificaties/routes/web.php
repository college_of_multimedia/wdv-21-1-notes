<?php

use Illuminate\Support\Facades\Route;
use App\Models\News;
use App\Models\Page;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/news', function(){
    $messages = News::all();
    return view('news.index', compact('messages') );
});

Route::get('/news/{news}', function($news_id){
    $message = News::find($news_id); 
    return view('news.show', compact('message') );
});

Route::get('/{page}', function ($url) {
    $page = Page::where('url', $url)->where('active', 1)->first();

    if (empty($page)){
        return view('404');
    }

    return view('page.show' , compact($page));
});

Route::get('/', function () {
    $naam = 'Henk';
    $plaats = "Amsterdam";
    DB::enableQueryLog();
    $messages = DB::table('news')
        ->where('publish_date', '>', date('2021-07-01') )
        ->orderBy('publish_date')
        ->get(); 

    // dd ( DB::getQueryLog()) ;    

    return view('welcome', 
        compact('naam','plaats', 'messages') 
    );

});

Route::get('/over_ons', function(){
    return view('overons');
});




