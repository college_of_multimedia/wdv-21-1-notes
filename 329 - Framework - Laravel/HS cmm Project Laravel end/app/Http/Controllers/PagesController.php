<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Page;
use App\Models\News;

class PagesController extends Controller
{
    /**
     * laat de indexpagina zien met 2 nieuwste berichten
     * @return \Illuminate\Contracts\View\Factory | \Illuminate\View\View
     */
    public function index(){
        $page = Page::activePage( 'welcome' );
        $messages = News::LatestNews(3);
        return view( 'pages.index', 
            compact('messages','page') );
    }

    /**
     * laat een andere pagina zien
     * @param  integer $page_id
     * @return \Illuminate\Contracts\View\Factory | \Illuminate\View\View
     */
    public function show($url){
        $page = Page::activePage($url);
         if ( isset($page->exists) ) {
            return view('pages.show', 
                compact('page') 
            );
        }

        // niet gevonden of niet actief dan 404
        return view('404');
   }
   /**
     * laat de home zien
     * @return \Illuminate\Contracts\View\Factory | \Illuminate\View\View
     */
    public function home(){
        return view( 'home' );
    }

}
