<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class News extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'body', 'publish_date'];

    /**
     * @return NewsObjecten
     */
    public function scopeLatestNews($query, $limit){
        return $query->orderBy('publish_date', 'desc')
        ->where('publish_date','<=', date('Y-m-d', time() ) )
        ->limit($limit)->get();
    }

    public function getDate(){
        $carbonDate = Carbon::now();
        $carbonDate->timestamp(strtotime($this->publish_date))
                   ->timezone('Europe/Amsterdam')
                   ->setLocale(LC_ALL, 'nl_NL.UTF-8');

        return $carbonDate;
    }

}
