# ************************************************************
# Sequel Ace SQL dump
# Version 3038
#
# https://sequel-ace.com/
# https://github.com/Sequel-Ace/Sequel-Ace
#
# Host: 192.168.10.19 (MySQL 8.0.25-0ubuntu0.20.04.1)
# Database: cmmproject
# Generation Time: 2021-08-17 15:45:42 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE='NO_AUTO_VALUE_ON_ZERO', SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table news
# ------------------------------------------------------------

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;

INSERT INTO `news` (`id`, `user_id`, `title`, `body`, `publish_date`, `created_at`, `updated_at`)
VALUES
	(1,0,'Titel','Dit is een Body','2020-01-19','2020-01-19 19:00:00','2020-01-19 19:00:00'),
	(2,0,'Nog een titel','Met een heel ander verhaal\n','2020-01-20','2020-01-20 19:00:00','2020-01-20 19:00:00'),
	(4,0,'bla bla','bla bla','2020-02-05','2020-02-05 20:32:05','2021-08-17 15:18:22'),
	(5,0,'Dit is een vierde titel','En dit is de body...','2021-08-17','2021-08-17 12:33:01','2021-08-17 12:33:01'),
	(6,0,'Een vijfde item','Dit is een vijfde item zijn body','2021-08-16','2021-08-17 15:16:26','2021-08-17 15:16:26');

/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pages
# ------------------------------------------------------------

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;

INSERT INTO `pages` (`id`, `title`, `url`, `body`, `active`, `user_id`, `created_at`, `updated_at`)
VALUES
	(1,'Over ons','over_ons','Over ons is veel BALALBALBALABLABLABALBALAB',1,0,'2020-01-19 19:00:00','2020-01-19 19:00:00'),
	(2,'Contact','contact','Dit is de contact pagina bla bla',0,0,'2020-01-19 19:00:00','2020-01-19 19:00:00'),
	(3,'Welkom','welcome','Welkom bla ALALBALBALABLABLABALBALAB',1,0,'2020-01-19 19:00:00','2020-01-19 19:00:00');

/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
