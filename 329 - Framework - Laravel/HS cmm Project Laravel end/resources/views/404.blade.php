@extends( 'layouts.master' )

@section('content')
    <section class="jumbotron p-3 p-md-5 text-white rounded bg-dark">
        <div class="col-md-6 px-0">
            <h1 class="jumbotron-heading">Pagina niet gevonden!</h1>
            <p>
                <a href="/" class="btn btn-primary" role="button">Welkom</a>
                <a href="/over_ons/" class="btn btn-primary" role="button">Over ons</a>
                <a href="/contact/" class="btn btn-primary" role="button">Contact</a>
                <a href="/news/" class="btn btn-primary" role="button">Nieuws</a>
            </p>
        </div>
    </section>
    <div class="row mb-2">
        <div class="col-md-12">
            <p>
                Misschien is de pagina niet actief
            </p>
        </div>
    </div>
@endsection
