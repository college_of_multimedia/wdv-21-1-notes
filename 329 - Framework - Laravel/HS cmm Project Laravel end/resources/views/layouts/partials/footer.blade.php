<footer class="text-muted">
    <div class="container">
        <p class="float-right" id="profiel_link">
            <a href="#">Back to top</a>
        </p>
        <p class="footer_data">
            {{ date('Y') }} CMM Project demo
        </p>
    </div>
</footer>
