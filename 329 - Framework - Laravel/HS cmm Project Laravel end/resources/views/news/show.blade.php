@extends( 'layouts.master' )


@section('title')
Toon bericht: {{ $news->title }}
@endsection


@section('content')
	<h1>{{$news->title}}</h1>

	<h3>{{$news->publish_date}}</h3>
	
	Het id is: {{$news->id}}
	<p>
		{!! $news->body !!}
	</p>  

	@auth
		<a href="/news/{{$news->id}}/edit" class="btn btn-info">edit</a>

		<form action="/news/{{$news->id}}" method="post">
			{{ csrf_field() }}
			<input type="hidden" name="_method" value="delete"><br>
			<button type="submit" class="btn btn-warning">Delete</button><br>
		</form>
		<br>
    @endauth
@endsection



















