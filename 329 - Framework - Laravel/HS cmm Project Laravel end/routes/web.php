<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

##### Route voor Authentication #####

Auth::routes();
// eventueel setup: composer require laravel/ui

##### Routes voor News #####

use App\Http\Controllers\NewsController;

Route::post('/news',                [ NewsController::class, 'store'     ])->middleware('auth');

Route::post('/news/{news}',         [ NewsController::class, 'update'    ])->middleware('auth');

Route::delete('/news/{news_id}',    [ NewsController::class, 'destroy'   ])->middleware('auth');

Route::get('/news/create',          [ NewsController::class, 'create'    ])->middleware('auth');

Route::get('/news/{news}/edit',     [ NewsController::class, 'edit'      ])->middleware('auth');

Route::get('/news/{news}',          [ NewsController::class, 'show'      ]);

Route::get('/news',                 [ NewsController::class, 'index'     ]);


##### Routes voor de pages #####

use App\Http\Controllers\PagesController;

Route::get( '/',                    [ PagesController::class, 'index'     ]);

Route::get( '/home',                [ PagesController::class, 'home'	  ])->middleware('auth');

Route::get( '/{page}',              [ PagesController::class, 'show'      ]);

