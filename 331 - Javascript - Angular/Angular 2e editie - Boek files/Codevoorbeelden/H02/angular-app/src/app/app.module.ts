// imports voor deze module
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CustomerComponent } from './customer/customer.component';

// TypeSCript Decorator voor modules
@NgModule({
  declarations: [
    AppComponent,
    CustomerComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})

// Geexporteerde JavaScript -klasse. Geen logica (die staat in de afzonderlijke componenten).
export class AppModule { }
