import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  // 1. Geef aan dat deze component twee variabelen ('properties') bevat.
  // Ze zijn beide van het type string.
  title: string;
  name: string;

  // 2. De constructor blijft leeg. Gebruik deze voor DI (zie hoofdstuk 5 en verder).
  constructor() {}

  ngOnInit() {
    // 3. Best practice: bind variabelen in ngOnInit (dit heet een 'lifecycle hook').
    this.title = 'Hello World, dit is Angular';
    this.name = 'Peter Kassenaar';
  }
}
