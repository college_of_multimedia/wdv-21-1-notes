import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title: string;
  cities: string[];

  constructor() {
  }

  ngOnInit() {
    this.title = 'Mijn favoriete steden';

    // Array samenstellen met strings
    this.cities = [
      'Groningen',
      'Hengelo',
      'Den Haag',
      'Enschede'
    ];
  }
}
