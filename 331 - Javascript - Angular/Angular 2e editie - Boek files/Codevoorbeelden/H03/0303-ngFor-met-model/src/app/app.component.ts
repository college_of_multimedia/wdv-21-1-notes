import { Component, OnInit } from '@angular/core';
import { City, ICity } from './shared/model/city.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title: string;
  cities: City[];

  constructor() {}

  ngOnInit() {
    this.title = 'Mijn favoriete steden - via Model';

    // Array samenstellen via class
    this.cities = [
      new City(1, 'Groningen', 'GR'),
      new City(2, 'Hengelo', 'OV'),
      new City(3, 'Den Haag', 'ZH'),
      new City(4, 'Enschede', 'OV')
    ];

    // array samenstellen via interface:
    const citiesVolgensInterface: ICity[] = [
      { id: 1, name: 'Groningen', province: 'GR' },
      { id: 2, name: 'Hengeloooo', province: 'OV' },
      { id: 3, name: 'Den Haag', province: 'ZH' },
      { id: 4, name: 'Enschede', province: 'OV' }
    ];
  }
}
