// city.model.ts

// 1. Voorbeeld, bij gebruik van een Class
export class City {
  constructor(
    public id: number,
    public name: string,
    public province: string
  ) {}
}

// 2. Voorbeeld, bij gebruik van een Interface
export interface ICity {
  id: number;
  name: string;
  province: string;
}
