import { Component, OnInit } from '@angular/core';
import { City } from './shared/model/city.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title: string;
  cities: City[];
  showCities: boolean;

  constructor() {}

  ngOnInit() {
    this.title = 'Mijn favoriete steden - met ngIf';

    // Array samenstellen via class
    this.cities = [
      new City(1, 'Groningen', 'GR'),
      new City(2, 'Hengelo', 'OV'),
      new City(3, 'Den Haag', 'ZH'),
      new City(4, 'Enschede', 'OV')
    ];
    // boolean evalueren en resultaat laten meetellen
    // om de lijst in app.component.html wel- of niet te tonen.
    this.showCities = this.cities.length > 3;
  }
}
