import { Component, OnInit } from '@angular/core';
import { City } from './shared/model/city.model';

@Component({
  selector: 'app-root',
  template: `
  <!-- inline HTML -->
  <div class="container">
    <h1>{{ title }}</h1>
    <ul class="list-group" *ngIf="showCities">
      <li class="list-group-item " *ngFor="let city of cities ">
        {{ city.id }} - {{ city.name }} ({{ city.province }})
      </li>
    </ul>
    <h2 *ngIf="cities.length> 3">Jij hebt veel favoriete steden!</h2>
  </div>
  `
})
export class AppComponent implements OnInit {
  title: string;
  cities: City[];
  showCities: boolean;

  constructor() {}

  ngOnInit() {
    this.title = 'Mijn favoriete steden - met inline template';

    // Array samenstellen via class
    this.cities = [
      new City(1, 'Groningen', 'GR'),
      new City(2, 'Hengelo', 'OV'),
      new City(3, 'Den Haag', 'ZH'),
      new City(4, 'Enschede', 'OV')
    ];
    // boolean evalueren en resultaat laten meetellen
    // om de lijst in app.component.html wel- of niet te tonen.
    this.showCities = this.cities.length > 3;
  }
}
