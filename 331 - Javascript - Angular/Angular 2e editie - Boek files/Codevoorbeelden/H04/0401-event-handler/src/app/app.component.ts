import { Component, OnInit } from '@angular/core';
import { City } from './shared/model/city.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title: string;
  cities: City[];
  toggleMsg: string = 'Verberg lijst met steden.';
  showCities: boolean = true;

  constructor() {}

  ngOnInit() {
    this.title = 'Favoriete steden - event handlers';

    // Array samenstellen via class
    this.cities = [
      new City(1, 'Groningen', 'GR'),
      new City(2, 'Hengelo', 'OV'),
      new City(3, 'Den Haag', 'ZH'),
      new City(4, 'Enschede', 'OV')
    ];
  }
  // event handler voor klikken op knop
  btnClick() {
    alert('Uw bestelling wordt uitgevoerd!');
  }

  // klikken op checkbox afvangen
  toggleCities() {
    this.showCities = !this.showCities;
    this.showCities
      ? (this.toggleMsg = 'Verberg lijst met steden.')
      : (this.toggleMsg = 'Toon de lijst met steden');
  }
}
