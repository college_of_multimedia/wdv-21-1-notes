import { Component, OnInit } from '@angular/core';
import { City } from './shared/model/city.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title: string;
  cities: City[];
  showCities: boolean = true;
  newCity: string = '';

  constructor() {}

  ngOnInit() {
    this.title = 'Stad toevoegen';

    // Array samenstellen via class
    this.cities = [
      new City(1, 'Groningen', 'GR'),
      new City(2, 'Hengelo', 'OV'),
      new City(3, 'Den Haag', 'ZH'),
      new City(4, 'Enschede', 'OV')
    ];
  }
  // event handler voor klikken op knop
  btnClick() {
    alert('Uw bestelling wordt uitgevoerd!');
  }

  // Aangeklikte stad tonen in een alert-box
  showCity(city: City) {
    alert('Uw favoriete stad is: ' + city.name);
  }

  addCity(value: string) {
    let addedCity = new City(
      this.cities.length + 1, // id
      value, // naam
      'Onbekend' // provincie
    );
    this.cities.push(addedCity);
    this.newCity = `toegevoegd: ${value}`;
  }
}
