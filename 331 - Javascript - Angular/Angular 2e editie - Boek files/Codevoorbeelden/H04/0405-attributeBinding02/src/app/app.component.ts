import { Component, OnInit } from '@angular/core';
import { City } from './shared/model/city.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title: string;
  cities: City[];
  showCities: boolean = true;
  currentCity: City;
  cityPhoto: string = '';
  constructor() {}

  ngOnInit() {
    this.title = 'Stad selecteren - attribute binding';

    // Array samenstellen via class
    this.cities = [
      new City(1, 'Groningen', 'GR'),
      new City(2, 'Hengelo', 'OV'),
      new City(3, 'Den Haag', 'ZH'),
      new City(4, 'Enschede', 'OV')
    ];
  }
  // event handler voor klikken op knop
  btnClick() {
    alert('Uw bestelling wordt uitgevoerd!');
  }

  showCity(city: City) {
    this.currentCity = city;
    this.cityPhoto = `assets/img/${this.currentCity.name}.jpg`;
  }
}
