import { Component, OnInit } from '@angular/core';
import { City } from './shared/model/city.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title: string;
  cities: City[];
  showCities: boolean = true;
  currentCity: City;
  cityPhoto: string = '';
  newCity: string = '';
  constructor() {}

  ngOnInit() {
    this.title = 'Stad toevoegen via [(ngModel)]';

    // Array samenstellen via class
    this.cities = [
      new City(1, 'Groningen', 'GR'),
      new City(2, 'Hengelo', 'OV'),
      new City(3, 'Den Haag', 'ZH'),
      new City(4, 'Enschede', 'OV')
    ];
  }
  // Stad binden aan HTML-attribuut
  showCity(city: City) {
    this.currentCity = city;
    this.cityPhoto = `assets/img/${this.currentCity.name}.jpg`;
  }

  // Stad toevoegen via [(ngModel)].
  // Let op, geen parameter meer nodig!
  addCity() {
    const addedCity = new City(this.cities.length + 1, this.newCity, 'Onbekend');
    this.cities.push(addedCity);
  }
}
