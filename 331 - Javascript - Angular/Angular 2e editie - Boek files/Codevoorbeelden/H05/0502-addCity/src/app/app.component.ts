// app.component.ts
import { Component, OnInit } from '@angular/core';
import { City } from './shared/model/city.model';
import { CityService } from './shared/services/city.service'; // importeer de Service

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title: string;
  cities: City[];
  currentCity: City;
  cityPhoto: string = '';

  constructor(private cityService: CityService) {}

  ngOnInit() {
    this.title = 'Stad toevoegen via Service';
    this.cities = this.cityService.getCities();
  }
  // Stad binden aan HTML-attribuut
  showCity(city: City) {
    this.currentCity = city;
    this.cityPhoto = `assets/img/${this.currentCity.name}.jpg`;
  }

  // Stad toevoegen --> doorgeven aan de service
  addCity(cityName: string) {
    this.cityService.addCity(cityName);
  }
}
