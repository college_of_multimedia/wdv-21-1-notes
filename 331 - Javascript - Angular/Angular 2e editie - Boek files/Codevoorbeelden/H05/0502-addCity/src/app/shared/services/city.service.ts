import { Injectable } from '@angular/core';
import { City } from '../model/city.model';

@Injectable({
  providedIn: 'root'
})
export class CityService {
  private cities: City[] = [
    new City(1, 'Groningen', 'Groningen'),
    new City(2, 'Hengelo', 'Overijssel'),
    new City(3, 'Den Haag', 'Zuid-Holland'),
    new City(4, 'Enschede', 'Overijssel')
  ];

  // retourneer alle cities
  getCities(): City[] {
    return this.cities;
  }

  // retourneer city op basis van ID
  getCity(id: number): City {
    return this.cities.find(c => c.id === id);
  }

  addCity(cityName: string): void {
    let newCity = new City(
      this.cities.length + 1,
      cityName,
      'onbekend' // TODO: provincie doorgeven
    );
    this.cities.push(newCity);
  }

  // Overige methods voor de service...
}
