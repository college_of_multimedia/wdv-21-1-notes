// app.component.ts
import {Component, OnInit} from '@angular/core';
import {City} from './shared/model/city.model';
import {CityService} from './shared/services/city.service'; // importeer de Service
import {HttpClient} from '@angular/common/http';
import {tap} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title: string;
  cities: City[];
  currentCity: City;
  cityPhoto: string = '';

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.title = 'Steden via HttpClient';
    // Communcatie via httpClient. Idealiter gaat dit via een service!
    // Nu, als demo even rechtstreeks in de component.
    this.http
      .get<City[]>('../assets/data/cities.json') // 1. Get data
      .pipe(
        tap(result => console.log('opgehaald via JSON: ', result)) // 2. Optioneel: operators
      )
      .subscribe(cities => (this.cities = cities)); // 3. Subscribe
  }

  // Stad binden aan view
  showCity(city: City) {
    // TODO
    this.currentCity = city;
    this.cityPhoto = `assets/img/${this.currentCity.name}.jpg`;
  }

  // Stad toevoegen --> doorgeven aan de service
  addCity(cityName: string) {
    // TODO
  }

  // Stad verwijderen --> doorgeven aan de service
  deleteCity(city: City) {
    // TODO
  }
}
