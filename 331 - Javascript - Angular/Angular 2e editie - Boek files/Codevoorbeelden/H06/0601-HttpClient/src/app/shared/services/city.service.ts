import { Injectable } from '@angular/core';
import { City } from '../model/city.model';

@Injectable()
export class CityService {
  // retourneer alle cities
  getCities() {
    // TODO: talk to API
  }

  // retourneer city op basis van ID
  getCity(id: number) {
    // TODO: talk to API
  }

  // Stad toevoegen,
  addCity(cityName: string): void {
    // TODO: talk to API
  }

  // stad verwijderen
  deleteCity(city: City): void {
    // TODO: talk to API
  }

  // Overige methods voor de service...
  //...
}
