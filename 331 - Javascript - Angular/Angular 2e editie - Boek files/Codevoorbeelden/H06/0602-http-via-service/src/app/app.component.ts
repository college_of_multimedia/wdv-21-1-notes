// app.component.ts
import { Component, OnInit } from '@angular/core';
import { City } from './shared/model/city.model';
import { CityService } from './shared/services/city.service'; // importeer de Service

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title: string;
  cities: City[];
  currentCity: City;
  cityPhoto: string = '';

  constructor(private cityService: CityService) {}

  ngOnInit() {
    this.title = 'Steden via HttpClient en service';
    // Gegevens ophalen via de Service. Subscription is hier, in de component!
    this.cityService.getCities()
      .subscribe(cities => (this.cities = cities));
  }

  // Stad binden aan view
  showCity(city: City) {
    // TODO
    this.currentCity = city;
    this.cityPhoto = `assets/img/${this.currentCity.name}.jpg`;
  }

  // Stad toevoegen --> doorgeven aan de service
  addCity(cityName: string) {
    // TODO
  }

  // Stad verwijderen --> doorgeven aan de service
  deleteCity(city: City) {
    // TODO
  }
}
