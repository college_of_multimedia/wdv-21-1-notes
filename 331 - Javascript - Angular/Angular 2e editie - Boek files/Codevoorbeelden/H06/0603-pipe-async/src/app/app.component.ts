// app.component.ts
import { Component, OnInit } from '@angular/core';
import { City } from './shared/model/city.model';
import { CityService } from './shared/services/city.service'; // importeer de Service
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title: string;
  // cities$ is nu een *observable* naar een array met City-objecten,
  // in plaats van dat de cities rechtstreeks in de variabele worden geplaatst.
  cities$: Observable<City[]>;
  currentCity: City;
  cityPhoto: string = '';

  constructor(private cityService: CityService) {}

  ngOnInit() {
    this.title = 'Steden met | async ';
    this.cities$ = this.cityService.getCities(); // directe toekenning
  }

  // Stad binden aan view
  showCity(city: City) {
    // TODO
    this.currentCity = city;
    this.cityPhoto = `assets/img/${this.currentCity.name}.jpg`;
  }

  // Stad toevoegen --> doorgeven aan de service
  addCity(cityName: string) {
    // TODO
  }

  // Stad verwijderen --> doorgeven aan de service
  deleteCity(city: City) {
    // TODO
  }
}
