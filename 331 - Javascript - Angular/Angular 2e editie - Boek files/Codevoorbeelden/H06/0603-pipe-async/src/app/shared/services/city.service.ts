// city.service.ts
import { Injectable } from '@angular/core';
import { City } from '../model/city.model';
import { HttpClient } from '@angular/common/http';

// Observable stuff
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()
export class CityService {
  constructor(private http: HttpClient) {}
  // retourneer alle cities
  getCities(): Observable<City[]> {
    return this.http
      .get<City[]>('../assets/data/cities.json')
      .pipe(tap(result => console.log('opgehaald via de service: ', result)));
  }

  // retourneer city op basis van ID
  getCity(id: number) {
    // TODO: talk to API
  }

  // Stad toevoegen,
  addCity(cityName: string): void {
    // TODO: talk to API
  }

  // stad verwijderen
  deleteCity(city: City): void {
    // TODO: talk to API
  }

  // Overige methods voor de service...
  //...
}
