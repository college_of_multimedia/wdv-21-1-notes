import {Component} from '@angular/core';
import {MovieService} from './shared/movie.service';
import {Observable} from 'rxjs';
import {Movie} from './shared/movie.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // Properties voor de component/class
  public movies$: Observable<Movie[]>;

  constructor(private movieService: MovieService) {
    // ...eventuele extra initialisaties
  }

  searchMovies(keyword: string) {
    this.movies$ = this.movieService.searchMovies(keyword);
  }
}
