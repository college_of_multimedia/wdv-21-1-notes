// movie.service.ts
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Movie } from './movie.model';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Injectable()
export class MovieService {
  url: string = 'http://www.omdbapi.com/?apikey=f1f56c8e&';

  constructor(private http: HttpClient) {}

  // retourneer alle movies
  searchMovies(keyword): Observable<Movie[]> {
    return this.http
      .get<Movie[]>(this.url + `s=${keyword}`)
      .pipe(
        tap(res => console.log(res)), // log return-resultaten in de console
        map(res => res['Search'])  // unwrap de Search-array die de API in het resultaat heeft geplaatst en retourneer deze.
      );
  }
}
