// app.component.ts
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { City } from './shared/model/city.model';
import { CityService } from './shared/services/city.service'; // importeer de Service
import { of } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title: string;
  cities$: Observable<City[]>;
  currentCity$: Observable<City>;

  constructor(private cityService: CityService) {}

  ngOnInit() {
    this.title = 'Steden via json-server';
    this.cities$ = this.cityService.getCities();
  }

  // Detailgegevens voor stad ophalen
  getCity(id: number) {
    this.currentCity$ = this.cityService.getCity(id);
  }

  // Stad toevoegen --> doorgeven aan de service
  addCity(cityName: string, provinceName: string) {
    // id === null, omdat deze automatisch door json-server wordt ingevuld
    const newCity = new City(null, cityName, provinceName);
    this.cityService.addCity(newCity).subscribe((addedCity: City) => {
      // cities opnieuw ophalen in de subscription.
      this.cities$ = this.cityService.getCities();
      this.currentCity$ = of(addedCity);
    });
  }

  // Stad verwijderen --> doorgeven aan de service
  deleteCity(city: City) {
    // TODO
  }

  editCity(city: City) {
    // TODO
  }

  clear() {
    this.currentCity$ = null;
  }
}
