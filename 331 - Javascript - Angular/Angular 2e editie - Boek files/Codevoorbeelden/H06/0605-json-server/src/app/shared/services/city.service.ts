// city.service.ts
import { Injectable } from '@angular/core';
import { City } from '../model/city.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';

// Observable stuff
import { tap, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { of } from 'rxjs';

@Injectable()
export class CityService {
  url = 'http://localhost:3000/cities';

  constructor(private http: HttpClient) {}

  // retourneer alle cities
  getCities(): Observable<City[]> {
    return this.http.get<City[]>(this.url).pipe(
      tap(result => console.log('opgehaald via de service: ', result)),
      catchError(err => {
        console.log(
          'Geen API aangetroffen. Start eerst json-server met "npm run json-server". '
        );
        // De methode moet een observable teruggeven. genereer daarom een
        // observable op basis van de error. De methode hiervoor heet of().
        return of(err);
      })
    );
  }

  // retourneer city op basis van ID
  getCity(id: number): Observable<City> {
    return this.http.get<City>(`${this.url}/${id}`);
  }

  // Stad toevoegen,
  addCity(newCity: City): Observable<City> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post<City>(this.url, newCity, { headers: headers });
  }

  // stad verwijderen
  deleteCity(city: City): void {
    // TODO: talk to API
  }

  // stad wijzigen
  editCity(city: City): void {
    // TODO: talk to API
  }

  // Overige methods voor de service...
  //...
}
