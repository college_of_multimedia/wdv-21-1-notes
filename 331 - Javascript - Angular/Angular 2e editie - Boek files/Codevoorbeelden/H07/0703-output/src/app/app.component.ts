// app.component.ts
import { Component, OnInit } from '@angular/core';
import { City } from './shared/model/city.model';
import { CityService } from './shared/services/city.service'; // importeer de Service

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title: string;
  cities: City[];
  currentCity: City;
  cityPhoto: string = '';

  constructor(private cityService: CityService) {}

  ngOnInit() {
    this.title = 'Steden met detailcomponent';
    this.cityService.getCities().subscribe(cities => (this.cities = cities));
  }

  // Stad binden aan view
  showCity(city: City) {
    this.currentCity = city;
    this.cityPhoto = `assets/img/${this.currentCity.name}.jpg`;
  }

  // Event verwerken dat uit de detailcomponent komt
  updateRating(rating: number): void {
    this.currentCity.rating += rating;
  }

  clear() {
    this.currentCity = null;
  }
}
