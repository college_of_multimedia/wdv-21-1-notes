// app.module.ts
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

// Services
import { CityService } from './shared/services/city.service';

// Componenten
import { AppComponent } from './app.component';
import { CityDetailComponent } from './city-detail/city-detail.component';

@NgModule({
  declarations: [AppComponent, CityDetailComponent],
  imports: [BrowserModule, HttpClientModule],
  providers: [CityService],
  bootstrap: [AppComponent]
})
export class AppModule {}
