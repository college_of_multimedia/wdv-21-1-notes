import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { City } from '../shared/model/city.model';

@Component({
  selector: 'app-city-detail',
  templateUrl: './city-detail.component.html',
  styleUrls: ['./city-detail.component.css']
})
export class CityDetailComponent implements OnInit {
  @Input() city: City;
  @Output() rating: EventEmitter<number> = new EventEmitter<number>();

  constructor() {}

  ngOnInit() {}

  rate(num: number): void {
    console.log('rating voor ', this.city.name, ': ', num);
    this.rating.emit(num);
  }
}
