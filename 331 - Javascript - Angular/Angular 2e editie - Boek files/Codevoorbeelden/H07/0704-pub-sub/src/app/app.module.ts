// app.module.ts
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

// Services
import { CityService } from './shared/services/city.service';
import { OrderService } from './shared/services/order.service';

// Componenten
import { AppComponent } from './app.component';
import { CityDetailComponent } from './city-detail/city-detail.component';
import { CityOrdersComponent } from './city-orders/city-orders.component';

@NgModule({
  declarations: [AppComponent, CityDetailComponent, CityOrdersComponent],
  imports: [BrowserModule, HttpClientModule],
  providers: [CityService, OrderService],
  bootstrap: [AppComponent]
})
export class AppModule {}
