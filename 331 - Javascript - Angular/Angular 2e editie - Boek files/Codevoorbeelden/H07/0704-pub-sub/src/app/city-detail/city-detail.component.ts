import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { City } from '../shared/model/city.model';
import { OrderService } from '../shared/services/order.service';

@Component({
  selector: 'app-city-detail',
  templateUrl: './city-detail.component.html',
  styleUrls: ['./city-detail.component.css']
})
export class CityDetailComponent implements OnInit {
  @Input() city: City;
  @Output() rating: EventEmitter<number> = new EventEmitter<number>();

  constructor(private orderService: OrderService) {}

  ngOnInit() {}

  // Waardering geven aan steden. Deze wordt verstuurd naar de parent component
  rate(num: number): void {
    console.log('rating voor ', this.city.name, ': ', num);
    this.rating.emit(num);
  }

  // Stedentripje boeken. Deze event wordt gegooid als EventEmitter.
  // In de parent component (of andere componenten) kan hier worden .subscribe()'d.
  order(city) {
    console.log(`Stedentripje geboekt voor: 
			${this.city.name}, voor EUR ${this.city.price}`);
    this.orderService.Stream.next(city); // gooi event
  }
}
