// city-orders.component.ts
import { Component, OnInit } from '@angular/core';
import { CityOrderModel } from '../shared/model/cityOrders.model';
import { OrderService } from '../shared/services/order.service';
import { City } from '../shared/model/city.model';

@Component({
  selector: 'app-city-orders',
  templateUrl: './city-orders.component.html',
  styleUrls: ['./city-orders.component.css']
})
export class CityOrdersComponent implements OnInit {
  currentOrders: CityOrderModel[] = [];
  totalPrice: number = 0;

  constructor(private orderService: OrderService) {}

  ngOnInit() {
    this.orderService.Stream.subscribe((city: City) => this.processOrder(city));
  }

  processOrder(city: City): void {
    console.log('Order voor city ontvangen: ', city);
    this.currentOrders.push(new CityOrderModel(city));
    this.calculateTotal();
  }

  calculateTotal() {
    this.totalPrice = 0; // reset
    this.currentOrders.forEach((order: CityOrderModel) => {
      this.totalPrice += order.numBookings * order.city.price;
    });

    // OF: Gebruik de reduce-functie voor arrays
    // this.totalPrice = this.currentOrders
    //     .reduce((acc, order) => acc + order.numBookings * order.city.price, 0)
  }

  cancel() {
    this.currentOrders = [];
  }

  confirm() {
    // POST this.currentOrders.stringify()....etc.
    alert('TODO: order opslaan in database...');
  }
}
