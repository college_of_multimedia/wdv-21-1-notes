// home.component.ts
import { Component, OnInit } from '@angular/core';
import { City } from '../shared/model/city.model';
import { CityService } from '../shared/services/city.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  cities: City[];
  currentCity: City;
  cityPhoto: string = '';

  constructor(private cityService: CityService) {}

  ngOnInit() {
    this.cityService.getCities().subscribe(cities => (this.cities = cities));
  }

  // Stad binden aan view
  showCity(city: City) {
    // TODO
    this.currentCity = city;
    this.cityPhoto = `assets/img/${this.currentCity.name}.jpg`;
  }

  // Stad toevoegen --> doorgeven aan de service
  addCity(cityName: string) {
    // TODO
  }

  // Stad verwijderen --> doorgeven aan de service
  deleteCity(city: City) {
    // TODO
  }
}
