// app.module.ts
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

// Services
import { CityService } from './shared/services/city.service';

// Componenten
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { CityAddComponent } from './city-add/city-add.component';
import { CityDetailComponent } from './city-detail/city-detail.component';

// Routing stuff
const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'add', component: CityAddComponent },
  { path: 'detail/:id/:name', component: CityDetailComponent }
];

@NgModule({
  declarations: [AppComponent, HomeComponent, CityAddComponent, CityDetailComponent],
  imports: [BrowserModule, HttpClientModule, RouterModule.forRoot(routes)],
  providers: [CityService],
  bootstrap: [AppComponent]
})
export class AppModule {}
