// city-detail.component.ts
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {Observable} from 'rxjs';
import {City} from '../shared/model/city.model';
import {CityService} from '../shared/services/city.service';

@Component({
  selector: 'app-city-detail',
  templateUrl: './city-detail.component.html',
  styleUrls: ['./city-detail.component.css']
})
export class CityDetailComponent implements OnInit {
  name: string;
  id: number;
  city$: Observable<City>; // voor ophalen van details per city

  constructor(private activatedRoute: ActivatedRoute,
              private cityService: CityService
  ) {
  }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((route: ParamMap) => {
      console.log(route);
      this.name = route.get('name');
      this.id = +route.get('id'); // Een plusteken '+' in plaats van parseInt()

      // aanvullend: voer een call uit naar een backend,
      // bijvoorbeeld iets als this.cityService.getCity(this.id);
      this.city$ = this.cityService.getCity(this.id);
    });
  }
}
