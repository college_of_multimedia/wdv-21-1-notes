import { Component, OnInit } from '@angular/core';

import {City} from "./shared/model/city.model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  title:string = '';
  name:string = '';
  cities:City[] = [];

  constructor() {}

  ngOnInit() {
    this.title = 'Mijn Favoriete Steden';
    this.name = 'Harald Schilling';
    this.cities = [
      new City(1,'Groningen', 'GR'),
      new City(2,'Hengelo', 'OV'),
      new City(3,'Den Haag', 'ZH'),
      new City(4,'Enschede', 'OV')
    ];
  }
}
