import { Component, OnInit } from '@angular/core';

import {City} from "./shared/model/city.model";

import {CityService} from "./shared/services/city.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  title:string = '';
  name:string = '';
  cities:City[] = []; // bevat een Array met City Objecten
  
  toggleMsg:string = 'Verberg lijst met steden.';
  showCities:boolean = true;

  newCity:string = '';
  newProvincie:string = '';

  currentCity:any = undefined;
  cityPhoto:string = '';

  // Dependency injection
  constructor(private cityService: CityService) {}

  ngOnInit() {
    this.title = 'Mijn Favoriete Steden via een Service';
    this.name = 'Harald';
    this.cities = this.cityService.getCities();
  }

  toggleCities(){
    this.showCities = !this.showCities; 

    this.showCities 
      ? this.toggleMsg = 'Verberg lijst met steden' 
      : this.toggleMsg = 'Toon lijst met steden'; 
    /* this.toggleMsg = this.showCities 
        ? 'Verberg lijst met steden' 
        : 'Toon lijst met steden'; 
    */
  }

  showCity (id:number){
    this.currentCity = this.cityService.getCity(id);
    this.cityPhoto = `assets/img/${ this.currentCity.name }.jpg`;
  }

  addCity(){
    this.cityService.addCity(this.newCity, this.newProvincie);
  }

  removeCity (id:number){
    // this.cityService.removeCity(id);
    console.log('City nr ' + id + ' deleted! (zgn)');
  }


}
