// city.service.ts
import { Injectable } from '@angular/core';
import { City } from '../model/city.model';

@Injectable() export class CityService {

	private cities: City[] = [
		new City(1,'Groningen', 'GR'),
      	new City(2,'Hengelo',   'OV'),
      	new City(3,'Den Haag',  'ZH'),
      	new City(4,'Enschede',  'OV')
	];

	getCities(): City[] {
		return this.cities;
	} 
	
	getCity(id: number): City|undefined {
		return this.cities.find(c => c.id === id);;
	} 

	addCity(cityName: string, cityProv: string){
		let newCity = new City(
			this.cities.length + 1,
			cityName,
			cityProv
		);
		this.cities.push(newCity);
	}
}
