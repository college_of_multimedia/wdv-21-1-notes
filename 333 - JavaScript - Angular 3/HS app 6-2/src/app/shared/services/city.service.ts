// city.service.ts
import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { City } from '../model/city.model';


@Injectable() export class CityService {

	private cities: City[] = [];

	constructor(private http: HttpClient){}

	getCities(): Observable<City[]> {
		return this.http
			.get<City[]>('../assets/data/cities.json')
			.pipe( 
        		tap(result => console.log('Opgehaald via json: ' , result) ) 
     		);
	} 

}
 
   