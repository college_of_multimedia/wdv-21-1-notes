/**
 * Generates the XHTML Friends Network 'rel' string from the inputs.
 *
 * @deprecated 3.5.0
 * @output wp-admin/js/xfn.js
 */
<<<<<<< HEAD
jQuery( function( $ ) {
=======
jQuery( document ).ready(function( $ ) {
>>>>>>> 1d2e1568b3c45049ab1434be2ad142d7c816d7b4
	$( '#link_rel' ).prop( 'readonly', true );
	$( '#linkxfndiv input' ).on( 'click keyup', function() {
		var isMe = $( '#me' ).is( ':checked' ), inputs = '';
		$( 'input.valinp' ).each( function() {
			if ( isMe ) {
				$( this ).prop( 'disabled', true ).parent().addClass( 'disabled' );
			} else {
				$( this ).removeAttr( 'disabled' ).parent().removeClass( 'disabled' );
				if ( $( this ).is( ':checked' ) && $( this ).val() !== '') {
					inputs += $( this ).val() + ' ';
				}
			}
		});
		$( '#link_rel' ).val( ( isMe ) ? 'me' : inputs.substr( 0,inputs.length - 1 ) );
	});
});
