<?php get_header(); ?>
<section id="main" class="wrapper">
	<div class="inner">						
    
		<header>
		    <h1>Niet gevonden</h1>
            <p>Helaas, de door u opgevraagde pagina bestaat niet.</p>
		</header>
		
        <div class="row">
        	<div class="6u 12u$(small)">
                <p>Misschien helpen deze links u verder:</p>
                <ul>
                    <?php wp_list_pages('title_li=&sort_column=menu_order'); ?>
                </ul>	
            </div>
        </div>
		
	</div>
</section>
                      
<?php 
get_footer();
