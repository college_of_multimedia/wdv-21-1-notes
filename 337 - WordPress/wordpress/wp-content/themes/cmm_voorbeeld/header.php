<!doctype html>
<html <?php language_attributes() ?>>
<head>
	<meta charset="<?php bloginfo('charset') ?>">
	<title><?php the_title() ?></title>

	<meta name="viewport" content="width=device-width, initial-scale=1" />

<?php wp_head() ?>
</head>

<body id="top" <?php body_class(); ?>> 
	<?php wp_body_open() ?>

	<!-- Header -->
	<header id="header">

		<!-- Logo -->
		<div class="logo">
			<a href="<?php bloginfo('url') ?>"><?php bloginfo('name') ?></a>
		</div>

		<!-- Nav -->
		<nav id="nav">
			<?php
			$defaults = [
				'theme_location'  => 'primary',
				'menu'            => '',
				'container'       => '',
				'container_class' => '',
				'container_id'    => '',
				'menu_class'      => 'localisation',
				'menu_id'         => 'localisation',
				'echo'            => true,
				'fallback_cb'     => 'wp_page_menu',
			];							
			wp_nav_menu( $defaults );							
			?>	
		</nav>

	</header>
