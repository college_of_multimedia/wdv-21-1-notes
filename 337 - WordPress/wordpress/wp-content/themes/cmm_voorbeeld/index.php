<?php get_header(); ?>
<?php
// voorbeeld debug, zie functions.php
// dump( get_post() );
?>
<section id="main" class="wrapper">
	<div class="inner">						
    
		<header>
		    <h1><?php the_title() ?></h1>
		</header>
		
        <div class="row">
        	<div class="12u">
				<?php the_content() ?>		
            </div>
          
        </div>
		
	</div>
</section>
                      
<?php 
get_footer();
