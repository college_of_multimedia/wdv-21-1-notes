<?php get_header(); ?>
<section id="main" class="wrapper">
	<div class="inner">						
    
		<header>
		    <h1>BLOG PAGE: <?php the_title() ?></h1>
		</header>
		
        <div class="row">
        	<div class="6u 12u$(small)">
				<?php the_content() ?>		
            </div>
            <div class="6u$ 12u$(small)">
            	<?php if ( has_post_thumbnail() ) : ?>
                    <span class="image fit">
                    	<img src="<?php the_post_thumbnail_url() ?>" alt="<?php the_title() ?>">
                    </span>
                <?php endif; ?>
            	
            </div>
        </div>
		
	</div>
</section>
                      
<?php 
get_footer();
