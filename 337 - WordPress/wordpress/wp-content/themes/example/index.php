<?php get_header() ?>
<?php the_post() ?>

<!-- Main -->
			<section id="main" class="wrapper">
				<div class="inner">						
                
					<header>
						<h1><?php the_field('titel'); ?></h1>					
					</header>
					
                    <div class="row">
                    	<div class="6u 12u$(small)">
							<header>
                                <h2><?php the_title(); ?></h2>                               
                            </header>
							<?php the_content(); ?>		
                        </div>
                        <div class="6u$ 12u$(small)">
                        	<?php if ( has_post_thumbnail() ) { ?>
                                <span class="image fit"><img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_field('titel'); ?>"></span>
                            <?php } ?>
                        	
                        </div>
                    </div>
					
				</div>
			</section>
                      

<?php get_footer() ?>
		