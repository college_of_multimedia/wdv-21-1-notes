<section style="padding:20px 0;"  class="wrapper">
            <div class="container">
                <form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                    <div style="text-align:center;"> 
                    	<br>    
                        <input style="max-width:300px; display:inline;" type="text" placeholder="Zoeken in deze website..." value="<?php the_search_query(); ?>" name="s" id="s" />
                        <a href="javascript:document.getElementById('searchform').submit();" class="button icon fa-search" id="searchsubmit">Zoeken...</a>            		</div>
                </form>
            </div>
</section>