<?php get_header() ?>
<?php the_post() ?>

<!-- Main -->
			<section id="main" class="wrapper">
				<div class="inner">						
                
					<header>
						<h1><?php the_title(); ?></h1>
					</header>
					
                    <div class="row">
                    	<div class="6u 12u$(small)">							
							<?php the_content(); ?>	
                            
                            <p><em><?php 
                                        the_tags(
                                            '<i class="fa fa-tags"></i> ', // before
                                            ', ', // separator
                                            '<br>' // after
                                        ); 
                                        ?>                           
                                       <?php echo _('Geplaatst op:&nbsp; '); the_time('j F Y'); echo _(' &nbsp;in:&nbsp;'); the_category(', '); ?></em></p>  	
                        </div>
                        <div class="6u$ 12u$(small)">
                        	<?php if ( has_post_thumbnail() ) { ?>
                                <span class="image fit"><img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>"></span>
                            <?php } ?>                        	
                        </div>
                    </div>
					
				</div>
			</section>
                      

<?php get_footer() ?>
		