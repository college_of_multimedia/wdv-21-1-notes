<?php
/**
 * Custom classname block support flag.
 *
 * @package WordPress
<<<<<<< HEAD
 * @since 5.6.0
=======
>>>>>>> 1d2e1568b3c45049ab1434be2ad142d7c816d7b4
 */

/**
 * Registers the custom classname block attribute for block types that support it.
 *
<<<<<<< HEAD
 * @since 5.6.0
=======
>>>>>>> 1d2e1568b3c45049ab1434be2ad142d7c816d7b4
 * @access private
 *
 * @param WP_Block_Type $block_type Block Type.
 */
function wp_register_custom_classname_support( $block_type ) {
<<<<<<< HEAD
	$has_custom_classname_support = block_has_support( $block_type, array( 'customClassName' ), true );

=======
	$has_custom_classname_support = true;
	if ( property_exists( $block_type, 'supports' ) ) {
		$has_custom_classname_support = _wp_array_get( $block_type->supports, array( 'customClassName' ), true );
	}
>>>>>>> 1d2e1568b3c45049ab1434be2ad142d7c816d7b4
	if ( $has_custom_classname_support ) {
		if ( ! $block_type->attributes ) {
			$block_type->attributes = array();
		}

		if ( ! array_key_exists( 'className', $block_type->attributes ) ) {
			$block_type->attributes['className'] = array(
				'type' => 'string',
			);
		}
	}
}

/**
 * Add the custom classnames to the output.
 *
<<<<<<< HEAD
 * @since 5.6.0
=======
>>>>>>> 1d2e1568b3c45049ab1434be2ad142d7c816d7b4
 * @access private
 *
 * @param  WP_Block_Type $block_type       Block Type.
 * @param  array         $block_attributes Block attributes.
 *
 * @return array Block CSS classes and inline styles.
 */
function wp_apply_custom_classname_support( $block_type, $block_attributes ) {
<<<<<<< HEAD
	$has_custom_classname_support = block_has_support( $block_type, array( 'customClassName' ), true );
	$attributes                   = array();
=======
	$has_custom_classname_support = true;
	$attributes                   = array();
	if ( property_exists( $block_type, 'supports' ) ) {
		$has_custom_classname_support = _wp_array_get( $block_type->supports, array( 'customClassName' ), true );
	}
>>>>>>> 1d2e1568b3c45049ab1434be2ad142d7c816d7b4
	if ( $has_custom_classname_support ) {
		$has_custom_classnames = array_key_exists( 'className', $block_attributes );

		if ( $has_custom_classnames ) {
			$attributes['class'] = $block_attributes['className'];
		}
	}

	return $attributes;
}

// Register the block support.
WP_Block_Supports::get_instance()->register(
	'custom-classname',
	array(
		'register_attribute' => 'wp_register_custom_classname_support',
		'apply'              => 'wp_apply_custom_classname_support',
	)
);
