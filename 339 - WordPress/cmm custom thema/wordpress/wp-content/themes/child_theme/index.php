<?php get_header(); ?>
<?php
// voorbeeld debug, zie functions.php
// dump( get_post() );
?>
<section id="main" class="wrapper">
	Dit is mijn Child theme index pagina
	
	<?php if ( have_posts() ) :?>

		<?php while( have_posts() ) : ?>
			<?php the_post() ?>
			<div class="inner">						
		    
				<header>
				    <h1><?php the_title() ?></h1>
				</header>
				Post ID: <?= get_the_ID() ?>
		        <div class="row">
		        	<div class="12u">
						<em><?php the_excerpt() ?></em>
		            </div>
		          
		        </div>
				
			</div>
		<?php endwhile; ?>

	<?php endif; ?>
</section>
                      
<?php 
get_footer();
