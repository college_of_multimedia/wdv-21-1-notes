<?php get_header(); ?>
<?php 
$style = '';
if( get_field('achtergrond_kleur') ) {
	$style = 'background: ' . get_field('achtergrond_kleur');
}
?>
<section id="main" class="wrapper" style="<?= $style ?>" >
	<div class="inner">						
    
		<header>
		    <h1>BLOG PAGE TEMPLATE: <?php the_title() ?></h1>
		    <h2><?php the_field( 'sub_titel' ) ?>
		</header>
		
        <div class="row">
        	<div class="6u 12u$(small)">
				<?php the_content() ?>		
            </div>
            <div class="6u$ 12u$(small)">
            	<?php if ( has_post_thumbnail() ) : ?>
                    <span class="image fit">
                    	<img src="<?php the_post_thumbnail_url() ?>" alt="<?php the_title() ?>">
                    </span>
                <?php endif; ?>
            	
            </div>
        </div>
		
	</div>
</section>
                      
<?php 
get_footer();
