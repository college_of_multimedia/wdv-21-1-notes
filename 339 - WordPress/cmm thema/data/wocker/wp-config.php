<?php

// BEGIN iThemes Security - Deze regel niet wijzigen of verwijderen
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Bestandseditor uitschakelen - Beveiliging > Instellingen > WordPress tweaks > Bestandseditor
// END iThemes Security - Deze regel niet wijzigen of verwijderen

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wordpress' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          '.Btv+*@1Rfgc#EDMjk~^(Txb:.p$d(1o>S/UaQVUWEc/faW#Gk763 1 k!y0 !;E' );
define( 'SECURE_AUTH_KEY',   'yTUG8=CMB6mh_S9)H6WBu>h>_)mJ&+rE0cxOr(pF!rAMm]]3-Z_(zbz=lx;AgWP!' );
define( 'LOGGED_IN_KEY',     'Y`vuq#ef(k:/D)rE5XTri6BsmA^<7tXm|jGf<iCZ~)/BK]k7O;!4bXX?_m;<w^-.' );
define( 'NONCE_KEY',         '+?<ar/1Fiq i6[2u#8I6tJ;x#U):B9V#zRJ[_R7U<|U_LFU_j<M8S#6zSWJH0aH]' );
define( 'AUTH_SALT',         'nI?w8jD2jd(pR$<O<s9tM3+1?hng722UBi^[Y0tax Au3y+Nkpcg,Eih+}9+MdnA' );
define( 'SECURE_AUTH_SALT',  'P}W>Sa!?m[F4cN #Ez8@R@493Z.dl)!=)B7K%;||#tmCC<_NhyT5d?&yFJ4HLv2o' );
define( 'LOGGED_IN_SALT',    '-!ueqPtlV/:O*?4~{f(1<vF{&:anZk=,TmJgOBA <07Ca+oPaIPCs06&[?>%Gkcn' );
define( 'NONCE_SALT',        '%!x,CC_n]1#^T>Xpx;`pH)$7?z&AI4brmUo57A6]~BfOWD*Z[H.*3iKK fe%*om1' );
define( 'WP_CACHE_KEY_SALT', 'Kl`Qkpn,&9Jh/^yG6C`e>+VHvq5zT)ZZf%QoEL]1u+u[wn [_^<QT01R1(HII0&Y' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


define( 'WP_DEBUG', true );
define( 'WP_DEBUG_LOG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/* Multisite */

define( 'MULTISITE', true );
define( 'SUBDOMAIN_INSTALL', true );
define( 'DOMAIN_CURRENT_SITE', 'cmm.test' );
define( 'PATH_CURRENT_SITE', '/' );
define( 'SITE_ID_CURRENT_SITE', 1 );
define( 'BLOG_ID_CURRENT_SITE', 1 );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
