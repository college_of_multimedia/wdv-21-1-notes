<?php get_header(); ?>
<?php
// voorbeeld debug, zie functions.php
// dump( get_post() );
?>
<section id="main" class="wrapper">
	<h3>Dit is mijn Child theme index pagina</h3>


<?php
// informatie ophalen uit de options tabel
//$testOption = get_option('wp_user_roles');

//var_dump( $testOption['editor'] );

?> 
	
	<?php if ( have_posts() ) :?>

		<?php while( have_posts() ) : ?>
			<?php the_post() ?>
			<div class="inner">						
		    
				<header>
				    <h1><a href="<?= get_permalink( ) ?>"><?php the_title() ?></a></h1>
				</header>
				Post ID: <?= get_the_ID() ?>
		        <div class="row">
		        	<div class="12u">
						<em><?php the_excerpt() ?></em>
		            </div>
		          
		        </div>
				
			</div>
		<?php endwhile; ?>

	<?php endif; ?>
</section>
                      
<?php 
get_footer();
