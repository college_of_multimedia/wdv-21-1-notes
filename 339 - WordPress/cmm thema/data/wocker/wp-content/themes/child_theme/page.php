<?php get_header(); ?>

<?php if ( !have_posts() ) return; ?>

<section id="main" class="wrapper">
	
	<?php while( have_posts() ) : ?>
		<?php the_post() ?>
		<div class="inner">						
	    
			<header>
			    <h1><?php the_title() ?></h1>
			</header>

	        <div class="row">
	        	<div class="12u">
					<?php the_content() ?>
	            </div>
	          
	        </div>
			
		</div>
	<?php endwhile; ?>
</section>
                      
<?php 
get_footer();
