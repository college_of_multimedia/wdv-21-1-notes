<?php get_header(); ?>
<section id="main" class="wrapper">
	<div class="inner">						
    
		<header>
		    <h1>Niet gevonden</h1>
            <p>Helaas, de door u opgevraagde pagina bestaat niet.</p>
		</header>
		
        <div class="row">
        	<div class="6u 12u$(small)">
                <p>Misschien helpen deze links u verder:</p>
                <ul>
                    <?php wp_list_pages('title_li=&sort_column=menu_order'); ?>
                </ul>

                <ul>
                // Haal de laatste posts op in een loop
                <?php
				$args = [
				  'numberposts' => 2,
				  'post_type'   => 'post'
				];
				 
				// $posts = get_posts( $args );
				$posts = wp_get_recent_posts( $args, OBJECT );
				foreach( $posts as $post ) : ?>
					<li>
						<a href="<?= get_permalink( $post->ID ) ?>">
						<?= get_the_title( $post->ID ) ?>
					</a>
					<br>
					<em><?= get_the_excerpt( $post->ID ) ?></em>
					</li>
				<?php endforeach; ?>
				</ul>
            </div>
        </div>
		
	</div>
</section>
                      
<?php 
get_footer();
