<?php

/**
 * [dump description]
 * @param  [type] $stuff [description]
 * @return [type]        [description]
 */
function dump($stuff) {
	// lokaal werkt dit niet omdat mijn ip dat "::1" is
	// maar op productie zorgt dit ervoor dat niemand buiten mijn mijn netwerk de debug informatie ziet.
	if ( '95.97.52.34' != $_SERVER['REMOTE_ADDR'] ) {
		return;
	}

	echo '<pre>';
	var_dump($stuff);
	echo '</pre>';
		
}

/**
 * Toevoegen van javascript includes in de footer
 * 
 * [enqueueScripts description]
 * @return [type] [description]
 */
function enqueueScripts() {
	wp_enqueue_script( 
		'cmmvoorbeeld', 
		get_template_directory_uri() . '/assets/js/main.js', 
		[ 'jquery' ],
		1.0,
		true );
}
add_action( 'wp_enqueue_scripts', 'enqueueScripts' );

/**
 * Toevoegen van styles ( css scripts ) in de header 
 * 
 * [enqueueStyles description]
 * @return [type] [description]
 */
function enqueueStyles() {
	// voorbeeld wanneer er een extra css file nodig is
	// wp_enqueue_style( 
	// 	'linden',
	// 	get_template_directory_uri() . '/assets/css/linden.css'
	// );
 //    wp_enqueue_style( 
 //    	'cmmvoorbeeld', 
 //    	get_template_directory_uri() . '/assets/css/main.css',
 //    	['linden'],
 //    	1.1
 //    );

    wp_enqueue_style( 
    	'cmmvoorbeeld', 
    	get_template_directory_uri() . '/assets/css/main.css',
    	[],
    	1.1
    );
}
add_action( 'wp_enqueue_scripts', 'enqueueStyles' );

/**
 * See https://codex.wordpress.org/Plugin_API/Action_Reference
 * for the init hook: https://developer.wordpress.org/reference/hooks/init
 */
/**
 * Voorbeeld om de website pagina te downloaden in plaats van weer te gevne in de browser
 * [initFunction description]
 * @return [type] [description]
 */
function initFunction() {
	header('Content-Disposition: attachment; filename=voorbeeld.txt');
	header('Content-Type: application/octet-stream');
	ob_clean();
}
// add_action( 'init', 'initFunction', 25 );
// In de WordPress code base staat deze functie:
// do_action( 'init' );



/**
 * Title filter, titel aanpassen
 */
// voorbeeld titel aanpassen
function customTitle( string $postTitle, int $postId ):string
{
	return 'MijnID:'. $postId . ' titel: ' . $postTitle;
}
// add_filter( 'the_title', 'customTitle', 11, 2 );

// voorbeeld lowercase
function lowercaseTitle( string $postTitle, int $postId ):string
{
	return strtolower( $postTitle );
}
// add_filter( 'the_title', 'lowercaseTitle', 10, 2 );

// reset van titel, overschrijf alle andere title filters
function resetTitle( string $postTitle, int $postId ):string
{
	$post = get_post( $postId );
	return ucfirst( strtolower( $post->post_title ) );
}
// add_filter( 'the_title', 'resetTitle', 15, 2 );



/**
 * Theme Support
 */
register_nav_menus( [
    'primary' 			=> __( 'Hoofdmenu', 'localisation' ),
	'primary_mobile' 	=> __( 'Hoofdmenu op mobiel', 'localisation' ),
	'secundairy' 		=> __( 'Footermenu', 'localisation' ),
] );


// Post thumbnail
add_theme_support( 'post-thumbnails' );


// Editor CSS
function theme_add_editor_styles() {
    add_editor_style( '/assets/css/style-admin.css' );
}
add_action( 'admin_init', 'theme_add_editor_styles' );

/**
 * Verwijderen van sommige elementen uit het admin menu
 */
function actionCleanupAdminMenu() {
	// niet uitvoeren als je niet in de admin omgeving zit
	if ( ! is_admin() ) {
		return;
	}

	// de gebruiker 'admin' mag wel alles zien
	$user = wp_get_current_user();
	if ( 'admin' === $user->display_name ) {
		return;
	}


	//remove_menu_page( 'tools.php' );
	remove_menu_page( 'themes.php' );
	remove_submenu_page( 'themes.php', 'themes.php' );
	remove_menu_page( 'edit-comments.php' );
}
add_action( 'admin_menu', 'actionCleanupAdminMenu' );

function actionCleanupHead() {
	// EditURI link
	remove_action( 'wp_head', 'rsd_link' );

	// Category feed links
	remove_action( 'wp_head', 'feed_links_extra', 3 );

	// Post and comment feed links
	remove_action( 'wp_head', 'feed_links', 2 );
	remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );

	// Windows Live Writer
	remove_action( 'wp_head', 'wlwmanifest_link' );

	// Index link
	remove_action( 'wp_head', 'index_rel_link' );

	// Previous link
	remove_action( 'wp_head', 'parent_post_rel_link', 10 );

	// Start link
	remove_action( 'wp_head', 'start_post_rel_link', 10 );

	// Canonical
	remove_action( 'wp_head', 'rel_canonical', 10 );

	// Shortlink
	remove_action( 'wp_head', 'wp_shortlink_wp_head', 10 );

	// Links for adjacent posts
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10 );

	// WP version
	remove_action( 'wp_head', 'wp_generator' );

	remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
	remove_action( 'wp_head', 'wp_oembed_add_host_js' );
	remove_action( 'rest_api_init', 'wp_oembed_register_route' );
	remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );


	add_filter( 'gform_init_scripts_footer', '__return_true' );
}
add_action( 'init', 'actionCleanupHead' );


/**
 * Register our sidebars and widgetized areas.
 */
function customWidgets() {
	register_sidebar( array(
		'name'          => 'Single post right sidebar',
		'id'            => 'single_right_1',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => 'Footer part',
		'id'            => 'footer_part',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'customWidgets' );

function initCustomPostType() {
	 register_post_type( 'books',
        array(
            'labels' => array(
                'name' 			=> __( 'Boeken' ),
                'singular_name' => __( 'Boek' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'boek'),
            'show_in_rest' => true,
        )
    );
	$labels = array(
	    'name' => _x( 'Tags', 'taxonomy general name' ),
	    'singular_name' => _x( 'Tag', 'taxonomy singular name' ),
	    'search_items' =>  __( 'Search Tags' ),
	    'popular_items' => __( 'Popular Tags' ),
	    'all_items' => __( 'All Tags' ),
	    'parent_item' => null,
	    'parent_item_colon' => null,
	    'edit_item' => __( 'Edit Tag' ), 
	    'update_item' => __( 'Update Tag' ),
	    'add_new_item' => __( 'Add New Tag' ),
	    'new_item_name' => __( 'New Tag Name' ),
	    'separate_items_with_commas' => __( 'Separate tags with commas' ),
	    'add_or_remove_items' => __( 'Add or remove tags' ),
	    'choose_from_most_used' => __( 'Choose from the most used tags' ),
	    'menu_name' => __( 'Tags' ),
	  ); 


	  register_taxonomy('tag','books', array(
	    'hierarchical' => true,
	    'labels' => $labels,
	    'show_ui' => true,
	    'update_count_callback' => '_update_post_term_count',
	    'query_var' => true,
	    'rewrite' => array( 'slug' => 'tag' ),
	  ));

    remove_post_type_support( 'books', 'comments' );
    remove_post_type_support( 'books', 'commentstatus' );
    add_post_type_support( 'books', 'thumbnail' );
}
add_action( 'init', 'initCustomPostType' );


/**
 * Custom filter
 */
function editBookDate( string $date, int $postId ): string
{
	if ( ! is_user_logged_in() ) {
		return $date;
	}
	return 'Aangepaste datum ' . get_the_modified_date( 'c', $postId );
}
add_filter( 'book_date', 'editBookDate', 10, 2 );


