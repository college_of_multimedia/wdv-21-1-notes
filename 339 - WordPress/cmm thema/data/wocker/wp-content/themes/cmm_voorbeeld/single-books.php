<?php get_header(); ?>
<section id="main" class="wrapper" >
	<div class="inner">						
    
		<header>
		    <h1>Boek: <?php the_title() ?></h1>
		    <em><?php the_field( 'authorTekst' ) ?></em>
		    <b><?= apply_filters( 'book_date', get_the_date(), get_the_ID() ) ?></b>
		</header>
		
        <div class="row">

        	<div class="6u 12u$(small)">
				<?php the_content() ?>		
            </div>

            <div class="6u$ 12u$(small)">

            	<?php if ( has_post_thumbnail() ) : ?>
                    <span class="image fit">
                    	<img src="<?php the_post_thumbnail_url() ?>" alt="<?php the_title() ?>">
                    </span>
                <?php endif; ?>
            	
            </div>
        </div>
		
	</div>
</section>
                      
<?php 
get_footer();
