<?php get_header(); ?>
<?php 
$style = '';
if( get_field('achtergrond_kleur') ) {
	$style = 'background: ' . get_field('achtergrond_kleur');
}
?>
<section id="main" class="wrapper" style="<?= $style ?>" >
	<div class="inner">						
    
		<header>
		    <h1>BLOG PAGE TEMPLATE: <?php the_title() ?></h1>
		    <h2><?php the_field( 'sub_titel' ) ?></h2>
		</header>
		
        <div class="row">

        	<div class="6u 12u$(small)">
				<?php the_content() ?>		
            </div>

            <div class="6u$ 12u$(small)">
				<?php do_shortcode('[contact-form-7 id="43" title="Contactformulier 1"]'); ?>
				
                <?php dynamic_sidebar( 'single_right_1' ); ?>

            	<?php if ( has_post_thumbnail() ) : ?>
                    <span class="image fit">
                    	<img src="<?php the_post_thumbnail_url() ?>" alt="<?php the_title() ?>">
                    </span>
                <?php endif; ?>
            	
            </div>
        </div>
		
	</div>
</section>
                      
<?php 
get_footer();
