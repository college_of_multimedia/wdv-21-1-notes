<?php

// definieer de namespace voor dit project
namespace library;

// de autload functionaliteit die automatisch de classes laad.
require_once( '../library/autoload.php' );


$database = new library\Controllers\DatabaseController();

$taart = new library\Models\Taart( $database );

echo $taart->getTitle();



function doeIets( $db ) {
	$res =  $db->runQuery('SELECT * FROM ....');
	return $res->getFirstRow();
}