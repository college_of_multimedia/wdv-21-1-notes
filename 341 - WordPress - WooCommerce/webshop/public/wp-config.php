<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'webshop' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ' Xg[8?Hx)kx+3j2D3AVqU&=B9O(tK8 ~rKR0]j~^z:ON(PAE=JBzRC31Hty:s;Yg' );
define( 'SECURE_AUTH_KEY',  'nD=U%pXkV;dN%CJjFQo^^jB,a#Orwu#+z5,[{dytpV@]{GWl4Ze#x1n5%:f(c&he' );
define( 'LOGGED_IN_KEY',    '9d[T:MD/ev^yVlV,P`NG@n]j2)3J1}[[m<?_e5@s%)~9;U~~tl@!eNjGmJ=QNEh.' );
define( 'NONCE_KEY',        'TqT8XK+LI+_e96Vh-=?kEy@tln,aZ=aYsb%NT7W[gLQl)CL}O.D~GEKl:Jb#4kf<' );
define( 'AUTH_SALT',        '#y[w&k/Z;M:d/d~`X4{:cn$kFdC,VR9cO;v{!0l.6[6wp[I+,!CHM^yp.EP&_@ll' );
define( 'SECURE_AUTH_SALT', 'M,I8ph+wfIQvcfh.i:T<<1[p(-h$.NfDJ1V-AoSzRnQr`qMNjmBn;vj[N`Qgq>uk' );
define( 'LOGGED_IN_SALT',   'DKQ+dT+^32}~*n8MO,.ud-0`HXU-? ~s6xb(oi-r#4W!nsjUc/J(9{{oJ$}@,}(f' );
define( 'NONCE_SALT',       'uN)n9{RB+=TqzEJA&RBSvYx+0(_@}K=ducnW>jppgwui1sT3M:34Fec>; hcl)NO' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'ws_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
