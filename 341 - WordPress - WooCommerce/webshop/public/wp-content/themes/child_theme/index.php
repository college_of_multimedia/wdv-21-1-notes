<?php

namespace library;

get_header(); 


// voorbeeld debug, zie functions.php
// Helpers::dump( get_post() );
?>
<section id="main" class="wrapper">
	<div class="inner">		
		<header>
			<h1>Dit is mijn Child theme index pagina</h1>
		</header>
		<?php
			Helpers::dump( 'Dit is mijn index pagina' );
		?>

		<?php if ( have_posts() ) :?>

			<?php while( have_posts() ) : ?>
				<?php the_post() ?>				
			 
				<div class="row">
					<div class="12u">
						<h2><a href="<? the_permalink() ?>"><?php the_title() ?></a></h2>
						Post ID: <?= get_the_ID() ?>
						<em><?php the_excerpt() ?></em>
					</div>
				</div>
			<?php endwhile; ?>

		<?php endif; ?>
	</div>
</section>
                      
<?php 
get_footer();
