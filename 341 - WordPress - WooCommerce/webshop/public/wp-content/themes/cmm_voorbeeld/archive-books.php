<?php get_header(); ?>

<section id="main" class="wrapper">
	<div class="inner">	

	<h1>Dit is mijn Boeken overzicht pagina.</h1>
	<br>

	<?php if ( have_posts() ) :?>

		<?php while( have_posts() ) : ?>
			<?php the_post() ?>

		        <div class="row">
		    		<div class="6u">
				    	<h2><a href="<? the_permalink() ?>"><?php the_title() ?></a></h2>
		        		<em><?php the_excerpt() ?></em>
		            </div>
		        	<div class="6u">
						Post ID: <?= get_the_ID() ?>
					
		            	<?php if ( has_post_thumbnail() ) : ?>
		            		<?php
		            		// Hier haal ik de uitgeligde afbeelding van deze post op.
		            		// door gebruik te maken van wp_get_attachment_image() 
		            		// krijg ik een compleet img object terug.
		            		// inclusief de src variaties.
		            		// De 'book_thumbnail' is een afmeting die ik ingesteld heb in de ThemeSupport();
		            		?>
		            		<a href="<? the_permalink() ?>">
			                    <span class="image">
			                    	<?= wp_get_attachment_image( get_post_thumbnail_id() , 'book_thumbnail', false, ['alt'=>get_the_title()] ) ?>
			                    </span>
		                	</a>
		                <?php endif; ?>
		        	</div>
		          
		        </div>
		<?php endwhile; ?>

	<?php endif; ?>
				
	</div>
</section>
                      
<?php 
get_footer();
