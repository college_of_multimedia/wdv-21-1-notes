<!-- Footer -->
	<footer id="footer">
		<div class="inner">
			<div class="split style1">
				<div class="contact">
					<h2>Contact</h2>
					<ul class="contact-icons">
						<li class="icon fa-home">Egelenburg 150 – 152<br>1081 GK Amsterdam</li>
						<li class="icon fa-phone"><a href="tel:0204623939">020 – 462 39 39</a></li>
						<li class="icon fa-envelope-o"><a href="mailto:info@cmm.nl">info@cmm.nl</a></li>
					</ul>
					<?php						
					wp_nav_menu( ['theme_location'  => 'secundairy'] );
					?>	
                <?php dynamic_sidebar( 'footer_part' ); ?>
				</div>
				<form action="#" method="post">
					<h2>Email Us</h2>
					<div class="field half first">
						<input name="name" id="name" placeholder="Name" type="text" />
					</div>
					<div class="field half">
						<input name="email" id="email" placeholder="Email" type="email" />
					</div>
					<div class="field">
						<textarea name="message" id="message" rows="6" placeholder="Message"></textarea>
					</div>
					<ul class="actions">
						<li><input value="Send Message" class="button" type="submit" /></li>
					</ul>
				</form>
			</div>
			<div class="copyright">
				<p>&copy; <?= date('Y') ?> - <?php bloginfo('name') ?> - Alle rechten voorbehouden.</p>
			</div>
		</div>
	</footer>

<?php wp_footer() ?>
</body>
</html>