<?php

// definieer de namespace voor dit project
namespace library;

// de autload functionaliteit die automatisch de classes laad.
require_once( 'library/autoload.php' );

// require_once( 'library/helpers.php' );
// require_once( 'library/admin.php' );
// require_once( 'library/actions.php' );
// require_once( 'library/filters.php' );
// require_once( 'library/enqueueScripts.php' );
// require_once( 'library/themeSupport.php' );

// Wat we nu gaan doen is:
// NameSpacing
/*
- autoloader implementeren
- op basis van classes
- classname is gelijk aan document naam
*/

new Admin;
new Filters;
new Actions;
new EnqueueScripts;
new ThemeSupport;
new PostTypes;