<?php get_header(); ?>
<section id="main" class="wrapper">
	<div class="inner">						
    
		<header>
		    <h1><?php the_title() ?></h1>
		</header>
		
        <div class="row">
        	<div class="12u">
				<?php the_content() ?>		
            </div>
          
        </div>
		
	</div>
</section>
                      
<?php 
get_footer();
