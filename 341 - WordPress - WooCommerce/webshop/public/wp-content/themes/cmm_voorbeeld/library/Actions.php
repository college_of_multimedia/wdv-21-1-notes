<?php
/**
 * See https://codex.wordpress.org/Plugin_API/Action_Reference
 * for the init hook: https://developer.wordpress.org/reference/hooks/init
 */
namespace library;

class Actions
{
    public function __construct()
    {
        // mijn constructor  
        // add_action( 'init', [ $this, 'initFunction' ], 25 );
        // In de WordPress code base staat deze functie:
        // do_action( 'init' );
    }

    /**
     * Voorbeeld om de website pagina te downloaden in plaats van weer te gevne in de browser
     * [initFunction description]
     * @return [type] [description]
     */
    public function initFunction() {
    	header('Content-Disposition: attachment; filename=voorbeeld.txt');
    	header('Content-Type: application/octet-stream');
    	ob_clean();
    }

}