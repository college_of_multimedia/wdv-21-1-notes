<?php

namespace library;

class Admin
{
	public function __construct()
	{
		// Post thumbnail
		add_theme_support( 'post-thumbnails' );
		
		add_action( 'admin_init', [ $this, 'themeAddEditorStyles' ] );

		add_action( 'admin_menu', [ $this, 'actionCleanupAdminMenu' ] );

	}

	/**
	 * Custom css bij de admin
	 */
	public function themeAddEditorStyles() {
	    add_editor_style( '/assets/css/style-admin.css' );
	}

	/**
	 * Verwijderen van sommige elementen uit het admin menu
	 */
	public function actionCleanupAdminMenu() {
		// niet uitvoeren als je niet in de admin omgeving zit
		if ( ! is_admin() ) {
			return;
		}

		// de gebruiker 'admin' mag wel alles zien
		$user = wp_get_current_user();
		if ( 'admin' === $user->display_name ) {
			return;
		}

		//remove_menu_page( 'tools.php' );
		remove_menu_page( 'themes.php' );
		remove_submenu_page( 'themes.php', 'themes.php' );
		remove_menu_page( 'edit-comments.php' );
	}

	/**
	 * Voorbeeld functie die ik ergens anders ga gebruiken.
	 */
	public function getAdminStatus() {
		//return $this->db;
		return 'dit komt uit de Admin.php';
	}
}
