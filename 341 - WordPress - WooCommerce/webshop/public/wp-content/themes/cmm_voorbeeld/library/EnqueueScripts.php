<?php

namespace library;

class EnqueueScripts
{
    public function __construct()
    {
		add_action( 'wp_enqueue_scripts', [ $this, 'enqueueScripts' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'enqueueStyles' ] );
    }
    
	/**
	 * Toevoegen van javascript includes in de footer
	 * 
	 * [enqueueScripts description]
	 * @return [type] [description]
	 */
	public function enqueueScripts() {
		wp_enqueue_script( 
			'cmmvoorbeeld', 
			get_template_directory_uri() . '/assets/js/main.js', 
			[ 'jquery' ],
			1.0,
			true );
	}

	/**
	 * Toevoegen van styles ( css scripts ) in de header 
	 * 
	 * [enqueueStyles description]
	 * @return [type] [description]
	 */
	public function enqueueStyles() {
	    wp_enqueue_style( 
	    	'cmmvoorbeeld', 
	    	get_template_directory_uri() . '/assets/css/main.css',
	    	[],
	    	1.1
	    );
	}
}
