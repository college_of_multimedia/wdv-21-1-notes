<?php

namespace library;

class Filters
{
    public function __construct()
    {
		// add_filter( 'the_title', [ $this, 'customTitle' ], 11, 2 );
		// add_filter( 'the_title', [ $this, 'lowercaseTitle' ], 10, 2 );
		// add_filter( 'the_title', [ $this, 'resetTitle' ], 15, 2 );
		add_filter( 'book_date', [ $this, 'editBookDate' ], 10, 2 );
    }
    
	/**
	 * Title filter, titel aanpassen
	 */
	// voorbeeld titel aanpassen
	public function customTitle( string $postTitle, int $postId ):string
	{
		return 'MijnID:'. $postId . ' titel: ' . $postTitle;
	}

	// voorbeeld lowercase
	public function lowercaseTitle( string $postTitle, int $postId ):string
	{
		return strtolower( $postTitle );
	}

	// reset van titel, overschrijf alle andere title filters
	public function resetTitle( string $postTitle, int $postId ):string
	{
		$post = get_post( $postId );
		return ucfirst( strtolower( $post->post_title ) );
	}

	/**
	 * Custom filter
	 */
	public function editBookDate( string $date, int $postId ): string
	{
		if ( ! is_user_logged_in() ) {
			return $date;
		}
		return 'Aangepaste datum ' . get_the_modified_date( 'c', $postId );
	}

}