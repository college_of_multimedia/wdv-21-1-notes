<?php
/**
 * Deze functie laad ik wanneer die nodig is.
 * In het betreffende document moet ik ervoor zorgen dat de namespace bekend is door bovenin het document de namespace te defineren
 * namespace library;
 * 
 * In het document kan ik vervolgend de 'dump' functie aanroepen:
 * 
 * <?php Helpers::dump( 'testje' ) ?>
 * 
 */
namespace library;

class Helpers
{
	/**
	 * [dump description]
	 * @param  [type] $stuff [description]
	 * @return [type]        [description]
	 */
	public static function dump($stuff) {
		// lokaal werkt dit niet omdat mijn ip dat "::1" is
		// maar op productie zorgt dit ervoor dat niemand buiten mijn mijn netwerk de debug informatie ziet.
		// om te testen wat je ip is: 
		// die( $_SERVER['REMOTE_ADDR']  );
		if ( '192.168.33.1' != $_SERVER['REMOTE_ADDR'] ) {
			return;
		}

		echo '<pre>';
		var_dump($stuff);
		echo '</pre>';
	}
}
