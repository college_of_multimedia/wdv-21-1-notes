<?php
/**
 * Register post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */

namespace library;

class PostTypes
{
    public function __construct()
    {
        add_action( 'init', [ $this, 'initCustomPostType' ] );
    }

    public function initCustomPostType()
    {
        // remove_post_type_support( 'post', 'comments' );
        // remove_post_type_support( 'post', 'commentstatus' );
        // add_post_type_support( 'post', 'thumbnail' );

		register_post_type( 'books',
	        array(
	            'labels' => array(
	                'name' 			=> __( 'Boeken' ),
	                'singular_name' => __( 'Boek' )
	            ),
	            'public' => true,
	            'has_archive' => true,
	            'rewrite' => array('slug' => 'boek'),
	            'show_in_rest' => true,
	        )
	    );
		$labels = array(
		    'name' => _x( 'Tags', 'taxonomy general name' ),
		    'singular_name' => _x( 'Tag', 'taxonomy singular name' ),
		    'search_items' =>  __( 'Search Tags' ),
		    'popular_items' => __( 'Popular Tags' ),
		    'all_items' => __( 'All Tags' ),
		    'parent_item' => null,
		    'parent_item_colon' => null,
		    'edit_item' => __( 'Edit Tag' ), 
		    'update_item' => __( 'Update Tag' ),
		    'add_new_item' => __( 'Add New Tag' ),
		    'new_item_name' => __( 'New Tag Name' ),
		    'separate_items_with_commas' => __( 'Separate tags with commas' ),
		    'add_or_remove_items' => __( 'Add or remove tags' ),
		    'choose_from_most_used' => __( 'Choose from the most used tags' ),
		    'menu_name' => __( 'Tags' ),
		  ); 


		  register_taxonomy('tag','books', array(
		    'hierarchical' => true,
		    'labels' => $labels,
		    'show_ui' => true,
		    'update_count_callback' => '_update_post_term_count',
		    'query_var' => true,
		    'rewrite' => array( 'slug' => 'tag' ),
		  ));

	    remove_post_type_support( 'books', 'comments' );
	    remove_post_type_support( 'books', 'commentstatus' );
	    add_post_type_support( 'books', 'thumbnail' );
    }
}