<?php
/**
 * Theme Support
 */

namespace library;

class ThemeSupport
{
    public function __construct()
    {
		add_action( 'init', [ $this, 'actionCleanupHead' ] );
		add_action( 'widgets_init', [ $this, 'customWidgets' ] );

		register_nav_menus( [
		    'primary' 			=> __( 'Hoofdmenu', 'localisation' ),
			'primary_mobile' 	=> __( 'Hoofdmenu op mobiel', 'localisation' ),
			'secundairy' 		=> __( 'Footermenu', 'localisation' ),
		] );

        $this->setImageSizes();
    }
	    
	/**
	 * Standaard WordPress elementen verwijderen uit de head
	 */
	public function actionCleanupHead() {
		// EditURI link
		remove_action( 'wp_head', 'rsd_link' );

		// Category feed links
		remove_action( 'wp_head', 'feed_links_extra', 3 );

		// Post and comment feed links
		remove_action( 'wp_head', 'feed_links', 2 );
		remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );

		// Windows Live Writer
		remove_action( 'wp_head', 'wlwmanifest_link' );

		// Index link
		remove_action( 'wp_head', 'index_rel_link' );

		// Previous link
		remove_action( 'wp_head', 'parent_post_rel_link', 10 );

		// Start link
		remove_action( 'wp_head', 'start_post_rel_link', 10 );

		// Canonical
		remove_action( 'wp_head', 'rel_canonical', 10 );

		// Shortlink
		remove_action( 'wp_head', 'wp_shortlink_wp_head', 10 );

		// Links for adjacent posts
		remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10 );

		// WP version
		remove_action( 'wp_head', 'wp_generator' );

		remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
		remove_action( 'wp_head', 'wp_oembed_add_host_js' );
		remove_action( 'rest_api_init', 'wp_oembed_register_route' );
		remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );

		add_filter( 'gform_init_scripts_footer', '__return_true' );
	}


	/**
	 * Register our sidebars and widgetized areas.
	 */
	public function customWidgets() {
		register_sidebar( array(
			'name'          => 'Single post right sidebar',
			'id'            => 'single_right_1',
			'before_widget' => '<div>',
			'after_widget'  => '</div>',
			'before_title'  => '<h2 class="rounded">',
			'after_title'   => '</h2>',
		) );

		register_sidebar( array(
			'name'          => 'Footer part',
			'id'            => 'footer_part',
			'before_widget' => '<div>',
			'after_widget'  => '</div>',
			'before_title'  => '<h2 class="rounded">',
			'after_title'   => '</h2>',
		) );

	}


    /**
     * The image sizes for this theme
     */
    public function setImageSizes()
    {
        add_image_size('book_thumbnail', 100, 150, false);
    }
}

