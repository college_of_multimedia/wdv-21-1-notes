<?php

namespace Library;

use Library\Controllers\NewsController;
use Library\Services\View;
use Library\Settings\Config;

require_once( '../Library/autoload.php' );

/*
 * Laad de instellingen
 */
$config = new Config();

/*
 * Maak een nieuws controller object
 */
$newsController = new NewsController( $config->getDatabase() );

/*
 * Haal de juiste pagina binnen
 */
$mijnArr = $config->getCurrentItem();
if ( 'newssave' == $mijnArr['class'] ) {
	$newsController->saveNews();
}
define( 'CURRENT_ITEM', $mijnArr['class'] );
		define( 'CURRENT_PAGE', $mijnArr['item'] );

View::showView( 'Main/header' );

/*
 * laat het overzicht zien als de url leeg is
 */
switch ( $mijnArr['class'] ) {
	case 'home' :
	default:
		echo $newsController->showNewsList();
		break;
	case 'page':
		echo View::getViewWith404( 'Page/' . ucfirst( $mijnArr['item'] ) );
		break;
	case 'news':
		$newsController->showNewsItem( $mijnArr['item'] );
		break;
	case 'newsedit':
		$newsController->showNewsItemEdit( $mijnArr['item'] );
		break;
}


View::showView( 'Main/footer' );
