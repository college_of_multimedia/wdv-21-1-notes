<?php

namespace library\Controllers;

class DatabaseController {

	protected $db;

	public function __construct() {
		# database verbinding maken + controle
		$this->db = mysqli_connect('localhost', 'root', 'root', 'cmm_nieuwsberichten');
	}

	public function getDb() {
		return $this->db;
	}

	public function getTitle() {
		return 'this is mijn titel uit de database';
	}
}
