<?php

namespace library\Models;

class Taart
{
	protected $database;
	public function __construct( $dbController )
	{
		$this->database = $dbController;
	}

	public function getTitle() {
		return $this->database->getTitle();
	}
}
