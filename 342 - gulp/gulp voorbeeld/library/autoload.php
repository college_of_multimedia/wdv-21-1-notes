<?php

/**
 * Plugin name: Autoloader
 *
 * This class add the autoload functionality to the theme
 */
class autoload {
	static public function _autoload( $class ) {
		$path = str_replace( '\\', '/', $class );

		if ( file_exists( trailingslashit( dirname( __DIR__ ) ) . $path . '.php' ) ) {
			require_once( trailingslashit( dirname( __DIR__ ) ) . $path . '.php' );
		}
	}
}

spl_autoload_register( 'autoload::_autoload' );