var gulp     = require( 'gulp' ),
    sass     = require( 'gulp-sass' ),
    cssmin   = require( 'gulp-cssmin' ),
    rename   = require( 'gulp-rename' ),
    phplint  = require( 'gulp-phplint' ),
    sassLint = require( 'gulp-sass-lint' );


// Compile de scss bestanden en minify de css file
gulp.task( 'styles', function() {
    return gulp.src( './sass/*.scss' )
            .pipe( sass().on( 'error', sass.logError ) )
            .pipe( cssmin() )
            .pipe( rename( { suffix : '.min' } ) )
            .pipe( gulp.dest( 'css' ) );
} );

// Controle of mijn php code netjes is en klopt
gulp.task( 'php_lint', function () {
    gulp.src( './*.php' )
        .pipe( phplint() );
    
    return gulp.src( '../library/**/*.php' )
        .pipe( phplint() );
} );

// Controleer of mijn scss bestanden netjes zijn
gulp.task( 'scss_lint', function() {
    return gulp.src( './sass/*.scss' )
            .pipe(sassLint())
            .pipe(sassLint.format())
            .pipe(sassLint.failOnError())
});


gulp.task( 'default', function() {
    gulp.watch( './sass/*.scss', ['styles'] );
});

