#Uitleg om dit project op te starten

Ga in de terminal/commandLine naar de Vagrant box. ( de lcoatie waar je vagrant file staat )
Login in de vagrant box: 
: vagrant ssh

Navigeer nu naar de public_html folder van je project
bv:
: ~/code/dekapper/public_html

Installeer de node modules:
: npm install

Voer je gulp commando uit:
: gulp styles