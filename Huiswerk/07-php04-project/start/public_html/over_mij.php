<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="favicon.ico">

	<title>College of MultiMedia | Web Developer</title>
	
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
</head>

<body>

    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Web Developer</a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a href="/">Home</a></li>
                    <li class="active"><a href="over_mij.php">Over mij</a></li>
                    <li><a href="#contact">Contact</a></li>
                </ul>
                <form class="navbar-form navbar-right">
                    <div class="form-group">
                        <input type="text" placeholder="Email" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="password" placeholder="Password" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-success">Sign in</button>
                </form>
            </div>
            <!--/.navbar-collapse -->
        </div>
    </div>

    <div class="jumbotron">
        <div class="container">
            <h1>Mijn tweede pagina</h1>

            <p>Op deze pagina zie je allemaal tekst over mij</p>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <h2>Dit is mijn foto</h2>

                <img src="images/clown_fish.png" alt="Clown Fish" class="my_picture">
            </div>
            <div class="col-lg-4">
                <h2>Hier kan wat tekst staan</h2>

                <p>Een Clown Fish of annemoon vis is een mooie gekleurde vis.</p>
            </div>
            <div class="col-lg-4">
                <h2>Amphiprioninae</h2>
                <p>Clownfish or anemonefish are fishes from the subfamily Amphiprioninae in the family Pomacentridae. Thirty species are recognized: one in the genus Premnas, while the remaining are in the genus Amphiprion. In the wild, they all form symbiotic mutualisms with sea anemones. Depending on species, anemonefish are overall yellow, orange, or a reddish or blackish color, and many show white bars or patches. The largest can reach a length of 15–16 centimetres (5.9–6.3 in), while the smallest barely achieve 7–8 centimetres</p>
				<em>Bron: https://en.wikipedia.org/wiki/Amphiprioninae</em>
            </div>
        </div>

        <hr>

        <footer>
            <p>&copy; 2017 College of MultiMedia</p>
        </footer>
    </div>
    <!-- /container -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
